package com.github.medavox.da.parsing.parsermode

import com.github.medavox.da.parser.PARSER_MODES
import com.github.medavox.da.parsing.parsermode.AbstractMode

class Listblock : AbstractMode() {
    init {
        allowedModes = PARSER_MODES.formatting +
                PARSER_MODES.substition +
                PARSER_MODES.disabled +
                PARSER_MODES.protected
    }

    override fun connectTo(mode: String) {
        this.Lexer.addEntryPattern("[ \t]*\n {2,}[-*]", mode, "listblock")
        this.Lexer.addEntryPattern("[ \t]*\n\t{1,}[-*]", mode, "listblock")
        this.Lexer.addPattern("\n {2,}[-*]", "listblock")
        this.Lexer.addPattern("\n\t{1,}[-*]", "listblock")
    }

    override fun postConnect() {
        this.Lexer.addExitPattern("\n", "listblock")
    }

    override fun getSort(): Int {
        return 10
    }
}

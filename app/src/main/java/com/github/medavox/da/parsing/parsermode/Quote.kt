package com.github.medavox.da.parsing.parsermode

import com.github.medavox.da.parser.PARSER_MODES
import com.github.medavox.da.parsing.parsermode.AbstractMode

class Quote : AbstractMode() {
    init {
        allowedModes = PARSER_MODES.formatting +
                PARSER_MODES.substition +
                PARSER_MODES.disabled +
                PARSER_MODES.protected
    }

    override fun connectTo(mode: String) {
        this.Lexer.addEntryPattern("\n>{1,}", mode, "quote")
    }

    override fun postConnect() {
        this.Lexer.addPattern("\n>{1,}", "quote")
        this.Lexer.addExitPattern("\n", "quote")
    }

    override fun getSort(): Int {
        return 220
    }
}

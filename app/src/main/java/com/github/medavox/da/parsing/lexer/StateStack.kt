package com.github.medavox.da.parsing.lexer

import java.util.*

/**
 * States for a stack machine.
 */
class StateStack(private val stack: Stack<String>) {
    /**
     * Constructor. Starts in named state.
     * @param start        Starting state name.
     */
    constructor(start: String) : this(Stack<String>().apply { push(start) })

    /**
     * Accessor for current state.
     * @return State       State.
     */
    fun getCurrent(): String {
        return stack.peek()
    }

    /**
     * Adds a state to the stack and sets it to be the current state.
     *
     * @param state        New state.
     */
    fun enter(state: String) {
        stack.push(state)
    }

    /**
     * Leaves the current state and reverts
     * to the previous one.
     * @return Boolean    false if we attempt to drop off the bottom of the list.
     */
    fun leave(): Boolean {
        if (stack.size == 1) {
            return false
        }
        stack.pop()
        return true
    }
}

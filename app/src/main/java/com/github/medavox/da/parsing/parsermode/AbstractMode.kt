package com.github.medavox.da.parsing.parsermode

import com.github.medavox.da.parsing.lexer.Lexer

/**
 * This class and all the subclasses below are used to reduce the effort required to register
 * modes with the Lexer.
 *
 * @author Harry Fuecks <hfuecks@gmail.com>
 */
abstract class AbstractMode : ModeInterface {

    /** @var \dokuwiki\Parsing\Lexer\Lexer $Lexer will be injected on loading FIXME this should be done by setter */
    lateinit var Lexer: Lexer
    protected var allowedModes: MutableList<String> = mutableListOf()

    abstract override fun getSort(): Int

    override fun preConnect() {
    }

    override fun connectTo(mode: String) {
    }

    override fun postConnect() {
    }

    override fun accepts(mode: String): Boolean {
        return mode in allowedModes
    }
}

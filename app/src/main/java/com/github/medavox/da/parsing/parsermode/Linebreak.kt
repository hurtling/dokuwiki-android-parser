package com.github.medavox.da.parsing.parsermode

import com.github.medavox.da.parsing.parsermode.AbstractMode

class Linebreak : AbstractMode() {
    override fun connectTo(mode: String) {
        this.Lexer.addSpecialPattern("\\x5C{2}(?:[ \\t]|(?=\\n))", mode, "linebreak")
    }

    override fun getSort(): Int {
        return 140
    }
}

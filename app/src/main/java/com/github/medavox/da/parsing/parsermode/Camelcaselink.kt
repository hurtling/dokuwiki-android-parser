package com.github.medavox.da.parsing.parsermode

import com.github.medavox.da.parsing.parsermode.AbstractMode

class Camelcaselink : AbstractMode() {
    override fun connectTo(mode: String) {
        this.Lexer.addSpecialPattern(
            "\\b[A-Z]+[a-z]+[A-Z][A-Za-z]*\\b",
            mode,
            "camelcaselink"
        )
    }

    override fun getSort(): Int {
        return 290
    }
}

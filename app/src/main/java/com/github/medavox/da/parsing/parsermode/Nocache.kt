package com.github.medavox.da.parsing.parsermode

import com.github.medavox.da.parsing.parsermode.AbstractMode

class Nocache : AbstractMode() {
    override fun connectTo(mode: String) {
        this.Lexer.addSpecialPattern("~~NOCACHE~~", mode, "nocache")
    }

    override fun getSort(): Int {
        return 40
    }
}

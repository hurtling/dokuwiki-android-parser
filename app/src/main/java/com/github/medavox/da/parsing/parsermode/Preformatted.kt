package com.github.medavox.da.parsing.parsermode

import com.github.medavox.da.parsing.parsermode.AbstractMode

class Preformatted : AbstractMode() {
    override fun connectTo(mode: String) {
        // Has hard coded awareness of lists...
        this.Lexer.addEntryPattern("\n  (?![\\*\\-])", mode, "preformatted")
        this.Lexer.addEntryPattern("\n\t(?![\\*\\-])", mode, "preformatted")
        // How to effect a sub pattern with the Lexer!
        this.Lexer.addPattern("\n  ", "preformatted")
        this.Lexer.addPattern("\n\t", "preformatted")
    }
    
    override fun postConnect() {
        this.Lexer.addExitPattern("\n", "preformatted")
    }
    
    override fun getSort(): Int {
        return 20
    }
}

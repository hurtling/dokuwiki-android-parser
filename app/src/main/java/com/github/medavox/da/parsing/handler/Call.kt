package com.github.medavox.da.parsing.handler

//val call = Call(handler, args, pos)
data class Call(var handler: String, var args: MutableList<Any?>, var pos: Int) {
    operator fun get(i: Int): Any? {
        return when(i) {
            0 -> handler
            1 -> args
            2 -> pos
            else -> throw ArrayIndexOutOfBoundsException("not a real array! only indices 0, 1 and 2 are valid")
        }
    }

    operator fun set(i: Int, b: Any?) {
        when(i) {
            0 -> handler = b as String
            1 -> args = b as MutableList<Any?>
            2 -> pos = b as Int
            else -> throw ArrayIndexOutOfBoundsException("not a real array! only indices 0, 1 and 2 are valid")
        }
    }
}
package com.github.medavox.da.parsing.parsermode

import com.github.medavox.da.parsing.parsermode.AbstractMode

class Rss : AbstractMode() {
    override fun connectTo(mode: String) {
        this.Lexer.addSpecialPattern("\\{\\{rss>[^\\}]+\\}\\}", mode, "rss")
    }

    override fun getSort(): Int {
        return 310
    }
}

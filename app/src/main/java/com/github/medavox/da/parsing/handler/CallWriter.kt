package com.github.medavox.da.parsing.handler

import com.github.medavox.da.parser.Doku_Handler
import com.github.medavox.da.parsing.handler.CallWriterInterface

class CallWriter(private var Handler: Doku_Handler?) : CallWriterInterface {
    override fun writeCall(call: Call) {
        Handler?.calls?.add(call)
    }

    override fun writeCalls(calls: List<Call>) {
        Handler?.calls?.addAll(calls)
    }

    override fun finalise() {
        Handler = null
    }
}

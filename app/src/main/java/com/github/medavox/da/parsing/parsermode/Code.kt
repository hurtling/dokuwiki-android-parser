package com.github.medavox.da.parsing.parsermode

import com.github.medavox.da.parsing.parsermode.AbstractMode

class Code : AbstractMode() {
    override fun connectTo(mode: String) {
        this.Lexer.addEntryPattern("<code\\b(?=.*</code>)", mode, "code")
    }

    override fun postConnect() {
        this.Lexer.addExitPattern("</code>", "code")
    }

    override fun getSort(): Int {
        return 200
    }
}

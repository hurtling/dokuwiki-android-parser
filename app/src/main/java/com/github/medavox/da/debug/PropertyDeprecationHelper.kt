package com.github.medavox.da.debug
import com.github.medavox.da.debug.DebugHelper

/**
 * Trait for issuing warnings on deprecated access.
 *
 * Adapted from https://github.com/wikimedia/mediawiki/blob/4aedefdbfd193f323097354bf581de1c93f02715/includes/debug/DeprecationHelper.php
 *
 */

/**
 * Use this trait in classes which have properties for which public access
 * is deprecated. Set the list of properties in $deprecatedPublicProperties
 * and make the properties non-public. The trait will preserve public access
 * but issue deprecation warnings when it is needed.
 *
 * Example usage:
 *     class Foo {
 *         use DeprecationHelper;
 *         protected $bar;
 *         public fun __construct() {
 *             this.deprecatePublicProperty( 'bar', '1.21', __CLASS__ );
 *         }
 *     }
 *
 *     val foo = Foo()
 *     foo.bar // works but logs a warning
 *
 * Cannot be used with classes that have their own __get/__set methods.
 *
 */
class PropertyDeprecationHelper {
    /**
     * List of deprecated properties, in <property name> => <class> format
     * where <class> is the the name of the class defining the property
     *
     * E.g. [ '_event' => '\dokuwiki\Cache\Cache' ]
     * @var MutableMap<String, String>
     */
    protected val deprecatedPublicProperties: MutableMap<String, String> = mutableMapOf()

    /**
     * Mark a property as deprecated. Only use this for properties that used to be public and only
     *   call it in the constructor.
     *
     * @param property The name of the property.
     * @param class name of the class defining the property
     * @see DebugHelper.dbgDeprecatedProperty
     */
    protected fun deprecatePublicProperty(property: String, clazz: String? = null) {
        deprecatedPublicProperties[property] = clazz ?: this::class.java.name
    }

    fun __get(name: String): Any? {
        if (deprecatedPublicProperties.containsKey(name)) {
            val className = deprecatedPublicProperties[name]
            DebugHelper.dbgDeprecatedProperty(className!!, name)
            return this::class.java.getDeclaredField(name).get(this)
        }
        val qualifiedName = "${this::class.java.name}::\$$name"
        if (deprecationHelperGetPropertyOwner(name) != null) {
            // Someone tried to access a normal non-public property. Try to behave like PHP would.
            error("Cannot access non-public property $qualifiedName")
        } else {
            // Non-existing property. Try to behave like PHP would.
            println("Undefined property: $qualifiedName")
        }
        return null
    }

    fun __set(name: String, value: Any?) {
        if (deprecatedPublicProperties.containsKey(name)) {
            val className = deprecatedPublicProperties[name]
            DebugHelper.dbgDeprecatedProperty(className!!, name)
            this::class.java.getDeclaredField(name).set(this, value)
            return
        }
        val qualifiedName = "${this::class.java.name}::\$$name"
        if (deprecationHelperGetPropertyOwner(name) != null) {
            // Someone tried to access a normal non-public property. Try to behave like PHP would.
            error("Cannot access non-public property $qualifiedName")
        } else {
            // Non-existing property. Try to behave like PHP would.
            this::class.java.getDeclaredField(name).set(this, value)
        }
    }

    /**
     * Like property_exists but also check for non-visible private properties and returns which
     * class in the inheritance chain declared the property.
     * @param property
     * @return String|Boolean Best guess for the class in which the property is defined.
     */
    private fun deprecationHelperGetPropertyOwner(property: String): String? {
        // Easy branch: check for protected property / private property of the current class.
        if (this::class.java.getDeclaredField(property) != null) {
            // The class name is not necessarily correct here but getting the correct class
            // name would be expensive, this will work most of the time and getting it
            // wrong is not a big deal.
            return this::class.java.name
        }
        // property_exists() returns false when the property does exist but is private (and not
        // defined by the current class, for some value of "current" that differs slightly
        // between engines).
        // Since PHP triggers an error on public access of non-public properties but happily
        // allows public access to  properties, we need to detect this case as well.
        // Reflection is slow so use array cast hack to check for that:
        val obfuscatedProps = this::class.java.declaredFields.map { it.name }
        val obfuscatedPropTail = "\u0000$property"
        for (obfuscatedProp in obfuscatedProps) {
            // private props are in the form \0<classname>\0<propname>
            if (obfuscatedProp.contains(obfuscatedPropTail, true)) {
                val className = obfuscatedProp.substring(1, obfuscatedProp.length - obfuscatedPropTail.length)
                if (className == "*") {
                    // sanity; this shouldn't be possible as protected properties were handled earlier
                    return this::class.java.name
                }
                return className
            }
        }
        return null
    }
}

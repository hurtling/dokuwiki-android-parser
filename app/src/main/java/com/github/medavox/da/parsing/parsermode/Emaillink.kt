package com.github.medavox.da.parsing.parsermode

import com.github.medavox.da.PREG_PATTERN_VALID_EMAIL
import com.github.medavox.da.parsing.parsermode.AbstractMode

class Emaillink : AbstractMode() {
    override fun connectTo(mode: String) {
        // pattern below is defined in inc/mail.php
        this.Lexer.addSpecialPattern("<${PREG_PATTERN_VALID_EMAIL}>", mode, "emaillink")
    }

    override fun getSort(): Int {
        return 340
    }
}

package com.github.medavox.da.extension

/**
 * DokuWiki Base Plugin
 *
 * Most plugin types inherit from this class
 */
abstract class Plugin : PluginInterface by PluginTrait()

package com.github.medavox.da

const val AUTH_NONE = 0
const val AUTH_READ = 1
const val AUTH_EDIT = 2
const val AUTH_CREATE = 4
const val AUTH_UPLOAD = 8
const val AUTH_DELETE = 16
const val AUTH_ADMIN = 255

const val MSG_PUBLIC = 0
const val MSG_USERS_ONLY = 1
const val MSG_MANAGERS_ONLY = 2
const val MSG_ADMINS_ONLY = 4

const val DOKU_LEXER_ENTER = 1
const val DOKU_LEXER_MATCHED = 2
const val DOKU_LEXER_UNMATCHED = 3
const val DOKU_LEXER_EXIT = 4
const val DOKU_LEXER_SPECIAL = 5

const val DOKU_CHANGE_TYPE_CREATE = "C"
const val DOKU_CHANGE_TYPE_EDIT = "E"
const val DOKU_CHANGE_TYPE_MINOR_EDIT = "e"
const val DOKU_CHANGE_TYPE_DELETE = "D"
const val DOKU_CHANGE_TYPE_REVERT = "R"

const val RECENTS_SKIP_DELETED = 2
const val RECENTS_SKIP_MINORS = 4
const val RECENTS_SKIP_SUBSPACES = 8
const val RECENTS_MEDIA_CHANGES = 16
const val RECENTS_MEDIA_PAGES_MIXED = 32
const val RECENTS_ONLY_CREATION = 64

const val DOKU_MEDIA_DELETED = 1
const val DOKU_MEDIA_NOT_AUTH = 2
const val DOKU_MEDIA_INUSE = 4
const val DOKU_MEDIA_EMPTY_NS = 8

const val MAILHEADER_EOL = "\r\n"


//============================
// constants that were stored elsewhere in the php

const val DOKU_LF = "\n"

/**
 * Patterns for use in email detection and validation
 *
 * NOTE: there is an unquoted '/' in RFC2822_ATEXT, it must remain unquoted to be used in the parser
 * the pattern uses non-capturing groups as captured groups aren't allowed in the parser
 * select pattern delimiters with care!
 *
 * May not be completly RFC conform!
 * @link http://www.faqs.org/rfcs/rfc2822.html (paras 3.4.1 & 3.2.4)
 *
 * @author Chris Smith <chris@jalakai.co.uk>
 * Check if a given mail address is valid
 */
const val RFC2822_ATEXT = "0-9a-zA-Z!#$%&'*+/=?^_`{|}~-"

const val PREG_PATTERN_VALID_EMAIL =  "["+RFC2822_ATEXT+"]+(?:\\.["+RFC2822_ATEXT+"]+)*@(?i:[0-9a-z][0-9a-z-]*\\.)+(?i:[a-z]{2,63})"

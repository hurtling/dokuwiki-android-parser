package com.github.medavox.da.parsing.handler

import com.github.medavox.da.DOKU_LEXER_ENTER
import com.github.medavox.da.DOKU_LEXER_EXIT
import com.github.medavox.da.DOKU_LEXER_SPECIAL
import java.util.*

class Block {
    private val calls: MutableList<Call> = mutableListOf()
    private var skipEol = false
    private var inParagraph = false
    private var blockOpen = arrayOf(
        "header",
        "listu_open", "listo_open", "listitem_open", "listcontent_open",
        "table_open", "tablerow_open", "tablecell_open", "tableheader_open", "tablethead_open",
        "quote_open",
        "code", "file", "hr", "preformatted", "rss",
        "footnote_open"
    )
    private var blockClose = arrayOf(
        "header",
        "listu_close", "listo_close", "listitem_close", "listcontent_close",
        "table_close", "tablerow_close", "tablecell_close", "tableheader_close", "tablethead_close",
        "quote_close",
        "code", "file", "hr", "preformatted", "rss",
        "footnote_close"
    )
    private var stackOpen = arrayOf(
        "section_open"
    )
    private var stackClose = arrayOf(
        "section_close"
    )

    init {
        // Constructor. Adds loaded syntax plugins to the block and stack arrays
        val DOKU_PLUGINS = emptyMap<String, Any>()
        //check if syntax plugins were loaded
        if (DOKU_PLUGINS["syntax"] != null) {
            for ((n, p) in DOKU_PLUGINS["syntax"] as Map<String, Any>) {
                val ptype = (p as Any).getPType()
                if (ptype == "block") {
                    blockOpen += "plugin_$n"
                    blockClose += "plugin_$n"
                } else if (ptype == "stack") {
                    stackOpen += "plugin_$n"
                    stackClose += "plugin_$n"
                }
            }
        }
    }

    private fun openParagraph(pos: Int) {
        if (inParagraph) return
        calls.add(Call("p_open", mutableListOf(), pos))
        inParagraph = true
        skipEol = true
    }

    private fun closeParagraph(pos: Int) {
        if (!inParagraph) return
        var content = ""
        val ccount = calls.size
        for (i in ccount - 1 downTo 0) {
            if (calls[i][0] == "p_open") {
                break
            } else if (calls[i][0] == "cdata") {
                content += calls[i].args[0]
            } else {
                content = "found markup"
                break
            }
        }
        if (content.trim() == "") {
            for (x in ccount downTo i + 1) calls.removeAt(x)
        } else {
            val lastIndex = calls.size - 1
            if (calls[lastIndex][0] == "cdata") calls[lastIndex].args[0] = calls[lastIndex].args[0].trimEnd('\n')
            calls.add(Call("p_close", mutableListOf(), pos))
        }
        inParagraph = false
        skipEol = true
    }

    private fun addCall(call: Call) {
        val key = calls.size
        if (key > 0 && call[0] == "cdata" && calls[key - 1][0] == "cdata") {
            calls[key - 1].args[0] = calls[key - 1].args[0] as String + call.args[0]
        } else {
            calls.add(call)
        }
    }

    private fun storeCall(call: Call) {
        calls.add(call)
    }

    fun process(calls: List<Call>): List<Call> {
        openParagraph(0)
        for ((key, call) in calls.withIndex()) {
            var cname = call[0] as String
            var plugin = false
            var plugin_open = false
            var plugin_close = false
            if (cname == "plugin") {
                cname = "plugin_" + call.args[0]
                plugin = true
                plugin_open = (call.args[2] == DOKU_LEXER_ENTER || call.args[2] == DOKU_LEXER_SPECIAL)
                plugin_close = (call.args[2] == DOKU_LEXER_EXIT || call.args[2] == DOKU_LEXER_SPECIAL)
            }
            if (stackClose.contains(cname) && (!plugin || plugin_close)) {
                closeParagraph(call.pos)
                storeCall(call)
                openParagraph(call.pos)
                continue
            }
            if (stackOpen.contains(cname) && (!plugin || plugin_open)) {
                closeParagraph(call.pos)
                storeCall(call)
                openParagraph(call.pos)
                continue
            }
            if (blockClose.contains(cname) && (!plugin || plugin_close)) {
                closeParagraph(call.pos)
                storeCall(call)
                openParagraph(call.pos)
                continue
            }
            if (blockOpen.contains(cname) && (!plugin || plugin_open)) {
                closeParagraph(call.pos)
                storeCall(call)
                continue
            }
            if (cname == "eol") {
                if (!skipEol) {
                    if (calls.getOrNull(key + 1)?.get(0) == "eol") {
                        closeParagraph(call.pos)
                        openParagraph(call.pos)
                    } else {
                        addCall(Call("cdata", mutableListOf("\n"), call.pos))
                    }
                }
                continue
            }
            addCall(call)
            skipEol = false
        }
        val call = calls.last()
        closeParagraph(call.pos)
        return calls
    }
}

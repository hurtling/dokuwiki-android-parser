package com.github.medavox.da.parsing.parsermode

import com.github.medavox.da.parser.PARSER_MODES
import com.github.medavox.da.parsing.parsermode.AbstractMode

class Base : AbstractMode() {
    init {
        val allowedModes = PARSER_MODES.container +
                PARSER_MODES.baseonly +
                PARSER_MODES.paragraphs +
                PARSER_MODES.formatting +
                PARSER_MODES.substition +
                PARSER_MODES.protected +
                PARSER_MODES.disabled
    }

    override fun getSort(): Int {
        return 0
    }
}

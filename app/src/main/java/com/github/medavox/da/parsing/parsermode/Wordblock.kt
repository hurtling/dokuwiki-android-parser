package com.github.medavox.da.parsing.parsermode

class Wordblock : AbstractMode {
    private var badwords: Array<String> = emptyArray()
    private var pattern: String = ""

    constructor(badwords: Array<String>) {
        this.badwords = badwords
    }

    override fun preConnect() {
        if (badwords.isEmpty() || pattern.isNotEmpty()) {
            return
        }
        var sep = ""
        for (badword in badwords) {
            pattern += "$sep(?<=\\b)(?i)" + com.github.medavox.da.parsing.lexer.Lexer.escape(badword) + "(?-i)(?=\\b)"
            sep = "|"
        }
    }

    override fun connectTo(mode: String) {
        if (pattern.isNotEmpty()) {
            this.Lexer.addSpecialPattern(pattern, mode, "wordblock")
        }
    }

    override fun getSort(): Int {
        return 250
    }
}

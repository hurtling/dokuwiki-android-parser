package com.github.medavox.da.parsing.handler

class Preformatted(callWriter: CallWriterInterface) : AbstractRewriter(callWriter) {
    private var pos: Int = 0
    private var text: String = ""

    override fun finalise() {
        writeCall(Call("preformatted_end", listOf(), calls.last().pos))
        process()
        callWriter.finalise()
//        callWriter = null
    }


    override fun process(): CallWriterInterface? {
        for (call in calls) {
            when (call[0]) {
                "preformatted_start" -> {
                    pos = call.pos
                }
                "preformatted_newline" -> {
                    text += "\n"
                }
                "preformatted_content" -> {
                    text += call.args[0]
                }
                "preformatted_end" -> {
                    if (text.trim().isNotEmpty()) {
                        callWriter.writeCall(Call("preformatted", listOf(text), pos))
                    }
                    callWriter.writeCall(Call("eol", emptyList(), pos))
                    callWriter.writeCall(Call("eol", emptyList(), pos))
                }
            }
        }
        return callWriter
    }
}

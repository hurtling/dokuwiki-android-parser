package com.github.medavox.da.parsing.parsermode

import com.github.medavox.da.parsing.parsermode.AbstractMode

class Filelink : AbstractMode() {
    private var pattern: String = ""

    override fun preConnect() {
        val ltrs = "\\w"
        val gunk = "/\\#~:.?+=&%@!\\-"
        val punc = ".:?\\-;,"
        val host = ltrs + punc
        val any = ltrs + gunk + punc
        pattern = "\\b(?i)file(?-i)://[$any]+?[$punc]*[^$any]"
    }

    override fun connectTo(mode: String) {
        this.Lexer.addSpecialPattern(
            pattern,
            mode,
            "filelink"
        )
    }

    override fun getSort(): Int {
        return 360
    }
}

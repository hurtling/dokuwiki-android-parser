package com.github.medavox.da.parser

import com.github.medavox.da.conf
import java.util.ArrayList

class Doku_Renderer_metadata : Doku_Renderer() {
    companion object {
        /** the approximate byte lenght to capture for the abstract */
        const val ABSTRACT_LEN = 250
        /** the maximum UTF8 character length for the abstract */
        const val ABSTRACT_MAX = 500
    }

    /** transient meta data, will be reset on each rendering */
    var meta: ArrayList<Any> = ArrayList()
    /** persistent meta data, will be kept until explicitly deleted */
    var persistent: ArrayList<Any> = ArrayList()
    /** the list of headers used to create unique link ids */
    protected var headers: ArrayList<Any> = ArrayList()
    /** temporary $doc store */
    protected var store: String = ""
    /** keeps the first image reference */
    protected var firstimage: String = ""
    /** whether or not data is being captured for the abstract, public to be accessible by plugins */
    var capturing: Boolean = true
    /** determines if enough data for the abstract was collected, yet */
    var capture: Boolean = true
    /** number of bytes captured for abstract */
    protected var captured: Int = 0

    /**
     * Returns the format produced by this renderer.
     *
     * @return string always 'metadata'
     */
    override fun getFormat(): String {
        return "metadata"
    }

    /**
     * Initialize the document
     *
     * Sets up some of the persistent info about the page if it doesn't exist, yet.
     */
    override fun document_start() {
        headers = ArrayList()
        // external pages are missing create date
        if (persistent.get("date")?.get("created") == null || persistent.get("date")?.get("created") == "") {
            persistent.get("date")?.set("created", filectime(wikiFN(ID)))
        }
        if (persistent.get("user") == null) {
            persistent.set("user", "")
        }
        if (persistent.get("creator") == null) {
            persistent.set("creator", "")
        }
        // reset metadata to persistent values
        meta = persistent
    }

    /**
     * Finalize the document
     *
     * Stores collected data in the metadata
     */
    fun document_end() {
        // store internal info in metadata (notoc,nocache)
        meta.set("internal", info)
        if (meta.get("description")?.get("abstract") == null) {
            // cut off too long abstracts
            doc = doc.trim()
            if (doc.length > ABSTRACT_MAX) {
                doc = doc.substring(0, ABSTRACT_MAX) + "…"
            }
            meta.get("description")?.set("abstract", doc)
        }
        meta.get("relation")?.set("firstimage", firstimage)
        if (meta.get("date")?.get("modified") == null) {
            meta.get("date")?.set("modified", filemtime(wikiFN(ID)))
        }
    }

    /**
     * Render plain text data
     *
     * This function takes care of the amount captured data and will stop capturing when
     * enough abstract data is available
     *
     * @param $text
     */
    override fun cdata(text: String) {
        if (!capture || !capturing) {
            return
        }
        doc += text
        captured += text.length
        if (captured > ABSTRACT_LEN) {
            capture = false
        }
    }

    /**
     * Add an item to the TOC
     *
     * @param string $id       the hash link
     * @param string $text     the text to display
     * @param int    $level    the nesting level
     */
    override fun toc_additem(id: String, text: String, level: Int) {
        //only add items within configured levels
        if (level >= conf.get("toptoclevel") && level <= conf.get("maxtoclevel")) {
            // the TOC is one of our standard ul list arrays ;-)
            meta.get("description")?.get("tableofcontents")?.add(
                mapOf(
                    "hid" to id,
                    "title" to text,
                    "type" to "ul",
                    "level" to level - conf.get("toptoclevel") + 1
                )
            )
        }
    }

    /**
     * Render a heading
     *
     * @param string $text  the text to display
     * @param int    $level header level
     * @param int    $pos   byte position in the original source
     */
    fun header(text: String, level: Int, pos: Int) {
        if (meta.get("title") == null) {
            meta.set("title", text)
        }
        // add the header to the TOC
        val hid = _headerToLink(text, true)
        toc_additem(hid, text, level)
        // add to summary
        cdata("\n$text\n")
    }

    /**
     * Open a paragraph
     */
    override fun p_open() {
        cdata("\n")
    }

    /**
     * Close a paragraph
     */
    override fun p_close() {
        cdata("\n")
    }

    /**
     * Create a line break
     */
    override fun linebreak() {
        cdata("\n")
    }

    /**
     * Create a horizontal line
     */
    override fun hr() {
        cdata("\n----------\n")
    }

    /**
     * Callback for footnote start syntax
     *
     * All following content will go to the footnote instead of
     * the document. To achieve this the previous rendered content
     * is moved to $store and $doc is cleared
     *
     * @author Andreas Gohr <andi@splitbrain.org>
     */
    override fun footnote_open() {
        if (capture) {
            // move current content to store
            // this is required to ensure safe behaviour of plugins accessed within footnotes
            store = doc
            doc = ""
            // disable capturing
            capturing = false
        }
    }

    /**
     * Callback for footnote end syntax
     *
     * All content rendered whilst within footnote syntax mode is discarded,
     * the previously rendered content is restored and capturing is re-enabled.
     *
     * @author Andreas Gohr
     */
    override fun footnote_close() {
        if (capture) {
            // re-enable capturing
            capturing = true
            // restore previously rendered content
            doc = store
            store = ""
        }
    }

    /**
     * Open an unordered list
     */
    override fun listu_open() {
        cdata("\n")
    }

    /**
     * Open an ordered list
     */
    override fun listo_open() {
        cdata("\n")
    }

    /**
     * Open a list item
     *
     * @param int $level the nesting level
     * @param bool $node true when a node; false when a leaf
     */
    override fun listitem_open(level: Int, node: Boolean = false) {
        cdata("\t".repeat(level) + "* ")
    }

    /**
     * Close a list item
     */
    override fun listitem_close() {
        cdata("\n")
    }

    /**
     * Output preformatted text
     *
     * @param string $text
     */
    override fun preformatted(text: String) {
        cdata(text)
    }

    /**
     * Start a block quote
     */
    override fun quote_open() {
        cdata("\n\t\"")
    }

    /**
     * Stop a block quote
     */
    override fun quote_close() {
        cdata("\"\n")
    }

    /**
     * Display text as file content, optionally syntax highlighted
     *
     * @param string $text text to show
     * @param string $lang programming language to use for syntax highlighting
     * @param string $file file path label
     */
    fun file(text: String, lang: String? = null, file: String? = null) {
        cdata("\n$text\n")
    }

    /**
     * Display text as code content, optionally syntax highlighted
     *
     * @param string $text     text to show
     * @param string $language programming language to use for syntax highlighting
     * @param string $file     file path label
     */
    fun code(text: String, language: String? = null, file: String? = null) {
        cdata("\n$text\n")
    }

    /**
     * Format an acronym
     *
     * Uses $this->acronyms
     *
     * @param string $acronym
     */
    override fun acronym(acronym: String) {
        cdata(acronym)
    }

    /**
     * Format a smiley
     *
     * Uses $this->smiley
     *
     * @param string $smiley
     */
    override fun smiley(smiley: String) {
        cdata(smiley)
    }

    /**
     * Format an entity
     *
     * Entities are basically small text replacements
     *
     * Uses $this->entities
     *
     * @param string $entity
     */
    override fun entity(entity: String) {
        cdata(entity)
    }

    /**
     * Typographically format a multiply sign
     *
     * Example: ($x=640, $y=480) should result in "640×480"
     *
     * @param string|int $x first value
     * @param string|int $y second value
     */
    override fun multiplyentity(x: Any, y: Any) {
        cdata("$x×$y")
    }

    /**
     * Render an opening single quote char (language specific)
     */
    override fun singlequoteopening() {
        cdata(lang.get("singlequoteopening"))
    }

    /**
     * Render a closing single quote char (language specific)
     */
    override fun singlequoteclosing() {
        cdata(lang.get("singlequoteclosing"))
    }

    /**
     * Render an apostrophe char (language specific)
     */
    override fun apostrophe() {
        cdata(lang.get("apostrophe"))
    }

    /**
     * Render an opening double quote char (language specific)
     */
    override fun doublequoteopening() {
        cdata(lang.get("doublequoteopening"))
    }

    /**
     * Render an closinging double quote char (language specific)
     */
    override fun doublequoteclosing() {
        cdata(lang.get("doublequoteclosing"))
    }

    /**
     * Render a CamelCase link
     *
     * @param string $link The link name
     * @see http://en.wikipedia.org/wiki/CamelCase
     */
    override fun camelcaselink(link: String) {
        internallink(link, link)
    }

    /**
     * Render a page local link
     *
     * @param string $hash hash link identifier
     * @param string $name name for the link
     */
    fun locallink(hash: String, name: String? = null) {
        if (name is ArrayList<*>) {
            _firstimage(name.get("src") as String)
            if (name.get("type") == "internalmedia") {
                _recordMediaUsage(name.get("src") as String)
            }
        }
    }

    /**
     * keep track of internal links in $this->meta['relation']['references']
     *
     * @param string            $id   page ID to link to. eg. 'wiki:syntax'
     * @param string|array|null $name name for the link, array for media file
     */
    override fun internallink(id: String, name: Any? = null) {
        if (name is ArrayList<*>) {
            _firstimage(name.get("src") as String)
            if (name.get("type") == "internalmedia") {
                _recordMediaUsage(name.get("src") as String)
            }
        }
        val parts = id.split("?")
        if (parts.size == 2) {
            id = parts[0]
        }
        val default = _simpleTitle(id)
        // first resolve and clean up the $id
        val resolver = \dokuwiki\File\PageResolver(ID)
        id = resolver.resolveId(id)
        val page = sexplode("#", id, 2)[0]
        // set metadata
        meta.get("relation")?.get("references")?.set(page, page_exists(page))
        // add link title to summary
        if (capture) {
            val linkTitle = _getLinkTitle(name, default, id)
            doc += linkTitle
        }
    }

    /**
     * Render an external link
     *
     * @param string            $url  full URL with scheme
     * @param string|array|null $name name for the link, array for media file
     */
    override fun externallink(url: String, name: Any? = null) {
        if (name is ArrayList<*>) {
            _firstimage(name.get("src") as String)
            if (name.get("type") == "internalmedia") {
                _recordMediaUsage(name.get("src") as String)
            }
        }
        if (capture) {
            val linkTitle = _getLinkTitle(name, "<$url>")
            doc += linkTitle
        }
    }

    /**
     * Render an interwiki link
     *
     * You may want to use $this->_resolveInterWiki() here
     *
     * @param string       $match     original link - probably not much use
     * @param string|array $name      name for the link, array for media file
     * @param string       $wikiName  indentifier (shortcut) for the remote wiki
     * @param string       $wikiUri   the fragment parsed from the original link
     */
    fun interwikilink(match: String, name: Any, wikiName: String, wikiUri: String) {
        if (name is ArrayList<*>) {
            _firstimage(name.get("src") as String)
            if (name.get("type") == "internalmedia") {
                _recordMediaUsage(name.get("src") as String)
            }
        }
        if (capture) {
            val wikiUriParts = wikiUri.split("#")
            val wikiUriCleaned = wikiUriParts[0]
            val linkTitle = _getLinkTitle(name, wikiUriCleaned)
            doc += linkTitle
        }
    }

    /**
     * Link to windows share
     *
     * @param string       $url  the link
     * @param string|array $name name for the link, array for media file
     */
    override fun windowssharelink(url: String, name: Any? = null) {
        if (name is ArrayList<*>) {
            _firstimage(name.get("src") as String)
            if (name.get("type") == "internalmedia") {
                _recordMediaUsage(name.get("src") as String)
            }
        }
        if (capture) {
            if (name != null) {
                doc += name
            } else {
                doc += "<$url>"
            }
        }
    }

    /**
     * Render a linked E-Mail Address
     *
     * Should honor $conf['mailguard'] setting
     *
     * @param string       $address Email-Address
     * @param string|array $name    name for the link, array for media file
     */
    override fun emaillink(address: String, name: Any? = null) {
        if (name is ArrayList<*>) {
            _firstimage(name.get("src") as String)
            if (name.get("type") == "internalmedia") {
                _recordMediaUsage(name.get("src") as String)
            }
        }
        if (capture) {
            if (name != null) {
                doc += name
            } else {
                doc += "<$address>"
            }
        }
    }

    /**
     * Render an internal media file
     *
     * @param string $src     media ID
     * @param string $title   descriptive text
     * @param string $align   left|center|right
     * @param int    $width   width of media in pixel
     * @param int    $height  height of media in pixel
     * @param string $cache   cache|recache|nocache
     * @param string $linking linkonly|detail|nolink
     */
    fun internalmedia(
        src: String,
        title: String? = null,
        align: String? = null,
        width: Int? = null,
        height: Int? = null,
        cache: String? = null,
        linking: String? = null
    ) {
        if (capture && title != null) {
            doc += "[$title]"
        }
        _firstimage(src)
        _recordMediaUsage(src)
    }

    /**
     * Render an external media file
     *
     * @param string $src     full media URL
     * @param string $title   descriptive text
     * @param string $align   left|center|right
     * @param int    $width   width of media in pixel
     * @param int    $height  height of media in pixel
     * @param string $cache   cache|recache|nocache
     * @param string $linking linkonly|detail|nolink
     */
    fun externalmedia(
        src: String,
        title: String? = null,
        align: String? = null,
        width: Int? = null,
        height: Int? = null,
        cache: String? = null,
        linking: String? = null
    ) {
        if (capture && title != null) {
            doc += "[$title]"
        }
        _firstimage(src)
    }

    /**
     * Render the output of an RSS feed
     *
     * @param string $url    URL of the feed
     * @param array  $params Finetuning of the output
     */
    fun rss(url: String, params: Map<String, Any>) {
        meta.get("relation")?.get("haspart")?.set(url, true)
        meta.get("date")?.get("valid")?.set(
            "age",
            if (meta.get("date")?.get("valid")?.get("age") != null)
                min(meta.get("date")?.get("valid")?.get("age"), params.get("refresh"))
            else
                params.get("refresh")
        )
    }

    /**
     * Removes any Namespace from the given name but keeps
     * casing and special chars
     *
     * @param string $name
     *
     * @return mixed|string
     */
    override fun _simpleTitle(name: String): String {
        if (name is ArrayList<*>) {
            return ""
        }
        val nssep = if (conf.get("useslash")) "[:;/]" else "[:;]"
        var cleanedName = name.replace(".*$nssep".toRegex(), "")
        cleanedName = cleanedName.replace(".*#!".toRegex(), "")
        return cleanedName
    }

    /**
     * Construct a title and handle images in titles
     *
     * @param string|array|null $title    either string title or media array
     * @param string            $default  default title if nothing else is found
     * @param null|string       $id       linked page id (used to extract title from first heading)
     * @return string title text
     */
    fun _getLinkTitle(title: Any?, default: String, id: String? = null): String {
        if (title is ArrayList<*>) {
            if (title.get("title") != null) {
                return "[${title.get("title")}]"
            } else {
                return default
            }
        } else if (title == null || title.trim() == "") {
            if (useHeading("content") && id != null) {
                val heading = p_get_first_heading(id, METADATA_DONT_RENDER)
                if (heading != null) {
                    return heading
                }
            }
            return default
        } else {
            return title
        }
    }

    /**
     * Remember first image
     *
     * @param string $src image URL or ID
     */
    protected fun _firstimage(src: String) {
        if (firstimage != "") {
            return
        }
        val srcParts = src.split("#")
        val srcCleaned = if (!media_isexternal(srcParts[0])) (new \dokuwiki\File\MediaResolver(ID)).resolveId(srcParts[0]) else srcParts[0]
        if (Regex(".(jpe?g|gif|png)$", RegexOption.IGNORE_CASE).matches(srcCleaned)) {
            firstimage = srcCleaned
        }
    }

    /**
     * Store list of used media files in metadata
     *
     * @param string $src media ID
     */
    protected fun _recordMediaUsage(src: String) {
        val srcParts = src.split("#")
        val srcCleaned = if (!media_isexternal(srcParts[0])) (new \dokuwiki\File\MediaResolver(ID)).resolveId(srcParts[0]) else srcParts[0]
        val file = mediaFN(srcCleaned)
        meta.get("relation")?.get("media")?.set(srcCleaned, file_exists(file))
    }
}

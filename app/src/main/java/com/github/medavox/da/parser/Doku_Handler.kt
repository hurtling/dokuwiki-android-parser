package com.github.medavox.da.parser

import com.github.medavox.da.DOKU_LEXER_ENTER
import com.github.medavox.da.DOKU_LEXER_EXIT
import com.github.medavox.da.DOKU_LEXER_MATCHED
import com.github.medavox.da.DOKU_LEXER_UNMATCHED
import com.github.medavox.da.extension.Event
import com.github.medavox.da.extension.SyntaxPlugin
import com.github.medavox.da.parsing.handler.Block
import com.github.medavox.da.parsing.handler.Call
import com.github.medavox.da.parsing.handler.CallWriter
import com.github.medavox.da.parsing.handler.CallWriterInterface
import com.github.medavox.da.parsing.handler.Lists
import com.github.medavox.da.parsing.handler.Nest
import com.github.medavox.da.parsing.handler.Preformatted
import com.github.medavox.da.parsing.handler.Quote
import com.github.medavox.da.parsing.handler.Table

class Doku_Handler {
    protected var callWriter: CallWriterInterface
    var calls: MutableList<Call> = mutableListOf()
    protected var status: MutableMap<String, Any> = mutableMapOf(
        "section" to false,
        "doublequote" to 0
    )
    protected var rewriteBlocks = true
    protected var footnote: Boolean = false

    init {
        callWriter = CallWriter(this)
    }

    fun addCall(handler: String, args: MutableList<Any?>, pos: Int) {
        val call = Call(handler, args, pos)
        callWriter.writeCall(call)
    }

    fun getCallWriter(): CallWriterInterface {
        return callWriter
    }

    fun setCallWriter(callWriter: CallWriterInterface) {
        this.callWriter = callWriter
    }

    fun getStatus(status: String): Any? {
        if (!this.status.containsKey(status)) return null
        return this.status[status]
    }

    fun setStatus(status: String, value: Any) {
        this.status[status] = value
    }

    fun _addCall(handler: String, args: List<Any?>, pos: Int) {
        dbg_deprecated("addCall")
        addCall(handler, args.toMutableList(), pos)
    }

    fun addPluginCall(plugin: String, args: Any, state: Int, pos: Int, match: String) {
        val call = Call("plugin", mutableListOf(plugin, args, state, match), pos)
        callWriter?.writeCall(call)
    }

    fun finalize() {
        callWriter?.finalise()
        if (status["section"] as Boolean) {
            val last_call = calls.last()
            calls.add(Call("section_close", mutableListOf(), last_call.pos))
        }
        if (rewriteBlocks) {
            val B = Block()
            calls = B.process(calls)
        }
        Event.createAndTrigger("PARSER_HANDLER_DONE", this)
        calls.add(0, Call("document_start", mutableListOf(), 0))
        val last_call = calls.last()
        calls.add(Call("document_end", mutableListOf(), last_call.pos))
    }

    fun fetch(): Any? {
        val call = calls.iterator().next()
        if (call != false) {
            calls.iterator().next()
            return call
        }
        return false
    }

    fun nestingTag(match: String, state: Int, pos: Int, name: String) {
        when (state) {
            DOKU_LEXER_ENTER -> addCall(name + "_open", mutableListOf(), pos)
            DOKU_LEXER_EXIT -> addCall(name + "_close", mutableListOf(), pos)
            DOKU_LEXER_UNMATCHED -> addCall("cdata", mutableListOf(match), pos)
        }
    }

    fun plugin(match: String, state: Int, pos: Int, pluginname: String): Boolean {
        var data = arrayOf(match)
        val plugin = plugin_load("syntax", pluginname) as SyntaxPlugin?
        if (plugin != null) {
            data = plugin.handle(match, state, pos, this)
        }
        if (data !== false) {
            addPluginCall(pluginname, data, state, pos, match)
        }
        return true
    }

    fun base(match: String, state: Int, pos: Int): Boolean {
        when (state) {
            DOKU_LEXER_UNMATCHED -> {
                addCall("cdata", mutableListOf(match), pos)
                return true
            }
        }
        return false
    }

    fun header(match: String, state: Int, pos: Int): Boolean {
        var title = match.trim()
        var level = 7 - title.count { it == '=' }
        if (level < 1) level = 1
        title = title.trim('=').trim()
        if (status["section"] as Boolean) addCall("section_close", mutableListOf(), pos)
        addCall("header", mutableListOf(title, level, pos), pos)
        addCall("section_open", mutableListOf(level), pos)
        status["section"] = true
        return true
    }

    fun notoc(match: String, state: Int, pos: Int): Boolean {
        addCall("notoc", mutableListOf(), pos)
        return true
    }

    fun nocache(match: String, state: Int, pos: Int): Boolean {
        addCall("nocache", mutableListOf(), pos)
        return true
    }

    fun linebreak(match: String, state: Int, pos: Int): Boolean {
        addCall("linebreak", mutableListOf(), pos)
        return true
    }

    fun eol(match: String, state: Int, pos: Int): Boolean {
        addCall("eol", mutableListOf(), pos)
        return true
    }

    fun hr(match: String, state: Int, pos: Int): Boolean {
        addCall("hr", mutableListOf(), pos)
        return true
    }

    fun strong(match: String, state: Int, pos: Int): Boolean {
        nestingTag(match, state, pos, "strong")
        return true
    }

    fun emphasis(match: String, state: Int, pos: Int): Boolean {
        nestingTag(match, state, pos, "emphasis")
        return true
    }

    fun underline(match: String, state: Int, pos: Int): Boolean {
        nestingTag(match, state, pos, "underline")
        return true
    }

    fun monospace(match: String, state: Int, pos: Int): Boolean {
        nestingTag(match, state, pos, "monospace")
        return true
    }

    fun subscript(match: String, state: Int, pos: Int): Boolean {
        nestingTag(match, state, pos, "subscript")
        return true
    }

    fun superscript(match: String, state: Int, pos: Int): Boolean {
        nestingTag(match, state, pos, "superscript")
        return true
    }

    fun deleted(match: String, state: Int, pos: Int): Boolean {
        nestingTag(match, state, pos, "deleted")
        return true
    }

    fun unformatted(match: String, state: Int, pos: Int): Boolean {
        if (state == DOKU_LEXER_UNMATCHED) {
            addCall("unformatted", mutableListOf(match), pos)
        }
        return true
    }

    fun quote(match: String, state: Int, pos: Int): Boolean {
        when (state) {
            DOKU_LEXER_ENTER -> {
                callWriter = Quote(callWriter)
                addCall("quote_start", mutableListOf(match), pos)
            }
            DOKU_LEXER_EXIT -> {
                addCall("quote_end", mutableListOf(), pos)
                val reWriter = callWriter as Lists
                callWriter = reWriter.process()
            }
            DOKU_LEXER_MATCHED -> addCall("quote_newline", mutableListOf(match), pos)
            DOKU_LEXER_UNMATCHED -> addCall("cdata", mutableListOf(match), pos)
        }
        return true
    }

    fun file(match: String, state: Int, pos: Int): Boolean {
        return code(match, state, pos, "file")
    }

    fun acronym(match: String, state: Int, pos: Int): Boolean {
        addCall("acronym", mutableListOf(match), pos)
        return true
    }

    fun smiley(match: String, state: Int, pos: Int): Boolean {
        addCall("smiley", mutableListOf(match), pos)
        return true
    }

    fun wordblock(match: String, state: Int, pos: Int): Boolean {
        addCall("wordblock", mutableListOf(match), pos)
        return true
    }

    fun entity(match: String, state: Int, pos: Int): Boolean {
        addCall("entity", mutableListOf(match), pos)
        return true
    }

    fun multiplyentity(match: String, state: Int, pos: Int): Boolean {
        val matches = Regex("\\d+").findAll(match).map { it.value }.toList()
        addCall("multiplyentity", mutableListOf(matches[0], matches[1]), pos)
        return true
    }

    fun singlequoteopening(match: String, state: Int, pos: Int): Boolean {
        addCall("singlequoteopening", mutableListOf(), pos)
        return true
    }

    fun singlequoteclosing(match: String, state: Int, pos: Int): Boolean {
        addCall("singlequoteclosing", mutableListOf(), pos)
        return true
    }

    fun apostrophe(match: String, state: Int, pos: Int): Boolean {
        addCall("apostrophe", mutableListOf(), pos)
        return true
    }

    fun doublequoteopening(match: String, state: Int, pos: Int): Boolean {
        addCall("doublequoteopening", mutableListOf(), pos)
        status["doublequote"] = (status["doublequote"] as Int) + 1
        return true
    }

    fun doublequoteclosing(match: String, state: Int, pos: Int): Boolean {
        if (status["doublequote"] as Int <= 0) {
            doublequoteopening(match, state, pos)
        } else {
            addCall("doublequoteclosing", mutableListOf(), pos)
            status["doublequote"] = maxOf(0, (status["doublequote"] as Int) - 1)
        }
        return true
    }

    fun camelcaselink(match: String, state: Int, pos: Int): Boolean {
        addCall("camelcaselink", mutableListOf(match), pos)
        return true
    }

    fun filelink(match: String, state: Int, pos: Int): Boolean {
        addCall("filelink", mutableListOf(match, null), pos)
        return true
    }

    fun windowssharelink(match: String, state: Int, pos: Int): Boolean {
        addCall("windowssharelink", mutableListOf(match, null), pos)
        return true
    }

    fun media(match: String, state: Int, pos: Int): Boolean {
        val p = Doku_Handler_Parse_Media(match)
        addCall(
            p["type"] as String,
            arrayOf(
                p["src"],
                p["title"],
                p["align"],
                p["width"],
                p["height"],
                p["cache"],
                p["linking"]
            ),
            pos
        )
        return true
    }

    fun externallink(match: String, state: Int, pos: Int): Boolean {
        var url = match
        var title: String? = null
        if (url.substring(0, 3) == "ftp" && (url.substring(0, 6) != "ftp://")) {
            title = url
            url = "ftp://$url"
        }
        if (url.substring(0, 3) == "www" && (url.substring(0, 7) != "http://")) {
            title = url
            url = "http://$url"
        }
        addCall("externallink", mutableListOf(url, title), pos)
        return true
    }

    fun emaillink(match: String, state: Int, pos: Int): Boolean {
        val email = match.replace(Regex("^<|>$"), "")
        addCall("emaillink", mutableListOf(email, null), pos)
        return true
    }


    fun table(match: String, state: Int, pos: Int): Boolean {
        when (state) {
            DOKU_LEXER_ENTER -> {
                callWriter = Table(callWriter)
                addCall("table_start", mutableListOf(pos + 1), pos)
                if (match.trim() == "^") {
                    addCall("tableheader", mutableListOf(), pos)
                } else {
                    addCall("tablecell", mutableListOf(), pos)
                }
            }
            DOKU_LEXER_EXIT -> {
                addCall("table_end", mutableListOf(pos), pos)
                val reWriter = callWriter as Table
                callWriter = reWriter.process()
            }
            DOKU_LEXER_UNMATCHED -> {
                if (match.trim() != "") {
                    addCall("cdata", mutableListOf(match), pos)
                }
            }
            DOKU_LEXER_MATCHED -> {
                if (match == " ") {
                    addCall("cdata", mutableListOf(match), pos)
                } else if (Regex(":::").containsMatchIn(match)) {
                    addCall("rowspan", mutableListOf(match), pos)
                } else if (Regex("\t+").containsMatchIn(match)) {
                    addCall("table_align", mutableListOf(match), pos)
                } else if (Regex(" {2,}").containsMatchIn(match)) {
                    addCall("table_align", mutableListOf(match), pos)
                } else if (match == "\n|") {
                    addCall("table_row", mutableListOf(), pos)
                    addCall("tablecell", mutableListOf(), pos)
                } else if (match == "\n^") {
                    addCall("table_row", mutableListOf(), pos)
                    addCall("tableheader", mutableListOf(), pos)
                } else if (match == "|") {
                    addCall("tablecell", mutableListOf(), pos)
                } else if (match == "^") {
                    addCall("tableheader", mutableListOf(), pos)
                }
            }
        }
        return true
    }

    fun rss(match: String, state: Int, pos: Int): Boolean {
        val link = match.replace(Regex("^\\{\\{rss>|\\}\\}$"), "")
        val (link, params) = sexplode(" ", link, 2, "")
        val p = mutableMapOf<String, Any>()
        if (Regex("\\b(\\d+)\\b").containsMatchIn(params)) {
            p["max"] = Regex("\\b(\\d+)\\b").find(params)!!.groupValues[1].toInt()
        } else {
            p["max"] = 8
        }
        p["reverse"] = Regex("rev").containsMatchIn(params)
        p["author"] = Regex("\\b(by|author)").containsMatchIn(params)
        p["date"] = Regex("\\b(date)").containsMatchIn(params)
        p["details"] = Regex("\\b(desc|detail)").containsMatchIn(params)
        p["nosort"] = Regex("\\b(nosort)\\b").containsMatchIn(params)
        if (Regex("\\b(\\d+)([dhm])\\b").containsMatchIn(params)) {
            val period = mapOf('d' to 86400, 'h' to 3600, 'm' to 60)
            p["refresh"] = maxOf(600, Regex("\\b(\\d+)([dhm])\\b").find(params)!!.groupValues[1].toInt() * period[Regex("\\b(\\d+)([dhm])\\b").find(params)!!.groupValues[2]]!!)
        } else {
            p["refresh"] = 14400
        }
        addCall("rss", mutableListOf(link, p), pos)
        return true
    }

    fun internallink(match: String, state: Int, pos: Int): Boolean {
        val link = match.replace(Regex("^\\[\\[|\\]\\]$"), "")
        val link = sexplode("|", link, 2)
        if (link[1] == null) {
            link[1] = null
        } else if (Regex("^\\{\\{[^}]+\\}\\}$").containsMatchIn(link[1]!!)) {
            link[1] = Doku_Handler_Parse_Media(link[1]!!)
        }
        link[0] = link[0].trim()
        if (link_isinterwiki(link[0])) {
            val interwiki = sexplode(">", link[0], 2, "")
            addCall("interwikilink", mutableListOf(link[0], link[1], interwiki[0].toLowerCase(), interwiki[1]), pos)
        } else if (Regex("^\\\\\\\\[^\\\\]+?\\\\").containsMatchIn(link[0])) {
            addCall("windowssharelink", mutableListOf(link[0], link[1]), pos)
        } else if (Regex("^([a-z0-9\\-\\.+]+?)://", RegexOption.IGNORE_CASE).containsMatchIn(link[0])) {
            addCall("externallink", mutableListOf(link[0], link[1]), pos)
        } else if (Regex("<${PREG_PATTERN_VALID_EMAIL}>").containsMatchIn(link[0])) {
            addCall("emaillink", mutableListOf(link[0], link[1]), pos)
        } else if (Regex("!^#.+!").containsMatchIn(link[0])) {
            addCall("locallink", mutableListOf(link[0].substring(1), link[1]), pos)
        } else {
            addCall("internallink", mutableListOf(link[0], link[1]), pos)
        }
        return true
    }

    fun parse_highlight_options(options: String): MutableMap<String, Any>? {
        val result = mutableMapOf<String, Any>()
        val matches = Regex("(\\w+(?:=\"[^\"]*\"))|(\\w+(?:=[^\\s]*))|(\\w+[^=\\s\\]])\\s*").findAll(options)
        for (match in matches) {
            val equal_sign = match.value.indexOf('=')
            if (equal_sign == -1) {
                val key = match.value.trim()
                result[key] = 1
            } else {
                val key = match.value.substring(0, equal_sign)
                val value = match.value.substring(equal_sign + 1)
                val value = value.trim('"')
                if (value.length > 0) {
                    result[key] = value
                } else {
                    result[key] = 1
                }
            }
        }
        result = result.filterKeys { setOf("enable_line_numbers", "start_line_numbers_at", "highlight_lines_extra", "enable_keyword_links").contains(it) }.toMutableMap()
        if (result.containsKey("enable_line_numbers")) {
            if (result["enable_line_numbers"] == "false") {
                result["enable_line_numbers"] = false
            }
            result["enable_line_numbers"] = (result["enable_line_numbers"] as String).toBoolean()
        }
        if (result.containsKey("highlight_lines_extra")) {
            result["highlight_lines_extra"] = (result["highlight_lines_extra"] as String).split(",").map { it.toInt() }.filter { it > 0 }.distinct()
        }
        if (result.containsKey("start_line_numbers_at")) {
            result["start_line_numbers_at"] = (result["start_line_numbers_at"] as String).toInt()
        }
        if (result.containsKey("enable_keyword_links")) {
            if (result["enable_keyword_links"] == "false") {
                result["enable_keyword_links"] = false
            }
            result["enable_keyword_links"] = (result["enable_keyword_links"] as String).toBoolean()
        }
        if (result.isEmpty()) {
            return null
        }
        return result
    }

    fun footnote(match: String, state: Int, pos: Int): Boolean {
        if (!footnote) footnote = false
        when (state) {
            DOKU_LEXER_ENTER -> {
                if (footnote) {
                    addCall("cdata", mutableListOf(match), pos)
                    break
                }
                footnote = true
                callWriter = Nest(callWriter, "footnote_close")
                addCall("footnote_open", mutableListOf(), pos)
            }
            DOKU_LEXER_EXIT -> {
                if (!footnote) {
                    addCall("cdata", mutableListOf(match), pos)
                    break
                }
                footnote = false
                addCall("footnote_close", mutableListOf(), pos)
                val reWriter = callWriter as Nest
                callWriter = reWriter.process()
            }
            DOKU_LEXER_UNMATCHED -> {
                addCall("cdata", mutableListOf(match), pos)
            }
        }
        return true
    }

    fun listblock(match: String, state: Int, pos: Int): Boolean {
        when (state) {
            DOKU_LEXER_ENTER -> {
                callWriter = Lists(callWriter)
                addCall("list_open", mutableListOf(match), pos)
            }
            DOKU_LEXER_EXIT -> {
                addCall("list_close", mutableListOf(), pos)
                val reWriter = callWriter as Lists
                callWriter = reWriter.process()
            }
            DOKU_LEXER_MATCHED -> {
                addCall("list_item", mutableListOf(match), pos)
            }
            DOKU_LEXER_UNMATCHED -> {
                addCall("cdata", mutableListOf(match), pos)
            }
        }
        return true
    }

    fun code(match: String, state: Int, pos: Int, type: String = "code"): Boolean {
        if (state == DOKU_LEXER_UNMATCHED) {
            val matches = sexplode(">", match, 2, "")
            val options = Regex("\\[.*\\]").find(matches[0])?.value
            if (options != null) {
                matches[0] = matches[0].replace(options, "")
            }
            val param = matches[0].split("\\s+".toRegex(), 2).toMutableList()
            while (param.size < 2) param.add(null)
            if (param[0] == "html") param[0] = "html4strict"
            if (param[0] == "-") param[0] = null
            param.add(0, matches[1])
            if (options != null) {
                param.add(parse_highlight_options(options))
            }
            addCall(type, param, pos)
        }
        return true
    }

    fun preformatted(match: String, state: Int, pos: Int): Boolean {
        when (state) {
            DOKU_LEXER_ENTER -> {
                callWriter = Preformatted(callWriter)
                addCall("preformatted_start", mutableListOf(), pos)
            }
            DOKU_LEXER_EXIT -> {
                addCall("preformatted_end", mutableListOf(), pos)
                val reWriter = callWriter as Preformatted
                callWriter = reWriter.process()
            }
            DOKU_LEXER_MATCHED -> {
                addCall("preformatted_newline", mutableListOf(), pos)
            }
            DOKU_LEXER_UNMATCHED -> {
                addCall("preformatted_content", mutableListOf(match), pos)
            }
        }
        return true
    }
}
fun Doku_Handler_Parse_Media(match: String): MutableMap<String, Any> {
    var link = match.replace(Regex("^\\{\\{|\\}\\}$"), "")
    link = sexplode("|", link, 2)
    val ralign = Regex("^ ").containsMatchIn(link[0].toString())
    val lalign = Regex(" $").containsMatchIn(link[0].toString())
    val align = when {
        lalign && ralign -> "center"
        ralign -> "right"
        lalign -> "left"
        else -> null
    }
    if (link[1] == null) {
        link[1] = null
    }
    link[0] = link[0].trim()
    val pos = link[0].lastIndexOf('?')
    val (src, param) = if (pos != -1) {
        link[0].substring(0, pos) to link[0].substring(pos + 1)
    } else {
        link[0] to ""
    }
    val (w, h) = if (Regex("(\\d+)(x(\\d+))?").containsMatchIn(param)) {
        val size = Regex("(\\d+)(x(\\d+))?").find(param)!!.groupValues
        (if (size[1].isNotEmpty()) size[1].toInt() else null) to (if (size[3].isNotEmpty()) size[3].toInt() else null)
    } else {
        null to null
    }
    val linking = when {
        Regex("nolink", RegexOption.IGNORE_CASE).containsMatchIn(param) -> "nolink"
        Regex("direct", RegexOption.IGNORE_CASE).containsMatchIn(param) -> "direct"
        Regex("linkonly", RegexOption.IGNORE_CASE).containsMatchIn(param) -> "linkonly"
        else -> "details"
    }
    val cache = if (Regex("(nocache|recache)", RegexOption.IGNORE_CASE).containsMatchIn(param)) {
        Regex("(nocache|recache)").find(param)!!.groupValues[1]
    } else {
        "cache"
    }
    val call = if (media_isexternal(src) || link_isinterwiki(src)) {
        "externalmedia"
    } else {
        "internalmedia"
    }
    val params = mutableMapOf(
        "type" to call,
        "src" to src,
        "title" to link[1],
        "align" to align,
        "width" to w,
        "height" to h,
        "cache" to cache,
        "linking" to linking
    )
    return params
}

package com.github.medavox.da.parsing.handler

class Table(callWriter: CallWriterInterface) : AbstractRewriter(callWriter) {
    private var tableCalls:MutableList<Call> = mutableListOf()
    private var maxCols = 0
    private var maxRows = 1
    private var currentCols = 0
    private var firstCell = false
    private var lastCellType = "tablecell"
    private var inTableHead = true
    private val currentRow = hashMapOf("tableheader" to 0, "tablecell" to 0)
    private var countTableHeadRows = 0

    override fun finalise() {
        writeCall(Call("table_end", mutableListOf(), calls.last().pos))
        process()
        callWriter.finalise()
//        callWriter = null
    }

    override fun process(): CallWriterInterface {
        for (call in calls) {
            when (call[0]) {
                "table_start" -> {
                    tableStart(call)
                }
                "table_row" -> {
                    tableRowClose(call)
                    tableRowOpen(Call("tablerow_open", call.args, call.pos))
                }
                "tableheader", "tablecell" -> {
                    tableCell(call)
                }
                "table_end" -> {
                    tableRowClose(call)
                    tableEnd(call)
                }
                else -> {
                    tableDefault(call)
                }
            }
        }
        callWriter.writeCalls(tableCalls)
        return callWriter
    }

    private fun tableStart(call: Call) {
        tableCalls.add(Call("table_open", call.args, call.pos))
        tableCalls.add(Call("tablerow_open", mutableListOf(), call.pos))
        firstCell = true
    }

    private fun tableEnd(call: Call) {
        tableCalls.add(Call("table_close", call.args, call.pos))
        finalizeTable()
    }

    private fun tableRowOpen(call: Call) {
        tableCalls.add(call)
        currentCols = 0
        firstCell = true
        lastCellType = "tablecell"
        maxRows++
        if (inTableHead) {
            currentRow["tablecell"] = 0
            currentRow["tableheader"] = 0
        }
    }

    private fun tableRowClose(call: Call) {
        if (inTableHead && (inTableHead = isTableHeadRow())) {
            countTableHeadRows++
        }
        // Strip off final cell opening and anything after it
        var discard: Call
        while (tableCalls.isNotEmpty()) {
            discard = tableCalls.removeAt(tableCalls.size - 1)
            if (discard[0] == "tablecell_open" || discard[0] == "tableheader_open") {
                break
            }
            if (currentRow[discard[0]] != null) {
                currentRow[discard[0]] = currentRow[discard[0]]!! - 1
            }
        }
        tableCalls.add(Call("tablerow_close", mutableListOf(), call.pos))
        if (currentCols > maxCols) {
            maxCols = currentCols
        }
    }

    private fun isTableHeadRow(): Boolean {
        val td = currentRow["tablecell"]
        val th = currentRow["tableheader"]
        if (th == null || td!! > 2) return false
        if (2 * td > th) return false
        return true
    }

    private fun tableCell(call: Call) {
        if (inTableHead) {
            currentRow[call.handler!!] = currentRow[call[0]]!! + 1
        }
        if (!firstCell) {
            // Increase the span
            val lastCall = tableCalls[tableCalls.size - 1]
            // A cell call which follows an open cell means an empty cell so span
            if (lastCall[0] == "tablecell_open" || lastCall[0] == "tableheader_open") {
                tableCalls.add(Call("colspan", mutableListOf(), call.pos))
            }
            tableCalls.add(Call(lastCellType + "_close", mutableListOf(), call.pos))
            tableCalls.add(Call(call.handler + "_open", mutableListOf(1, null, 1), call.pos))
            lastCellType = call[0] as String
        } else {
            tableCalls.add(Call(call.handler + "_open", mutableListOf(1, null, 1), call.pos))
            lastCellType = call[0] as String
            firstCell = false
        }
        currentCols++
    }

    private fun tableDefault(call: Call) {
        tableCalls.add(call)
    }

    private fun finalizeTable() {
        // Add the max cols and rows to the table opening
        if (tableCalls[0][0] == "table_open") {
            // Adjust to num cols not num col delimeters
            tableCalls[0].args.add(maxCols - 1)
            tableCalls[0].args.add(maxRows)
            tableCalls[0].args.add(tableCalls[0].args.removeAt(0))
        } else {
            error("First element in table call list is not table_open")
        }
        var lastRow = 0
        var lastCell = 0
        val cellKey = hashMapOf<Int, Int>()
        val toDelete = mutableListOf<Int>()
        // if still in tableheader, then there can be no table header
        // as all rows can't be within <THEAD>
        if (inTableHead) {
            inTableHead = false
            countTableHeadRows = 0
        }
        // Look for the colspan elements and increment the colspan on the
        // previous non-empty opening cell. Once done, delete all the cells
        // that contain colspans
        for (key in 0 until tableCalls.size) {
            val call = tableCalls[key]
            when (call[0]) {
                "table_open" -> {
                    if (countTableHeadRows != 0) {
                        tableCalls.add(key + 1, Call("tablethead_open", mutableListOf(), call.pos))
                    }
                }
                "tablerow_open" -> {
                    lastRow++
                    lastCell = 0
                }
                "tablecell_open", "tableheader_open" -> {
                    lastCell++
                    cellKey[lastRow] = lastCell
                }
                "table_align" -> {
                    val prev = tableCalls[key - 1][0] in listOf("tablecell_open", "tableheader_open")
                    val next = tableCalls[key + 1][0] in listOf("tablecell_close", "tableheader_close")
                    // If the cell is empty, align left
                    if (prev && next) {
                        tableCalls[key - 1][1][1] = "left"
                        // If the previous element was a cell open, align right
                    } else if (prev) {
                        tableCalls[key - 1][1][1] = "right"
                        // If the next element is the close of an element, align either center or left
                    } else if (next) {
                        if (tableCalls[cellKey[lastRow]!!][1][1] == "right") {
                            tableCalls[cellKey[lastRow]!!][1][1] = "center"
                        } else {
                            tableCalls[cellKey[lastRow]!!][1][1] = "left"
                        }
                    }
                    // Now convert the whitespace back to cdata
                    tableCalls[key][0] = "cdata"
                }
                "colspan" -> {
                    tableCalls[key - 1][1][0] = false
                    for (i in key - 2 downTo cellKey[lastRow]!!) {
                        if (tableCalls[i][0] == "tablecell_open" ||
                            tableCalls[i][0] == "tableheader_open"
                        ) {
                            if (tableCalls[i][1][0] != false) {
                                tableCalls[i][1][0] = tableCalls[i][1][0] as Int + 1
                                break
                            }
                        }
                    }
                    toDelete.add(key - 1)
                    toDelete.add(key)
                    toDelete.add(key + 1)
                }
                "rowspan" -> {
                    if (tableCalls[key - 1][0] == "cdata") {
                        // ignore rowspan if previous call was cdata (text mixed with :::)
                        // we don't have to check next call as that wont match regex
                        tableCalls[key][0] = "cdata"
                    } else {
                        var spanningCell: Int? = null
                        // can't cross thead/tbody boundary
                        if (countTableHeadRows == 0 || lastRow - 1 != countTableHeadRows) {
                            for (i in lastRow - 1 downTo 0) {
                                if (tableCalls[cellKey[i]!!][0] == "tablecell_open" ||
                                    tableCalls[cellKey[i]!!][0] == "tableheader_open"
                                ) {
                                    if (tableCalls[cellKey[i]!!][1][2] >= lastRow - i) {
                                        spanningCell = i
                                        break
                                    }
                                }
                            }
                        }
                        if (spanningCell == null) {
                            // No spanning cell found, so convert this cell to
                            // an empty one to avoid broken tables
                            tableCalls[key][0] = "cdata"
                            tableCalls[key][1][0] = ""
                            break
                        }
                        tableCalls[cellKey[spanningCell]!!][1][2] = tableCalls[cellKey[spanningCell]!!][1][2] as Int + 1
                        tableCalls[key - 1][1][2] = false
                        toDelete.add(key - 1)
                        toDelete.add(key)
                        toDelete.add(key + 1)
                    }
                }
                "tablerow_close" -> {
                    // Fix broken tables by adding missing cells
                    val moreCalls = mutableListOf<Call>()
                    while (++lastCell < maxCols) {
                        moreCalls.add(Call("tablecell_open", mutableListOf(1, null, 1), call.pos))
                        moreCalls.add(Call("cdata", mutableListOf(""), call.pos))
                        moreCalls.add(Call("tablecell_close", mutableListOf(), call.pos))
                    }
                    val moreCallsLength = moreCalls.size
                    if (moreCallsLength != 0) {
                        tableCalls.addAll(key, moreCalls)
                        key += moreCallsLength
                    }
                    if (countTableHeadRows == lastRow) {
                        tableCalls.add(key + 1, Call("tablethead_close", mutableListOf<Any>(), call.pos))
                    }
                }
            }
        }
        // condense cdata
        val cnt = tableCalls.size
        var key = 0
        while (key < cnt) {
            if (tableCalls[key][0] == "cdata") {
                val ckey = key
                key++
                while (tableCalls[key][0] == "cdata") {
                    tableCalls[ckey][1][0] = tableCalls[ckey][1][0] as String + tableCalls[key][1][0]
                    toDelete.add(key)
                    key++
                }
                continue
            }
            key++
        }
        for (delete in toDelete) {
            tableCalls.removeAt(delete)
        }
        tableCalls = tableCalls.filterNotNull().toMutableList()
    }
}

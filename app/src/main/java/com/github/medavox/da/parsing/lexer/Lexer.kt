package com.github.medavox.da.parsing.lexer

import com.github.medavox.da.DOKU_LEXER_ENTER
import com.github.medavox.da.DOKU_LEXER_EXIT
import com.github.medavox.da.DOKU_LEXER_MATCHED
import com.github.medavox.da.DOKU_LEXER_SPECIAL
import com.github.medavox.da.DOKU_LEXER_UNMATCHED
import java.util.regex.Pattern
import com.github.medavox.da.parser.Doku_Handler

class Lexer(private val handler: Doku_Handler, private val start: String = "accept", private val case: Boolean = false) {
    private val regexes: MutableMap<String, ParallelRegex> = mutableMapOf()
    private val modeStack: StateStack = StateStack(start)
    private val modeHandlers: MutableMap<String, String> = mutableMapOf()

    init {
        regexes[start] = ParallelRegex(case)
    }

    fun addPattern(pattern: String, mode: String = "accept") {
        if (!regexes.containsKey(mode)) {
            regexes[mode] = ParallelRegex(case)
        }
        regexes[mode]?.addPattern(pattern)
    }

    fun addEntryPattern(pattern: String, mode: String, newMode: String) {
        if (!regexes.containsKey(mode)) {
            regexes[mode] = ParallelRegex(case)
        }
        regexes[mode]?.addPattern(pattern, newMode)
    }

    fun addExitPattern(pattern: String, mode: String) {
        if (!regexes.containsKey(mode)) {
            regexes[mode] = ParallelRegex(case)
        }
        regexes[mode]?.addPattern(pattern, "__exit")
    }

    fun addSpecialPattern(pattern: String, mode: String, special: String) {
        if (!regexes.containsKey(mode)) {
            regexes[mode] = ParallelRegex(case)
        }
        regexes[mode]?.addPattern(pattern, "_$special")
    }

    fun mapHandler(mode: String, handler: String) {
        modeHandlers[mode] = handler
    }

    /**
     * Splits the page text into tokens.
     *
     * Will fail if the handlers report an error or if no content is consumed. If successful then each
     * unparsed and parsed token invokes a call to the held listener.
     *
     * @param string $raw        Raw HTML text.
     * @return boolean           True on success, else false.
     */
    fun parse(raw: String): Boolean {
        if (handler == null) {
            return false
        }
        val initialLength = raw.length
        var length = initialLength
        var pos = 0
        while (true) {
            val parsed = reduce(raw)
            if (parsed is Boolean) {
                if (!parsed) {
                    return false
                }
                break
            }
            val (unmatched, matched, mode) = parsed as Triple<String, String, String>
            val currentLength = raw.length
            val matchPos = initialLength - currentLength - matched.length
            if (!dispatchTokens(unmatched, matched, mode, pos, matchPos)) {
                return false
            }
            if (currentLength == length) {
                return false
            }
            length = currentLength
            pos = initialLength - currentLength
        }
        if (parsed == null) {
            return false
        }
        return invokeHandler(raw, DOKU_LEXER_UNMATCHED, pos)
    }

    fun getModeStack(): StateStack {
        return modeStack
    }

    private fun dispatchTokens(unmatched: String, matched: String, mode: String, initialPos: Int, matchPos: Int): Boolean {
        if (!invokeHandler(unmatched, DOKU_LEXER_UNMATCHED, initialPos)) {
            return false
        }
        if (isModeEnd(mode)) {
            if (!invokeHandler(matched, DOKU_LEXER_EXIT, matchPos)) {
                return false
            }
            return modeStack.leave()
        }
        if (isSpecialMode(mode)) {
            modeStack.enter(decodeSpecial(mode))
            if (!invokeHandler(matched, DOKU_LEXER_SPECIAL, matchPos)) {
                return false
            }
            return modeStack.leave()
        }
        if (mode is String) {
            modeStack.enter(mode)
            return invokeHandler(matched, DOKU_LEXER_ENTER, matchPos)
        }
        return invokeHandler(matched, DOKU_LEXER_MATCHED, matchPos)
    }

    private fun isModeEnd(mode: String): Boolean {
        return mode == "__exit"
    }

    private fun isSpecialMode(mode: String): Boolean {
        return mode.startsWith("_")
    }

    private fun decodeSpecial(mode: String): String {
        return mode.substring(1)
    }

    private fun invokeHandler(content: String, isMatch: Int, pos: Int): Boolean {
        if (content.isNullOrEmpty()) {
            return true
        }
        var handler = modeStack.getCurrent()
        if (modeHandlers.containsKey(handler)) {
            handler = modeHandlers[handler]!!
        }
        if (handler.startsWith("plugin_")) {
            val (handlerName, plugin) = handler.split("_", limit = 2)
            return this.handler.handlerName(content, isMatch, pos, plugin)
        }
        return this.handler.handler(content, isMatch, pos)
    }

    /**
     * Tries to match a chunk of text and if successful removes the recognised chunk and any leading
     * unparsed data. Empty strings will not be matched.
     *
     * @param string $raw         The subject to parse. This is the content that will be eaten.
     * @return array|bool         Three item list of unparsed content followed by the
     *                            recognised token and finally the action the parser is to take.
     *                            True if no match, false if there is a parsing error.
     */
    private fun reduce(raw: String): Any? {
        if (!regexes.containsKey(modeStack.getCurrent())) {
            return false
        }
        if (raw == "") {
            return true
        }
        val regex = regexes[modeStack.getCurrent()] ?: return false
        val action = regex.split(raw)
        if (action != null) {
            val (unparsed, match, newRaw) = action
            return Triple(unparsed, match, action)
        }
        return true
    }

    companion object {
        fun escape(str: String): String {
            val chars = arrayOf(
                "/\\\\/",
                "/\\./",
                "/\\+/",
                "/\\*/",
                "/\\?/",
                "/\\[/",
                "/\\^/",
                "/\\]/",
                "/\\$/",
                "/\\{/",
                "/\\}/",
                "/\\=/",
                "/\\!/",
                "/\\</",
                "/\\>/",
                "/\\|/",
                "/\\:/"
            )
            val escaped = arrayOf(
                "\\\\\\\\",
                "\\.",
                "\\+",
                "\\*",
                "\\?",
                "\\[",
                "\\^",
                "\\]",
                "\\$",
                "\\{",
                "\\}",
                "\\=",
                "\\!",
                "\\<",
                "\\>",
                "\\|",
                "\\:"
            )
            var result = str
            for (i in chars.indices) {
                result = result.replace(Regex(chars[i]), escaped[i])
            }
            return result
        }
    }
}

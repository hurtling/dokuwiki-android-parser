package com.github.medavox.da.parsing.parsermode

import com.github.medavox.da.parsing.parsermode.AbstractMode

/**
 * Implements the 640x480 replacement
 */
class Multiplyentity : AbstractMode() {
    /** @inheritdoc */
    override fun connectTo(mode: String) {
        this.Lexer.addSpecialPattern(
            "(?<=\\b)(?:[1-9]|\\d{2,})[xX]\\d+(?=\\b)",
            mode,
            "multiplyentity"
        )
    }
    /** @inheritdoc */
    override fun getSort(): Int {
        return 270
    }
}

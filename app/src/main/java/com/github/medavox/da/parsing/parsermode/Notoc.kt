package com.github.medavox.da.parsing.parsermode

import com.github.medavox.da.parsing.parsermode.AbstractMode

class Notoc : AbstractMode() {
    override fun connectTo(mode: String) {
        this.Lexer.addSpecialPattern("~~NOTOC~~", mode, "notoc")
    }

    override fun getSort(): Int {
        return 30
    }
}

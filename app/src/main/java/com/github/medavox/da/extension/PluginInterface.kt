package com.github.medavox.da.extension

interface PluginInterface {
    fun getInfo(): Map<String, String>
    fun getPluginType(): String
    fun getPluginName(): String
    fun getPluginComponent(): String
    fun getLang(id: String): String
    fun locale_xhtml(id: String): String
    fun localFN(id: String, ext: String = "txt"): String
    fun setupLocale()
    fun getConf(setting: String, notset: Any = false): Any
    fun loadConfig()
    fun loadHelper(name: String, msg: Boolean = true): PluginInterface?
    fun email(email: String, name: String = "", clazz: String = "", more: String = ""): String
    fun external_link(link: String, title: String = "", clazz: String = "", target: String = "", more: String = ""): String
    fun render_text(text: String, format: String = "xhtml"): String?
    fun isSingleton(): Boolean
}

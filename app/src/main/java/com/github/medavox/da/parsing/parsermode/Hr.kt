package com.github.medavox.da.parsing.parsermode

import com.github.medavox.da.parsing.parsermode.AbstractMode

class Hr : AbstractMode() {
    override fun connectTo(mode: String) {
        this.Lexer.addSpecialPattern("\n[ \t]*-{4,}[ \t]*(?=\n)", mode, "hr")
    }

    override fun getSort(): Int {
        return 160
    }
}

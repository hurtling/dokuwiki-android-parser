package com.github.medavox.da.parser

import com.github.medavox.da.ChangeLog.MediaChangeLog
import com.github.medavox.da.File.MediaResolver
import com.github.medavox.da.File.PageResolver
import com.github.medavox.da.DOKU_LF

/**
 * Renderer for XHTML output
 *
 * This is DokuWiki's main renderer used to display page content in the wiki
 *
 * @author Harry Fuecks <hfuecks@gmail.com>
 * @author Andreas Gohr <andi@splitbrain.org>
 *
 */
open class Doku_Renderer_xhtml : Doku_Renderer() {
    /** @var MutableList store the table of contents */
    public var toc: MutableList<String> = mutableListOf()
    /** @var MutableList A stack of section edit data */
    protected var sectionedits: MutableList<MutableMap<String, Any>> = mutableListOf()
    /** @var Int last section edit id, used by startSectionEdit */
    protected var lastsecid: Int = 0
    /** @var MutableList a list of footnotes, list starts at 1! */
    protected var footnotes: MutableList<String> = mutableListOf()
    /** @var Int current section level */
    protected var lastlevel: Int = 0
    /** @var MutableList section node tracker */
    protected var node: MutableList<Int> = mutableListOf(0, 0, 0, 0, 0)
    /** @var String temporary $doc store */
    protected var store: String = ""
    /** @var MutableMap<String, Int> global counter, for table classes etc. */
    protected var _counter: MutableMap<String, Int> = mutableMapOf()
    /** @var Int counts the code and file blocks, used to provide download links */
    protected var _codeblock: Int = 0
    /** @var MutableList<String> list of allowed URL schemes */
    protected var schemes: MutableList<String>? = null

    /**
     * Register a new edit section range
     *
     * @param start Int The byte position for the edit start
     * @param data MutableMap<String, Any> Associative array with section data:
     *                       Key 'name': the section name/title
     *                       Key 'target': the target for the section edit,
     *                                     e.g. 'section' or 'table'
     *                       Key 'hid': header id
     *                       Key 'codeblockOffset': actual code block index
     *                       Key 'start': set in startSectionEdit(),
     *                                    do not set yourself
     *                       Key 'range': calculated from 'start' and
     *                                    $key in finishSectionEdit(),
     *                                    do not set yourself
     * @return String A marker class for the starting HTML element
     *
     * @author Adrian Lang <lang@cosmocode.de>
     */
    public fun startSectionEdit(start: Int, data: MutableMap<String, Any>): String {
        if (!data is MutableMap<String, Any>) {
            msg(
                String.format(
                    "startSectionEdit: $data \"%s\" is NOT an array! One of your plugins needs an update.",
                    hsc((String) $data)
                ), -1
            )
            // @deprecated 2018-04-14, backward compatibility
            val args = func_get_args()
            data = mutableMapOf()
            if(args[1] != null) data["target"] = args[1]
            if(args[2] != null) data["name"] = args[2]
            if(args[3] != null) data["hid"] = args[3]
        }
        data["secid"] = ++this.lastsecid
        data["start"] = start
        this.sectionedits.add(data)
        return "sectionedit" + data["secid"]
    }

    /**
     * Finish an edit section range
     *
     * @param end Int The byte position for the edit end; null for the rest of the page
     * @param hid Int
     * @return void
     * @throws Exception
     * @throws Exception
     */
    public fun finishSectionEdit(end: Int? = null, hid: Int? = null) {
        if (this.sectionedits.size == 0) {
            return
        }
        val data = this.sectionedits.removeAt(this.sectionedits.size - 1)
        if (end != null && end <= data["start"]) {
            return
        }
        if (hid != null) {
            data["hid"] += hid
        }
        data["range"] = data["start"].toString() + "-" + (if (end == null) "" else end.toString())
        data.remove("start")
        this.doc += "<!-- EDIT" + hsc(json_encode(data)) + " -->"
    }

    /**
     * Returns the format produced by this renderer.
     *
     * @return String always 'xhtml'
     */
    override fun getFormat(): String {
        return "xhtml"
    }

    /**
     * Initialize the document
     *
     * @return void
     */
    override fun document_start() {
        //reset some internals
        this.toc = mutableListOf()
    }

    /**
     * Finalize the document
     *
     * @return void
     */
    override fun document_end() {
        // Finish open section edits.
        while (this.sectionedits.size > 0) {
            if (this.sectionedits[this.sectionedits.size - 1]["start"] <= 1) {
                // If there is only one section, do not write a section edit
                // marker.
                this.sectionedits.removeAt(this.sectionedits.size - 1)
            } else {
                this.finishSectionEdit()
            }
        }
        if (this.footnotes.size > 0) {
            this.doc += "<div class=\"footnotes\">" + DOKU_LF
            for (id in this.footnotes.indices) {
                val footnote = this.footnotes[id]
                // check its not a placeholder that indicates actual footnote text is elsewhere
                if (footnote.substring(0, 5) != "@@FNT") {
                    // open the footnote and set the anchor and backlink
                    this.doc += "<div class=\"fn\">"
                    this.doc += "<sup><a href=\"#fnt__" + id + "\" id=\"fn__" + id + "\" class=\"fn_bot\">"
                    this.doc += id + ")</a></sup> " + DOKU_LF
                    // get any other footnotes that use the same markup
                    val alt = this.footnotes.indices.filter { this.footnotes[it] == "@@FNT$id" }
                    if (alt.size > 0) {
                        for (ref in alt) {
                            // set anchor and backlink for the other footnotes
                            this.doc += ", <sup><a href=\"#fnt__" + ref + "\" id=\"fn__" + ref + "\" class=\"fn_bot\">"
                            this.doc += ref + ")</a></sup> " + DOKU_LF
                        }
                    }
                    // add footnote markup and close this footnote
                    this.doc += "<div class=\"content\">" + footnote + "</div>"
                    this.doc += "</div>" + DOKU_LF
                }
            }
            this.doc += "</div>" + DOKU_LF
        }
        // Prepare the TOC
        val conf = $conf
        if (
            this.info["toc"] &&
            this.toc.size > 0 &&
            conf["tocminheads"] && this.toc.size >= conf["tocminheads"]
        ) {
            val TOC = this.toc
        }
        // make sure there are no empty paragraphs
        this.doc = preg_replace("#<p>\s*</p>#", "", this.doc)
    }

    /**
     * Add an item to the TOC
     *
     * @param id String the hash link
     * @param text String the text to display
     * @param level Int the nesting level
     * @return void
     */
    public override fun toc_additem(id: String, text: String, level: Int) {
        val conf = $conf
        //handle TOC
        if (level >= conf["toptoclevel"] && level <= conf["maxtoclevel"]) {
            this.toc.add(html_mktocitem(id, text, level - conf["toptoclevel"] + 1))
        }
    }

    /**
     * Render a heading
     *
     * @param text String the text to display
     * @param level Int header level
     * @param pos Int byte position in the original source
     * @param returnonly Boolean whether to return html or write to doc attribute
     * @return void|string writes to doc attribute or returns html depends on returnonly
     */
    public fun header(text: String, level: Int, pos: Int, returnonly: Boolean = false) {
        val conf = $conf
        if (text.isBlank()) return //skip empty headlines
        val hid = this._headerToLink(text, true)
        //only add items within configured levels
        this.toc_additem(hid, text, level)
        // adjust node to reflect hierarchy of levels
        this.node[level - 1]++
        if (level < this.lastlevel) {
            for (i in 0 until this.lastlevel - level) {
                this.node[this.lastlevel - i - 1] = 0
            }
        }
        this.lastlevel = level
        if (
            level <= conf["maxseclevel"] &&
            this.sectionedits.size > 0 &&
            this.sectionedits[this.sectionedits.size - 1]["target"] === "section"
        ) {
            this.finishSectionEdit(pos - 1)
        }
        // build the header
        var header = DOKU_LF + "<h" + level
        if (level <= conf["maxseclevel"]) {
            val data = mutableMapOf<String, Any>()
            data["target"] = "section"
            data["name"] = text
            data["hid"] = hid
            data["codeblockOffset"] = this._codeblock
            header += " class=\"" + this.startSectionEdit(pos, data) + "\""
        }
        header += " id=\"" + hid + "\">"
        header += this._xmlEntities(text)
        header += "</h$level>" + DOKU_LF
        if (returnonly) {
            return header
        } else {
            this.doc += header
        }
    }

    /**
     * Open a new section
     *
     * @param level Int section level (as determined by the previous header)
     * @return void
     */
    public override fun section_open(level: Int) {
        this.doc += "<div class=\"level$level\">" + DOKU_LF
    }

    /**
     * Close the current section
     *
     * @return void
     */
    public override fun section_close() {
        this.doc += DOKU_LF + "</div>" + DOKU_LF
    }

    /**
     * Render plain text data
     *
     * @param text String
     * @return void
     */
    public override fun cdata(text: String) {
        this.doc += this._xmlEntities(text)
    }

    /**
     * Open a paragraph
     *
     * @return void
     */
    public override fun p_open() {
        this.doc += DOKU_LF + "<p>" + DOKU_LF
    }

    /**
     * Close a paragraph
     *
     * @return void
     */
    public override fun p_close() {
        this.doc += DOKU_LF + "</p>" + DOKU_LF
    }

    /**
     * Create a line break
     *
     * @return void
     */
    public override fun linebreak() {
        this.doc += "<br/>" + DOKU_LF
    }

    /**
     * Create a horizontal line
     *
     * @return void
     */
    public override fun hr() {
        this.doc += "<hr />" + DOKU_LF
    }

    /**
     * Start strong (bold) formatting
     *
     * @return void
     */
    public override fun strong_open() {
        this.doc += "<strong>"
    }

    /**
     * Stop strong (bold) formatting
     *
     * @return void
     */
    public override fun strong_close() {
        this.doc += "</strong>"
    }

    /**
     * Start emphasis (italics) formatting
     *
     * @return void
     */
    public override fun emphasis_open() {
        this.doc += "<em>"
    }

    /**
     * Stop emphasis (italics) formatting
     *
     * @return void
     */
    public override fun emphasis_close() {
        this.doc += "</em>"
    }

    /**
     * Start underline formatting
     *
     * @return void
     */
    public override fun underline_open() {
        this.doc += "<em class=\"u\">"
    }

    /**
     * Stop underline formatting
     *
     * @return void
     */
    public override fun underline_close() {
        this.doc += "</em>"
    }

    /**
     * Start monospace formatting
     *
     * @return void
     */
    public override fun monospace_open() {
        this.doc += "<code>"
    }

    /**
     * Stop monospace formatting
     *
     * @return void
     */
    public override fun monospace_close() {
        this.doc += "</code>"
    }

    /**
     * Start a subscript
     *
     * @return void
     */
    public override fun subscript_open() {
        this.doc += "<sub>"
    }

    /**
     * Stop a subscript
     *
     * @return void
     */
    public override fun subscript_close() {
        this.doc += "</sub>"
    }

    /**
     * Start a superscript
     *
     * @return void
     */
    public override fun superscript_open() {
        this.doc += "<sup>"
    }

    /**
     * Stop a superscript
     *
     * @return void
     */
    public override fun superscript_close() {
        this.doc += "</sup>"
    }

    /**
     * Start deleted (strike-through) formatting
     *
     * @return void
     */
    public override fun deleted_open() {
        this.doc += "<del>"
    }

    /**
     * Stop deleted (strike-through) formatting
     *
     * @return void
     */
    public override fun deleted_close() {
        this.doc += "</del>"
    }

    /**
     * Callback for footnote start syntax
     *
     * All following content will go to the footnote instead of
     * the document. To achieve this the previous rendered content
     * is moved to $store and $doc is cleared
     *
     * @return void
     */
    public override fun footnote_open() {
        // move current content to store and record footnote
        this.store = this.doc
        this.doc = ""
    }

    /**
     * Callback for footnote end syntax
     *
     * All rendered content is moved to the $footnotes array and the old
     * content is restored from $store again
     *
     * @return void
     */
    public override fun footnote_close() {
        /** @var $fnid Int takes track of seen footnotes, assures they are unique even across multiple docs FS#2841 */
        val fnid = 0
        // assign new footnote id (we start at 1)
        fnid++
        // recover footnote into the stack and restore old content
        val footnote = this.doc
        this.doc = this.store
        this.store = ""
        // check to see if this footnote has been seen before
        val i = this.footnotes.indexOf(footnote)
        if (i == -1) {
            // its a new footnote, add it to the $footnotes array
            this.footnotes[fnid] = footnote
        } else {
            // seen this one before, save a placeholder
            this.footnotes[fnid] = "@@FNT" + i
        }
        // output the footnote reference and link
        this.doc += "<sup><a href=\"#fn__" + fnid + "\" id=\"fnt__" + fnid + "\" class=\"fn_top\">" + fnid + ")</a></sup>"
    }

    /**
     * Open an unordered list
     *
     * @param classes String|String[] css classes - have to be valid, do not pass unfiltered user input
     * @return void
     */
    public fun listu_open(classes: String? = null) {
        val classStr = if (classes != null) {
            if (classes is MutableList<String>) classes.joinToString(" ") else classes
        } else ""
        this.doc += "<ul$classStr>" + DOKU_LF
    }

    /**
     * Close an unordered list
     *
     * @return void
     */
    public override fun listu_close() {
        this.doc += "</ul>" + DOKU_LF
    }

    /**
     * Open an ordered list
     *
     * @param classes String|String[] css classes - have to be valid, do not pass unfiltered user input
     * @return void
     */
    public fun listo_open(classes: String? = null) {
        val classStr = if (classes != null) {
            if (classes is MutableList<String>) classes.joinToString(" ") else classes
        } else ""
        this.doc += "<ol$classStr>" + DOKU_LF
    }

    /**
     * Close an ordered list
     *
     * @return void
     */
    public override fun listo_close() {
        this.doc += "</ol>" + DOKU_LF
    }

        /**
     * Open a list item
     *
     * @param level the nesting level
     * @param node true when a node; false when a leaf
     */
    public override fun listitem_open(level: Int, node: Boolean = false) {
        val branching = if (node) " node" else ""
        doc += "<li class=\"level$level$branching\">"
    }

    /**
     * Close a list item
     */
    public override fun listitem_close() {
        doc += "</li>\n"
    }

    /**
     * Start the content of a list item
     */
    public override fun listcontent_open() {
        doc += "<div class=\"li\">"
    }

    /**
     * Stop the content of a list item
     */
    public override fun listcontent_close() {
        doc += "</div>\n"
    }

    /**
     * Output unformatted text
     *
     * Defaults to cdata()
     *
     * @param text
     */
    public override fun unformatted(text: String) {
        doc += _xmlEntities(text)
    }

    /**
     * Start a block quote
     */
    public override fun quote_open() {
        doc += "<blockquote><div class=\"no\">\n"
    }

    /**
     * Stop a block quote
     */
    public override fun quote_close() {
        doc += "</div></blockquote>\n"
    }

    /**
     * Output preformatted text
     *
     * @param text
     */
    public override fun preformatted(text: String) {
        doc += "<pre class=\"code\">${text.trim()}\n</pre>\n"
    }

    /**
     * Display text as file content, optionally syntax highlighted
     *
     * @param text text to show
     * @param language programming language to use for syntax highlighting
     * @param filename file path label
     * @param options assoziative array with additional geshi options
     */
    public fun file(text: String, language: String? = null, filename: String? = null, options: MutableList<Any>? = null) {
        _highlight("file", text, language, filename, options)
    }

    /**
     * Display text as code content, optionally syntax highlighted
     *
     * @param text text to show
     * @param language programming language to use for syntax highlighting
     * @param filename file path label
     * @param options assoziative array with additional geshi options
     */
    public fun code(text: String, language: String? = null, filename: String? = null, options: MutableList<Any>? = null) {
        _highlight("code", text, language, filename, options)
    }

    /**
     * Use GeSHi to highlight language syntax in code and file blocks
     *
     * @param type code|file
     * @param text text to show
     * @param language programming language to use for syntax highlighting
     * @param filename file path label
     * @param options assoziative array with additional geshi options
     */
    public fun _highlight(type: String, text: String, language: String? = null, filename: String? = null, options: MutableList<Any>? = null) {
        val ID = globalVariable("ID")
        val lang = globalVariable("lang")
        val INPUT = globalVariable("INPUT")
        val language = (language ?: "").replace(PREG_PATTERN_VALID_LANGUAGE.toRegex(), "")
        if (filename != null) {
            val ext = mimetype(filename, false).firstOrNull()
            var classValue = ext?.replace("/[^_\\-a-z0-9]+/i".toRegex(), "_")
            classValue = "mediafile mf_$classValue"
            var offset = 0
            if (INPUT.has("codeblockOffset")) {
                offset = INPUT.str("codeblockOffset")
            }
            doc += "<dl class=\"$type\">\n"
            doc += "<dt><a href=\"" +
                    exportlink(
                        ID,
                        "code",
                        mapOf("codeblock" to (offset + _codeblock))
                    ) + "\" title=\"${lang["download"]}\" class=\"$classValue\">"
            doc += hsc(filename)
            doc += "</a></dt>\n<dd>"
        }
        var text = text
        if (text[0] == "\n") {
            text = text.substring(1)
        }
        if (text.last() == "\n") {
            text = text.substring(0, text.length - 1)
        }
        if (language.isEmpty()) {
            doc += "<pre class=\"$type\">${_xmlEntities(text)}</pre>\n"
        } else {
            var classValue = "code"
            if (type != "code") classValue += " $type"
            doc += "<pre class=\"$classValue $language\">" +
                    p_xhtml_cached_geshi(text, language, "", options) +
                    "</pre>\n"
        }
        if (filename != null) {
            doc += "</dd></dl>\n"
        }
        _codeblock++
    }

    /**
     * Format an acronym
     *
     * Uses acronyms
     *
     * @param acronym
     */
    public override fun acronym(acronym: String) {
        if (acronyms.containsKey(acronym)) {
            val title = _xmlEntities(acronyms[acronym])
            doc += "<abbr title=\"$title\">${_xmlEntities(acronym)}</abbr>"
        } else {
            doc += _xmlEntities(acronym)
        }
    }

    /**
     * Format a smiley
     *
     * Uses smiley
     *
     * @param smiley
     */
    public override fun smiley(smiley: String) {
        if (smileys.containsKey(smiley)) {
            doc += "<img src=\"" + DOKU_BASE + "lib/images/smileys/" + smileys[smiley] +
                    "\" class=\"icon smiley\" alt=\"${_xmlEntities(smiley)}\" />"
        } else {
            doc += _xmlEntities(smiley)
        }
    }

    /**
     * Format an entity
     *
     * Entities are basically small text replacements
     *
     * Uses entities
     *
     * @param entity
     */
    public override fun entity(entity: String) {
        if (entities.containsKey(entity)) {
            doc += entities[entity]
        } else {
            doc += _xmlEntities(entity)
        }
    }

    /**
     * Typographically format a multiply sign
     *
     * Example: ($x=640, $y=480) should result in "640×480"
     *
     * @param x first value
     * @param y second value
     */
    public override fun multiplyentity(x: Any, y: Any) {
        doc += "$x&times;$y"
    }

    /**
     * Render an opening single quote char (language specific)
     */
    public override fun singlequoteopening() {
        val lang = globalVariable("lang")
        doc += lang["singlequoteopening"]
    }

    /**
     * Render a closing single quote char (language specific)
     */
    public override fun singlequoteclosing() {
        val lang = globalVariable("lang")
        doc += lang["singlequoteclosing"]
    }

    /**
     * Render an apostrophe char (language specific)
     */
    public override fun apostrophe() {
        val lang = globalVariable("lang")
        doc += lang["apostrophe"]
    }

    /**
     * Render an opening double quote char (language specific)
     */
    public override fun doublequoteopening() {
        val lang = globalVariable("lang")
        doc += lang["doublequoteopening"]
    }

    /**
     * Render an closinging double quote char (language specific)
     */
    public override fun doublequoteclosing() {
        val lang = globalVariable("lang")
        doc += lang["doublequoteclosing"]
    }

    /**
     * Render a CamelCase link
     *
     * @param link The link name
     * @param returnonly whether to return html or write to doc attribute
     * @return void|string writes to doc attribute or returns html depends on returnonly
     *
     * @see http://en.wikipedia.org/wiki/CamelCase
     */
    public fun camelcaselink(link: String, returnonly: Boolean = false): Any {
        if (returnonly) {
            return internallink(link, link, null, true)
        } else {
            internallink(link, link)
        }
    }

    /**
     * Render a page local link
     *
     * @param hash hash link identifier
     * @param name name for the link
     * @param returnonly whether to return html or write to doc attribute
     * @return void|string writes to doc attribute or returns html depends on returnonly
     */
    public fun locallink(hash: String, name: String? = null, returnonly: Boolean = false): Any {
        val ID = globalVariable("ID")
        val name = _getLinkTitle(name, hash, isImage)
        val hash = _headerToLink(hash)
        val title = "$ID ↵"
        val doc = "<a href=\"#$hash\" title=\"$title\" class=\"wikilink1\">$name</a>"
        if (returnonly) {
            return doc
        } else {
            this.doc += doc
        }
    }

    /**
     * Render an internal Wiki Link
     *
     * search, returnonly & linktype are not for the renderer but are used
     * elsewhere - no need to implement them in other renderers
     *
     * @param id pageid
     * @param name link name
     * @param search adds search url param
     * @param returnonly whether to return html or write to doc attribute
     * @param linktype type to set use of headings
     * @return void|string writes to doc attribute or returns html depends on returnonly
     */
    public fun internallink(id: String, name: String? = null, search: String? = null, returnonly: Boolean = false, linktype: String = "content"): Any {
        val conf = globalVariable("conf")
        val ID = globalVariable("ID")
        val INFO = globalVariable("INFO")
        val params = mutableListOf<Any>()
        val parts = id.split("?", limit = 2)
        if (parts.size == 2) {
            id = parts[0]
            params.addAll(parts[1].split("&"))
        }
        if (id.isEmpty()) {
            id = ID
        }
        val default = _simpleTitle(id)
        id = PageResolver(ID).resolveId(id, date_at, true)
        val exists = page_exists(id, date_at, false, true)
        val link = mutableMapOf<String, Any>()
        val name = _getLinkTitle(name, default, isImage, id, linktype)
        if (!isImage) {
            val classValue = if (exists) "wikilink1" else "wikilink2"
            link["class"] = classValue
            if (!exists) {
                link["rel"] = "nofollow"
            }
        } else {
            link["class"] = "media"
        }
        val (id, hash) = sexplode("#", id, 2)
        if (hash.isNotEmpty()) {
            hash = _headerToLink(hash)
        }
        link["target"] = conf["target"]["wiki"]
        link["style"] = ""
        link["pre"] = ""
        link["suf"] = ""
        link["more"] = "data-wiki-id=\"$id\""
        link["class"] = classValue
        if (date_at != null) {
            params.add("at=${rawurlencode(date_at)}")
        }
        link["url"] = wl(id, params)
        link["name"] = name
        link["title"] = id
        if (search != null) {
            link["url"] += if (conf["userewrite"]) "?" else "&"
            if (search is List<*>) {
                val search = search.map { rawurlencode(it) }
                link["url"] += "s[]=${search.joinToString("&s[]=")}"
            } else {
                link["url"] += "s=${rawurlencode(search)}"
            }
        }
        if (hash.isNotEmpty()) {
            link["url"] += "#$hash"
        }
        if (returnonly) {
            return _formatLink(link)
        } else {
            doc += _formatLink(link)
        }
    }

    /**
     * Render an external link
     *
     * @param url full URL with scheme
     * @param name name for the link, array for media file
     * @param returnonly whether to return html or write to doc attribute
     * @return void|string writes to doc attribute or returns html depends on returnonly
     */
    public fun externallink(url: String, name: Any? = null, returnonly: Boolean = false): Any {
        val conf = globalVariable("conf")
        val name = _getLinkTitle(name, url, isImage)
        val schemes = if (schemes == null) getSchemes() else schemes
        val (scheme) = url.split("://", limit = 2)
        val scheme = scheme.toLowerCase()
        if (!schemes.contains(scheme)) {
            url = ""
        }
        if (url.isEmpty()) {
            if (returnonly) {
                return name
            } else {
                doc += name
            }
            return
        }
        val classValue = if (!isImage) "urlextern" else "media"
        val link = mutableMapOf<String, Any>()
        link["target"] = conf["target"]["extern"]
        link["style"] = ""
        link["pre"] = ""
        link["suf"] = ""
        link["more"] = ""
        link["class"] = classValue
        link["url"] = url
        link["rel"] = ""
        link["name"] = name
        link["title"] = _xmlEntities(url)
        if (conf["relnofollow"]) {
            link["rel"] += " ugc nofollow"
        }
        if (conf["target"]["extern"]) {
            link["rel"] += " noopener"
        }
        if (returnonly) {
            return _formatLink(link)
        } else {
            doc += _formatLink(link)
        }
    }


    /**
     * Render an interwiki link
     *
     * You may want to use $this->_resolveInterWiki() here
     *
     * @param match original link - probably not much use
     * @param name name for the link, array for media file
     * @param wikiName indentifier (shortcut) for the remote wiki
     * @param wikiUri the fragment parsed from the original link
     * @param returnonly whether to return html or write to doc attribute
     * @return void|string writes to doc attribute or returns html depends on returnonly
     */
    public fun interwikilink(match: String, name: Any, wikiName: String, wikiUri: String, returnonly: Boolean = false): String? {
        val conf = global.conf
        val link = mutableMapOf<String, Any>()
        link["target"] = conf["target"]["interwiki"]
        link["pre"] = ""
        link["suf"] = ""
        link["more"] = ""
        link["name"] = this._getLinkTitle(name, wikiUri, isImage)
        link["rel"] = ""
        //get interwiki URL
        var exists: Any? = null
        val url = this._resolveInterWiki(wikiName, wikiUri, exists)
        if (!isImage) {
            val classValue = wikiName.replace(Regex("[^_\\-a-z0-9]+"), "_")
            link["class"] = "interwiki iw_$classValue"
        } else {
            link["class"] = "media"
        }
        //do we stay at the same server? Use local target
        if (url.startsWith(DOKU_URL) || url.startsWith(DOKU_BASE)) {
            link["target"] = conf["target"]["wiki"]
        }
        if (exists != null && !isImage) {
            if (exists) {
                link["class"] += " wikilink1"
            } else {
                link["class"] += " wikilink2"
                link["rel"] += " nofollow"
            }
        }
        if (conf["target"]["interwiki"]) link["rel"] += " noopener"
        link["url"] = url
        link["title"] = this._xmlEntities(link["url"])
        // output formatted
        if (returnonly) {
            if (url == "") return link["name"] as String
            return this._formatLink(link)
        } else {
            if (url == "") this.doc += link["name"] as String
            else this.doc += this._formatLink(link)
        }
    }

    /**
     * Link to windows share
     *
     * @param url the link
     * @param name name for the link, array for media file
     * @param returnonly whether to return html or write to doc attribute
     * @return void|string writes to doc attribute or returns html depends on returnonly
     */
    public fun windowssharelink(url: String, name: Any? = null, returnonly: Boolean = false): String? {
        val conf = global.conf
        //simple setup
        val link = mutableMapOf<String, Any>()
        link["target"] = conf["target"]["windows"]
        link["pre"] = ""
        link["suf"] = ""
        link["style"] = ""
        link["name"] = this._getLinkTitle(name, url, isImage)
        if (!isImage) {
            link["class"] = "windows"
        } else {
            link["class"] = "media"
        }
        link["title"] = this._xmlEntities(url)
        var urlValue = url.replace("\\", "/")
        urlValue = "file:///$urlValue"
        link["url"] = urlValue
        //output formatted
        if (returnonly) {
            return this._formatLink(link)
        } else {
            this.doc += this._formatLink(link)
        }
    }

    /**
     * Render a linked E-Mail Address
     *
     * Honors conf['mailguard'] setting
     *
     * @param address Email-Address
     * @param name name for the link, array for media file
     * @param returnonly whether to return html or write to doc attribute
     * @return void|string writes to doc attribute or returns html depends on returnonly
     */
    public fun emaillink(address: String, name: Any? = null, returnonly: Boolean = false): String? {
        val conf = global.conf
        //simple setup
        val link = mutableMapOf<String, Any>()
        link["target"] = ""
        link["pre"] = ""
        link["suf"] = ""
        link["style"] = ""
        link["more"] = ""
        link["name"] = this._getLinkTitle(name, "", isImage)
        if (!isImage) {
            link["class"] = "mail"
        } else {
            link["class"] = "media"
        }
        var addressValue = this._xmlEntities(address)
        addressValue = obfuscate(addressValue)
        val title = addressValue
        if (name.isNullOrEmpty()) {
            name = addressValue
        }
        if (conf["mailguard"] == "visible") addressValue = rawurlencode(addressValue)
        link["url"] = "mailto:$addressValue"
        link["name"] = name
        link["title"] = title
        //output formatted
        if (returnonly) {
            return this._formatLink(link)
        } else {
            this.doc += this._formatLink(link)
        }
    }

    /**
     * Render an internal media file
     *
     * @param src media ID
     * @param title descriptive text
     * @param align left|center|right
     * @param width width of media in pixel
     * @param height height of media in pixel
     * @param cache cache|recache|nocache
     * @param linking linkonly|detail|nolink
     * @param return HTML instead of adding to doc
     * @return void|string writes to doc attribute or returns html depends on return
     */
    public fun internalmedia(src: String, title: String? = null, align: String? = null, width: Int? = null,
                             height: Int? = null, cache: String? = null, linking: String? = null, return: Boolean = false): String? {
        val ID = global.ID
        if (src.contains("#")) {
            val (srcValue, hash) = src.split("#", limit = 2)
            src = MediaResolver(ID).resolveId(srcValue, this.date_at, true)
        } else {
            src = MediaResolver(ID).resolveId(src, this.date_at, true)
        }
        val exists = media_exists(src)
        var noLink = false
        val render = (linking == "linkonly")
        val link = this._getMediaLinkConf(src, title, align, width, height, cache, render)
        val (ext, mime) = mimetype(src, false)
        if (mime.startsWith("image") && render) {
            link["url"] = ml(
                src,
                mapOf(
                    "id" to ID,
                    "cache" to cache,
                    "rev" to this._getLastMediaRevisionAt(src)
                ),
                (linking == "direct")
            )
        } else if ((mime == "application/x-shockwave-flash" || media_supportedav(mime)) && render) {
            // don't link movies
            noLink = true
        } else {
            // add file icons
            val classValue = ext.replace(Regex("[^_\\-a-z0-9]+"), "_")
            link["class"] += " mediafile mf_$classValue"
            link["url"] = ml(
                src,
                mapOf(
                    "id" to ID,
                    "cache" to cache,
                    "rev" to this._getLastMediaRevisionAt(src)
                ),
                true
            )
            if (exists) link["title"] += " (${filesize_h(filesize(mediaFN(src)))})"
        }
        if (hash != null) link["url"] += "#$hash"
        //markup non existing files
        if (!exists) {
            link["class"] += " wikilink2"
        }
        //output formatted
        if (return) {
            if (linking == "nolink" || noLink) return link["name"] as String
            else return this._formatLink(link)
        } else {
            if (linking == "nolink" || noLink) this.doc += link["name"] as String
            else this.doc += this._formatLink(link)
        }
    }

    /**
     * Render an external media file
     *
     * @param src full media URL
     * @param title descriptive text
     * @param align left|center|right
     * @param width width of media in pixel
     * @param height height of media in pixel
     * @param cache cache|recache|nocache
     * @param linking linkonly|detail|nolink
     * @param return HTML instead of adding to doc
     * @return void|string writes to doc attribute or returns html depends on return
     */
    public fun externalmedia(src: String, title: String? = null, align: String? = null, width: Int? = null,
                             height: Int? = null, cache: String? = null, linking: String? = null, return: Boolean = false): String? {
        if (link_isinterwiki(src)) {
            val (shortcut, reference) = src.split(">", limit = 2)
            var exists: Any? = null
            val srcValue = this._resolveInterWiki(shortcut, reference, exists)
            if (srcValue == "" && title.isNullOrEmpty()) {
                // make sure at least something will be shown in this case
                title = reference
            }
            src = srcValue
        }
        val (srcValue, hash) = src.split("#", limit = 2)
        var noLink = false
        if (srcValue == "") {
            // only output plaintext without link if there is no src
            noLink = true
        }
        val render = (linking == "linkonly")
        val link = this._getMediaLinkConf(src, title, align, width, height, cache, render)
        link["url"] = ml(src, mapOf("cache" to cache))
        val (ext, mime) = mimetype(src, false)
        if (mime.startsWith("image") && render) {
            // link only jpeg images
            // if (ext != "jpg" && ext != "jpeg") noLink = true
        } else if ((mime == "application/x-shockwave-flash" || media_supportedav(mime)) && render) {
            // don't link movies
            noLink = true
        } else {
            // add file icons
            val classValue = ext.replace(Regex("[^_\\-a-z0-9]+"), "_")
            link["class"] += " mediafile mf_$classValue"
        }
        if (hash != null) link["url"] += "#$hash"
        //output formatted
        if (return) {
            if (linking == "nolink" || noLink) return link["name"] as String
            else return this._formatLink(link)
        } else {
            if (linking == "nolink" || noLink) this.doc += link["name"] as String
            else this.doc += this._formatLink(link)
        }
    }

    /**
     * Renders an RSS feed
     *
     * @param url URL of the feed
     * @param params Finetuning of the output
     *
     * @author Andreas Gohr <andi@splitbrain.org>
     */
    public fun rss(url: String, params: Map<String, Any>): Unit {
        val lang = global.lang
        val conf = global.conf
        val feed = FeedParser()
        feed.set_feed_url(url)
        //disable warning while fetching
        val elvl = if (!defined("DOKU_E_LEVEL")) {
            val elvlValue = error_reporting(E_ERROR)
            elvlValue
        } else {
            null
        }
        val rc = feed.init()
        if (elvl != null) {
            error_reporting(elvl)
        }
        if (params["nosort"]) feed.enable_order_by_date(false)
        //decide on start and end
        val mod: Int
        val start: Int
        val end: Int
        if (params["reverse"]) {
            mod = -1
            start = feed.get_item_quantity() - 1
            end = start - (params["max"] as Int)
            end = if (end < -1) -1 else end
        } else {
            mod = 1
            start = 0
            end = feed.get_item_quantity()
            end = if (end > params["max"] as Int) params["max"] as Int else end
        }
        this.doc += "<ul class=\"rss\">"
        if (rc) {
            for (x in start until end step mod) {
                val item = feed.get_item(x)
                this.doc += "<li><div class=\"li\">"
                val lnkurl = item.get_permalink()
                val title = html_entity_decode(item.get_title(), ENT_QUOTES, "UTF-8")
                // support feeds without links
                if (lnkurl != null) {
                    this.externallink(item.get_permalink(), title)
                } else {
                    this.doc += " ${hsc(item.get_title())}"
                }
                if (params["author"]) {
                    val author = item.get_author(0)
                    if (author != null) {
                        var name = author.get_name()
                        if (name.isNullOrEmpty()) name = author.get_email()
                        if (!name.isNullOrEmpty()) this.doc += " ${lang["by"]} ${hsc(name)}"
                    }
                }
                if (params["date"]) {
                    this.doc += " (${item.get_local_date(conf["dformat"])})"
                }
                if (params["details"]) {
                    var desc = item.get_description()
                    desc = strip_tags(desc)
                    desc = html_entity_decode(desc, ENT_QUOTES, "UTF-8")
                    this.doc += "<div class=\"detail\">"
                    this.doc += hsc(desc)
                    this.doc += "</div>"
                }
                this.doc += "</div></li>"
            }
        } else {
            this.doc += "<li><div class=\"li\">"
            this.doc += "<em>${lang["rssfailed"]}</em>"
            this.externallink(url)
            if (conf["allowdebug"]) {
                this.doc += "<!--${hsc(feed.error)}-->"
            }
            this.doc += "</div></li>"
        }
        this.doc += "</ul>"
    }

    /**
     * Start a table
     *
     * @param maxcols maximum number of columns
     * @param numrows NOT IMPLEMENTED
     * @param pos byte position in the original source
     * @param classes css classes - have to be valid, do not pass unfiltered user input
     */
    public fun table_open(maxcols: Int? = null, numrows: Int? = null, pos: Int? = null, classes: Any? = null): Unit {
        // initialize the row counter used for classes
        this._counter["row_counter"] = 0
        var classValue = "table"
        if (classes != null) {
            if (classes is List<*>) classValue += " ${classes.joinToString(" ")}"
            else classValue += " $classes"
        }
        if (pos != null) {
            val hid = this._headerToLink(classValue, true)
            val data = mutableMapOf<String, Any>()
            data["target"] = "table"
            data["name"] = ""
            data["hid"] = hid
            classValue += " ${this.startSectionEdit(pos, data)}"
        }
        this.doc += "<div class=\"$classValue\"><table class=\"inline\">\n"
    }

    /**
     * Close a table
     *
     * @param pos byte position in the original source
     */
    public fun table_close(pos: Int? = null): Unit {
        this.doc += "</table></div>\n"
        if (pos != null) {
            this.finishSectionEdit(pos)
        }
    }

    /**
     * Open a table header
     */
    public override fun tablethead_open(): Unit {
        this.doc += "\t<thead>\n"
    }

    /**
     * Close a table header
     */
    public override fun tablethead_close(): Unit {
        this.doc += "\t</thead>\n"
    }

    /**
     * Open a table body
     */
    public override fun tabletbody_open(): Unit {
        this.doc += "\t<tbody>\n"
    }

    /**
     * Close a table body
     */
    public override fun tabletbody_close(): Unit {
        this.doc += "\t</tbody>\n"
    }

    /**
     * Open a table footer
     */
    public override fun tabletfoot_open(): Unit {
        this.doc += "\t<tfoot>\n"
    }

    /**
     * Close a table footer
     */
    public override fun tabletfoot_close(): Unit {
        this.doc += "\t</tfoot>\n"
    }

        /**
     * Open a table row
     *
     * @param classes css classes - have to be valid, do not pass unfiltered user input
     */
    public fun tablerow_open(classes: Any? = null) {
        // initialize the cell counter used for classes
        _counter["cell_counter"] = 0
        var classValue = "row" + _counter["row_counter"]++
        if (classes != null) {
            if (classes is MutableList<*>) {
                classValue += " " + classes.joinToString(" ")
            } else {
                classValue += " " + classes
            }
        }
        doc += "\t<tr class=\"$classValue\">\n\t\t"
    }

    /**
     * Close a table row
     */
    public override fun tablerow_close() {
        doc += "\n\t</tr>\n"
    }

    /**
     * Open a table header cell
     *
     * @param colspan
     * @param align left|center|right
     * @param rowspan
     * @param classes css classes - have to be valid, do not pass unfiltered user input
     */
    public fun tableheader_open(colspan: Int = 1, align: String? = null, rowspan: Int = 1, classes: Any? = null) {
        var classValue = "class=\"col" + _counter["cell_counter"]++
        if (align != null) {
            classValue += " $align" + "align"
        }
        if (classes != null) {
            if (classes is MutableList<*>) {
                classValue += " " + classes.joinToString(" ")
            } else {
                classValue += " " + classes
            }
        }
        classValue += "\""
        doc += "<th $classValue"
        if (colspan > 1) {
            _counter["cell_counter"] += colspan - 1
            doc += " colspan=\"$colspan\""
        }
        if (rowspan > 1) {
            doc += " rowspan=\"$rowspan\""
        }
        doc += ">"
    }

    /**
     * Close a table header cell
     */
    public override fun tableheader_close() {
        doc += "</th>"
    }

    /**
     * Open a table cell
     *
     * @param colspan
     * @param align left|center|right
     * @param rowspan
     * @param classes css classes - have to be valid, do not pass unfiltered user input
     */
    public fun tablecell_open(colspan: Int = 1, align: String? = null, rowspan: Int = 1, classes: Any? = null) {
        var classValue = "class=\"col" + _counter["cell_counter"]++
        if (align != null) {
            classValue += " $align" + "align"
        }
        if (classes != null) {
            if (classes is MutableList<*>) {
                classValue += " " + classes.joinToString(" ")
            } else {
                classValue += " " + classes
            }
        }
        classValue += "\""
        doc += "<td $classValue"
        if (colspan > 1) {
            _counter["cell_counter"] += colspan - 1
            doc += " colspan=\"$colspan\""
        }
        if (rowspan > 1) {
            doc += " rowspan=\"$rowspan\""
        }
        doc += ">"
    }

    /**
     * Close a table cell
     */
    public override fun tablecell_close() {
        doc += "</td>"
    }

    /**
     * Returns the current header level.
     * (required e.g. by the filelist plugin)
     *
     * @return The current header level
     */
    public fun getLastlevel(): Int {
        return lastlevel
    }

    // Utility functions

    /**
     * Build a link
     *
     * Assembles all parts defined in link returns HTML for the link
     *
     * @param link attributes of a link
     * @return string
     */
    public fun _formatLink(link: Any): String {
        // make sure the url is XHTML compliant (skip mailto)
        if (!link["url"].startsWith("mailto:")) {
            link["url"] = link["url"].replace("&", "&amp;")
            link["url"] = link["url"].replace("&amp;amp;", "&amp;")
        }
        // remove double encodings in titles
        link["title"] = link["title"].replace("&amp;amp;", "&amp;")
        // be sure there are no bad chars in url or title
        // (we can't do this for name because it can contain an img tag)
        link["url"] = link["url"].replace(">", "%3E").replace("<", "%3C").replace("\"", "%22")
        link["title"] = link["title"].replace(">", "&gt;").replace("<", "&lt;").replace("\"", "&quot;")
        var ret = ""
        ret += link["pre"]
        ret += "<a href=\"${link["url"]}\""
        if (!link["class"].isNullOrEmpty()) ret += " class=\"${link["class"]}\""
        if (!link["target"].isNullOrEmpty()) ret += " target=\"${link["target"]}\""
        if (!link["title"].isNullOrEmpty()) ret += " title=\"${link["title"]}\""
        if (!link["style"].isNullOrEmpty()) ret += " style=\"${link["style"]}\""
        if (!link["rel"].isNullOrEmpty()) ret += " rel=\"${link["rel"].trim()}\""
        if (!link["more"].isNullOrEmpty()) ret += " ${link["more"]}"
        ret += ">"
        ret += link["name"]
        ret += "</a>"
        ret += link["suf"]
        return ret
    }

    /**
     * Renders internal and external media
     *
     * @param src media ID
     * @param title descriptive text
     * @param align left|center|right
     * @param width width of media in pixel
     * @param height height of media in pixel
     * @param cache cache|recache|nocache
     * @param render should the media be embedded inline or just linked
     * @return string
     */
    public fun _media(src: String, title: String? = null, align: String? = null, width: Int? = null,
                      height: Int? = null, cache: String? = null, render: Boolean = true): String {
        var ret = ""
        val (ext, mime) = mimetype(src)
        if (mime.startsWith("image")) {
            // first get the title
            var cap = ""
            if (!title.isNullOrEmpty()) {
                cap = _xmlEntities(title)
            } else if (ext == "jpg" || ext == "jpeg") {
                // try to use the caption from IPTC/EXIF
                val jpeg = JpegMeta(mediaFN(src))
                if (jpeg != null) cap = jpeg.getTitle()
                if (!cap.isNullOrEmpty()) {
                    cap = _xmlEntities(cap)
                }
            }
            if (!render) {
                // if the picture is not supposed to be rendered
                // return the title of the picture
                if (cap.isNullOrEmpty()) {
                    // just show the sourcename
                    cap = _xmlEntities(Utf8.basename(noNS(src)))
                }
                return cap
            }
            // add image tag
            ret += "<img src=\"" + ml(
                src,
                mapOf(
                    "w" to width, "h" to height,
                    "cache" to cache,
                    "rev" to _getLastMediaRevisionAt(src)
                )
            ) + "\""
            ret += " class=\"media$align\""
            ret += " loading=\"lazy\""
            if (!cap.isNullOrEmpty()) {
                ret += " title=\"$cap\""
                ret += " alt=\"$cap\""
            } else {
                ret += " alt=\"\""
            }
            if (width != null)
                ret += " width=\"${_xmlEntities(width.toString())}\""
            if (height != null)
                ret += " height=\"${_xmlEntities(height.toString())}\""
            ret += " />"
        } else if (media_supportedav(mime, "video") || media_supportedav(mime, "audio")) {
            // first get the title
            val cap = if (!title.isNullOrEmpty()) title else null
            if (!render) {
                // if the file is not supposed to be rendered
                // return the title of the file (just the sourcename if there is no title)
                return _xmlEntities(cap ?: Utf8.basename(noNS(src)))
            }
            val att = mutableMapOf<String, Any>()
            att["class"] = "media$align"
            if (cap != null) {
                att["title"] = cap
            }
            if (media_supportedav(mime, "video")) {
                // add video
                ret += _video(src, width, height, att)
            }
            if (media_supportedav(mime, "audio")) {
                // add audio
                ret += _audio(src, att)
            }
        } else if (mime == "application/x-shockwave-flash") {
            if (!render) {
                // if the flash is not supposed to be rendered
                // return the title of the flash
                val cap = if (!title.isNullOrEmpty()) title else Utf8.basename(noNS(src))
                return _xmlEntities(cap)
            }
            val att = mutableMapOf<String, Any>()
            att["class"] = "media$align"
            if (align == "right") att["align"] = "right"
            if (align == "left") att["align"] = "left"
            ret += html_flashobject(
                ml(src, mapOf("cache" to cache), true, "&"), width, height,
                mapOf("quality" to "high"),
                null,
                att,
                _xmlEntities(title)
            )
        } else if (!title.isNullOrEmpty()) {
            // well at least we have a title to display
            ret += _xmlEntities(title)
        } else {
            // just show the sourcename
            ret += _xmlEntities(Utf8.basename(noNS(src)))
        }
        return ret
    }

    /**
     * Escape string for output
     *
     * @param string
     * @return string
     */
    public fun _xmlEntities(string: String): String {
        return hsc(string)
    }

    /**
     * Construct a title and handle images in titles
     *
     * @param title either string title or media array
     * @param default default title if nothing else is found
     * @param isImage will be set to true if it's a media file
     * @param id linked page id (used to extract title from first heading)
     * @param linktype content|navigation
     * @return HTML of the title, might be full image tag or just escaped text
     */
    public fun _getLinkTitle(title: Any, default: String, isImage: Boolean, id: String? = null, linktype: String = "content"): String {
        isImage = false
        if (title is MutableList<*>) {
            isImage = true
            return _imageTitle(title)
        } else if (title.isNullOrEmpty() || title.trim() == "") {
            if (useHeading(linktype) && id != null) {
                val heading = p_get_first_heading(id)
                if (!heading.isBlank()) {
                    return _xmlEntities(heading)
                }
            }
            return _xmlEntities(default)
        } else {
            return _xmlEntities(title)
        }
    }

    /**
     * Returns HTML code for images used in link titles
     *
     * @param img
     * @return HTML img tag or similar
     */
    public fun _imageTitle(img: MutableList<Any>): String {
        val ID = globalVariable
        // some fixes on img['src']
        // see internalmedia() and externalmedia()
        img["src"] = img["src"].split("#")[0]
        if (img["type"] == "internalmedia") {
            img["src"] = MediaResolver(ID).resolveId(img["src"], date_at, true)
        }
        return _media(
            img["src"],
            img["title"],
            img["align"],
            img["width"],
            img["height"],
            img["cache"]
        )
    }

    /**
     * helperfunction to return a basic link to a media
     *
     * used in internalmedia() and externalmedia()
     *
     * @param src media ID
     * @param title descriptive text
     * @param align left|center|right
     * @param width width of media in pixel
     * @param height height of media in pixel
     * @param cache cache|recache|nocache
     * @param render should the media be embedded inline or just linked
     * @return associative array with link config
     */
    public fun _getMediaLinkConf(src: String, title: String, align: String, width: Int, height: Int, cache: String, render: Boolean): Any {
        val conf = globalVariable
        val link = mutableMapOf<String, Any>()
        link["class"] = "media"
        link["style"] = ""
        link["pre"] = ""
        link["suf"] = ""
        link["more"] = ""
        link["target"] = conf["target"]["media"]
        if (conf["target"]["media"]) link["rel"] = "noopener"
        link["title"] = _xmlEntities(src)
        link["name"] = _media(src, title, align, width, height, cache, render)
        return link
    }

    /**
     * Embed video(s) in HTML
     *
     * @param src - ID of video to embed
     * @param width - width of the video in pixels
     * @param height - height of the video in pixels
     * @param atts - additional attributes for the <video> tag
     * @return string
     */
    public fun _video(src: String, width: Int, height: Int, atts: Any? = null): String {
        // prepare width and height
        val att = if (atts != null) atts else mutableMapOf<String, Any>()
        att["width"] = width
        att["height"] = height
        if (att["width"] == null) att["width"] = 320
        if (att["height"] == null) att["height"] = 240
        var posterUrl = ""
        val files = mutableMapOf<String, String>()
        val tracks = mutableMapOf<String, String>()
        val isExternal = media_isexternal(src)
        if (isExternal) {
            // take direct source for external files
            val (_, srcMime) = mimetype(src)
            files[srcMime] = src
        } else {
            // prepare alternative formats
            val extensions = listOf("webm", "ogv", "mp4")
            files.putAll(media_alternativefiles(src, extensions))
            val poster = media_alternativefiles(src, listOf("jpg", "png"))
            tracks.putAll(media_trackfiles(src))
            if (poster.isNotEmpty()) {
                posterUrl = ml(poster.first(), "", true, "&")
            }
        }
        var out = ""
        // open video tag
        out += "<video ${buildAttributes(att)} controls=\"controls\""
        if (posterUrl.isNotEmpty()) out += " poster=\"${hsc(posterUrl)}\""
        out += ">\n"
        var fallback = ""
        // output source for each alternative video format
        for ((mime, file) in files) {
            val url = if (isExternal) file else ml(file, "", true, "&")
            val title = if (att["title"] != null) att["title"] as String else _xmlEntities(Utf8.basename(noNS(file)))
            out += "<source src=\"${hsc(url)}\" type=\"$mime\" />\n"
            // alternative content (just a link to the file)
            fallback += externalmedia(
                file,
                title,
                null,
                null,
                null,
                null,
                "linkonly",
                true
            )
        }
        // output each track if any
        for ((trackid, info) in tracks) {
            val (kind, srclang) = listOf(hsc(info[0]), hsc(info[1]))
            out += "<track kind=\"$kind\" srclang=\"$srclang\" "
            out += "label=\"$srclang\" "
            out += "src=\"${ml(trackid, "", true)}\">\n"
        }
        // finish
        out += fallback
        out += "</video>\n"
        return out
    }

    /**
     * Embed audio in HTML
     *
     * @param src - ID of audio to embed
     * @param atts - additional attributes for the <audio> tag
     * @return string
     */
    public fun _audio(src: String, atts: Any? = null): String {
        val files = mutableMapOf<String, String>()
        val isExternal = media_isexternal(src)
        if (isExternal) {
            // take direct source for external files
            val (_, srcMime) = mimetype(src)
            files[srcMime] = src
        } else {
            // prepare alternative formats
            val extensions = listOf("ogg", "mp3", "wav")
            files.putAll(media_alternativefiles(src, extensions))
        }
        var out = ""
        // open audio tag
        out += "<audio ${buildAttributes(atts)} controls=\"controls\">\n"
        var fallback = ""
        // output source for each alternative audio format
        for ((mime, file) in files) {
            val url = if (isExternal) file else ml(file, "", true, "&")
            val title = if (atts["title"] != null) atts["title"] as String else _xmlEntities(Utf8.basename(noNS(file)))
            out += "<source src=\"${hsc(url)}\" type=\"$mime\" />\n"
            // alternative content (just a link to the file)
            fallback += externalmedia(
                file,
                title,
                null,
                null,
                null,
                null,
                "linkonly",
                true
            )
        }
        // finish
        out += fallback
        out += "</audio>\n"
        return out
    }

    /**
     * _getLastMediaRevisionAt is a helperfunction to internalmedia() and _media()
     * which returns an existing media revision less or equal to rev or date_at
     *
     * @param media_id
     * @return string revision ('' for current)
     */
    protected fun _getLastMediaRevisionAt(media_id: String): String {
        if (date_at.isNullOrEmpty() || media_isexternal(media_id)) return ""
        val changelog = MediaChangeLog(media_id)
        return changelog.getLastRevisionAt(date_at)
    }
}
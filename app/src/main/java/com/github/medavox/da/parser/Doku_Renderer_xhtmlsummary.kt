package com.github.medavox.da.parser

class Doku_Renderer_xhtmlsummary : Doku_Renderer_xhtml() {
    private var sum_paragraphs = 0
    private var sum_capture = true
    private var sum_inSection = false
    private var sum_summary = ""
    private var sum_pageTitle = false

    override fun document_start() {
        doc += "\n<div>\n"
    }

    override fun document_end() {
        doc = sum_summary
        doc += "\n</div>\n"
    }

    override fun header(text: String, level: Int, pos: Int) {
        if (!sum_pageTitle) {
            info["sum_pagetitle"] = text
            sum_pageTitle = true
        }
        doc += "\n<h$level>"
        doc += _xmlEntities(text)
        doc += "</h$level>\n"
    }

    override fun section_open(level: Int) {
        if (sum_capture) {
            sum_inSection = true
        }
    }

    override fun section_close() {
        if (sum_capture && sum_inSection) {
            sum_summary += doc
            sum_capture = false
        }
    }

    override fun p_open() {
        if (sum_capture && sum_paragraphs < 2) {
            sum_paragraphs++
        }
        super.p_open()
    }

    override fun p_close() {
        super.p_close()
        if (sum_capture && sum_paragraphs >= 2) {
            sum_summary += doc
            sum_capture = false
        }
    }
}

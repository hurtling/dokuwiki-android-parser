package com.github.medavox.da.parsing.parsermode

import com.github.medavox.da.parsing.parsermode.AbstractMode

class Header : AbstractMode() {
    override fun connectTo(mode: String) {
        //we're not picky about the closing ones, two are enough
        this.Lexer.addSpecialPattern(
            "[ \t]*={2,}[^\n]+={2,}[ \t]*(?=\\n)",
            mode,
            "header"
        )
    }

    override fun getSort(): Int {
        return 50
    }
}

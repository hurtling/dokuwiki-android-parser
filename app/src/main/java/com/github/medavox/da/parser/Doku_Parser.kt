package com.github.medavox.da.parser

import com.github.medavox.da.debug.PropertyDeprecationHelper
import com.github.medavox.da.parsing.Parser

/**
 * Define various types of modes used by the parser - they are used to
 * populate the list of modes another mode accepts
 */
object PARSER_MODES {
    /**containers are complex modes that can contain many other modes
     hr breaks the principle but they shouldn't be used in tables / lists
     so they are put here*/
    val container = listOf("listblock", "table", "quote", "hr")
    /** some mode are allowed inside the base mode only*/
    val baseonly = listOf("header")
    /** modes for styling text -- footnote behaves similar to styling*/
    val formatting = listOf(
        "strong", "emphasis", "underline", "monospace",
        "subscript", "superscript", "deleted", "footnote"
    )
    /** modes where the token is simply replaced - they can not contain any
     other modes*/
    val substition = listOf(
        "acronym", "smiley", "wordblock", "entity",
        "camelcaselink", "internallink", "media",
        "externallink", "linebreak", "emaillink",
        "windowssharelink", "filelink", "notoc",
        "nocache", "multiplyentity", "quotes", "rss"
    )
    /** modes which have a start and end token but inside which
     no other modes should be applied*/
    val protected = listOf("preformatted", "code", "file")
    /** inside this mode no wiki markup should be applied but lineendings
     and whitespace isn't preserved*/
    val disabled = listOf("unformatted")
    /** used to mark paragraph boundaries*/
    val paragraphs = listOf("eol")
}

/**
 * Class Doku_Parser
 *
 * @deprecated 2018-05-04
 */
class Doku_Parser(handler: Doku_Handler = Doku_Handler()) : Parser(handler) {
    private val propertyDeprecationHelper: PropertyDeprecationHelper = PropertyDeprecationHelper()

    init {
        propertyDeprecationHelper.__set("modes", this, __CLASS__)
        propertyDeprecationHelper.__set("connected", this, __CLASS__)
    }

    override fun __set(name: String, value: Any?) {
        if (name == "Handler") {
            this.handler = value as Doku_Handler
            return
        }
        if (name == "Lexer") {
            this.lexer = value as Doku_Lexer
            return
        }
        propertyDeprecationHelper.__set(name, value)
    }

    override fun __get(name: String): Any? {
        if (name == "Handler") {
            return this.handler
        }
        if (name == "Lexer") {
            return this.lexer
        }
        return propertyDeprecationHelper.__get(name)
    }
}

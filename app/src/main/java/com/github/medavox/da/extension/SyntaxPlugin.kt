package com.github.medavox.da.extension

import com.github.medavox.da.parsing.parsermode.Plugin
import com.github.medavox.da.parser.Doku_Handler
import com.github.medavox.da.parser.Doku_Renderer
import com.github.medavox.da.parser.PARSER_MODES

/**
 * Syntax Plugin Prototype
 *
 * All DokuWiki plugins to extend the parser/rendering mechanism
 * need to inherit from this class
 *
 * @license    GPL 2 (http://www.gnu.org/licenses/gpl.html)
 * @author     Andreas Gohr <andi@splitbrain.org>
 */
abstract class SyntaxPlugin : Plugin() {
    protected var allowedModesSetup = false

    /**
     * Syntax Type
     *
     * Needs to return one of the mode types defined in PARSER_MODES in Parser.php
     *
     * @return string
     */
    abstract fun getType(): String

    /**
     * Allowed Mode Types
     *
     * Defines the mode types for other dokuwiki markup that maybe nested within the
     * plugin's own markup. Needs to return an array of one or more of the mode types
     * defined in PARSER_MODES in Parser.php
     *
     * @return Array<String>
     */
    open fun getAllowedTypes(): Array<String> {
        return arrayOf()
    }

    /**
     * Paragraph Type
     *
     * Defines how this syntax is handled regarding paragraphs. This is important
     * for correct XHTML nesting. Should return one of the following:
     *
     * 'normal' - The plugin can be used inside paragraphs
     * 'block'  - Open paragraphs need to be closed before plugin output
     * 'stack'  - Special case. Plugin wraps other paragraphs.
     *
     * @see Doku_Handler_Block
     *
     * @return string
     */
    open fun getPType(): String {
        return "normal"
    }

    /**
     * Handler to prepare matched data for the rendering process
     *
     * This function can only pass data to render() via its return value - render()
     * may be not be run during the object's current life.
     *
     * Usually you should only need the match param.
     *
     * @param match The text matched by the patterns
     * @param state The lexer state for the match
     * @param pos The character position of the matched text
     * @param handler The Doku_Handler object
     * @return Boolean|Array<Any> Return an array with all data you want to use in render, false don't add an instruction
     */
    abstract fun handle(match: String, state: Int, pos: Int, handler: Doku_Handler): Boolean

    /**
     * Handles the actual output creation.
     *
     * The function must not assume any other of the classes methods have been run
     * during the object's current life. The only reliable data it receives are its
     * parameters.
     *
     * The function should always check for the given output format and return false
     * when a format isn't supported.
     *
     * renderer contains a reference to the renderer object which is
     * currently handling the rendering. You need to use it for writing
     * the output. How this is done depends on the renderer used (specified
     * by format
     *
     * The contents of the data array depends on what the handler() function above
     * created
     *
     * @param format output format being rendered
     * @param renderer the current renderer object
     * @param data data created by handler()
     * @return Boolean rendered correctly? (however, returned value is not used at the moment)
     */
    abstract fun render(format: String, renderer: Doku_Renderer, data: Array<Any>): Boolean

    /**
     *  There should be no need to override this function
     *
     * @param mode
     * @return Boolean
     */
    override fun accepts(mode: String): Boolean {
        if (!allowedModesSetup) {
            val allowedModeTypes = getAllowedTypes()
            for (mt in allowedModeTypes) {
                allowedModes.addAll(PARSER_MODES[mt])
            }
            val idx = allowedModes.indexOf(getClassName(this).substring(7))
            if (idx != -1) {
                allowedModes.removeAt(idx)
            }
            allowedModesSetup = true
        }
        return super.accepts(mode)
    }
}

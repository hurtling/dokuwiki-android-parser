package com.github.medavox.da.parsing.parsermode

import com.github.medavox.da.parser.PARSER_MODES
import com.github.medavox.da.parsing.parsermode.AbstractMode

class Formatting(type: String) : AbstractMode() {
    private val type: String = type
    private val formatting: Map<String, Map<String, Any>> = mapOf(
        "strong" to mapOf(
            "entry" to "\\*\\*(?=.*\\*\\*)",
            "exit" to "\\*\\*",
            "sort" to 70
        ),
        "emphasis" to mapOf(
            "entry" to "//(?=[^\u0000]*[^:])",
            "exit" to "//",
            "sort" to 80
        ),
        "underline" to mapOf(
            "entry" to "__(?=.*__)",
            "exit" to "__",
            "sort" to 90
        ),
        "monospace" to mapOf(
            "entry" to "'(?=.*')",
            "exit" to "'",
            "sort" to 100
        ),
        "subscript" to mapOf(
            "entry" to "<sub>(?=.*</sub>)",
            "exit" to "</sub>",
            "sort" to 110
        ),
        "superscript" to mapOf(
            "entry" to "<sup>(?=.*</sup>)",
            "exit" to "</sup>",
            "sort" to 120
        ),
        "deleted" to mapOf(
            "entry" to "<del>(?=.*</del>)",
            "exit" to "</del>",
            "sort" to 130
        )
    )

    init {
        if (!formatting.containsKey(type)) {
            error("Invalid formatting type $type")
        }
        allowedModes = listOf(
            *(PARSER_MODES.formatting.toTypedArray().filter { it != type }.toTypedArray()),
            *PARSER_MODES.substition.toTypedArray(),
            *PARSER_MODES.disabled.toTypedArray()
        )
    }

    override fun connectTo(mode: String) {
        if (mode == type) {
            return
        }
        Lexer.addEntryPattern(formatting[type]!!["entry"] as String, mode, type)
    }

    override fun postConnect() {
        Lexer.addExitPattern(formatting[type]!!["exit"] as String, type)
    }

    override fun getSort(): Int {
        return formatting[type]!!["sort"] as Int
    }
}

package com.github.medavox.da.parsing.parsermode

import com.github.medavox.da.parsing.parsermode.AbstractMode

class Unformatted : AbstractMode() {
    override fun connectTo(mode: String) {
        this.Lexer.addEntryPattern("<nowiki>(?=.*</nowiki>)", mode, "unformatted")
        this.Lexer.addEntryPattern("%%(?=.*%%)", mode, "unformattedalt")
    }

    override fun postConnect() {
        this.Lexer.addExitPattern("</nowiki>", "unformatted")
        this.Lexer.addExitPattern("%%", "unformattedalt")
        this.Lexer.mapHandler("unformattedalt", "unformatted")
    }

    override fun getSort(): Int {
        return 170
    }
}

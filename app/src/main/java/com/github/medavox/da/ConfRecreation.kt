package com.github.medavox.da

/**I don't want to convert the entire PHP configfile system from dokuwiki,
 * so this file is ad-hoc implementation of the conf keys
 * needed by the code I DO have */

val conf: MutableMap<String, Any?> = mutableMapOf()

/*So far, a manual search has found these keys:
*/

//logdir
//querycdn
//disableactions
//openregister
//resendpasswd
//subscribers
//useheading
//lang
//plugin
//target
//relnofollow
//useslash
//tocminheads
//toptoclevl
//maxseclevl
//userewrite
//interwiki
//mailguard
//allowdebug
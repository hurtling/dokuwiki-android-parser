package com.github.medavox.da.parsing.parsermode

import com.github.medavox.da.parsing.parsermode.AbstractMode

class Quotes : AbstractMode() {
    override fun connectTo(mode: String) {
        val ws = "\\s/\\#~:+=&%@-\\x28\\x29\\]\\[\\{}><\"'".toRegex() // whitespace
        val punc = ";,\\.?!"
        if (conf["typography"] == 2) {
            this.Lexer.addSpecialPattern(
                "(?<=^|[$ws])'(?=[^$ws$punc])",
                mode,
                "singlequoteopening"
            )
            this.Lexer.addSpecialPattern(
                "(?<=^|[^$ws]|[$punc])'(?=$|[$ws$punc])",
                mode,
                "singlequoteclosing"
            )
            this.Lexer.addSpecialPattern(
                "(?<=^|[^$ws$punc])'(?=$|[^$ws$punc])",
                mode,
                "apostrophe"
            )
        }
        this.Lexer.addSpecialPattern(
            "(?<=^|[$ws])\"(?=[^$ws$punc])",
            mode,
            "doublequoteopening"
        )
        this.Lexer.addSpecialPattern(
            "\"",
            mode,
            "doublequoteclosing"
        )
    }

    override fun getSort(): Int {
        return 280
    }
}

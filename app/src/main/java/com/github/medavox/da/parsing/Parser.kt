package com.github.medavox.da.parsing

import com.github.medavox.da.parser.Doku_Handler
import com.github.medavox.da.parsing.lexer.Lexer
import com.github.medavox.da.parsing.parsermode.AbstractMode
import com.github.medavox.da.parsing.parsermode.Base
import com.github.medavox.da.parsing.parsermode.ModeInterface

open class Parser(protected val handler: Doku_Handler) {
    protected var lexer: Lexer? = null
    private val modes: MutableMap<String, AbstractMode> = mutableMapOf()
    private var connected = false


    private fun addBaseMode(baseMode: Base) {
        modes["base"] = baseMode
        if (lexer == null) {
            lexer = Lexer(handler, "base", true)
        } else {
            val l:Lexer = lexer as Lexer
            modes["base"]?.Lexer = l
        }
    }

    fun addMode(name: String, mode: AbstractMode) {
        if (!modes.containsKey("base")) {
            addBaseMode(Base())
        }
        mode.Lexer = (lexer as Lexer)
        modes[name] = mode
    }

    private fun connectModes() {
        if (connected) {
            return
        }
        for (mode in modes.keys) {
            if (mode == "base") {
                continue
            }
            modes[mode]?.preConnect()
            for (cm in modes.keys) {
                if (modes[cm]?.accepts(mode) == true) {
                    modes[mode]?.connectTo(cm)
                }
            }
            modes[mode]?.postConnect()
        }
        connected = true
    }

    fun parse(doc: String): List<Any> {
        connectModes()
        // Normalize CRs and pad doc
        val doc2 = "\n" + doc.replace("\r\n", "\n") + "\n"
        lexer?.parse(doc2)
        handler.finalize()
        return handler.calls
    }
}

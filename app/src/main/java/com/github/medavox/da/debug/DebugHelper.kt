package com.github.medavox.da.debug

import com.github.medavox.da.debug.Doku_Event
import com.github.medavox.da.extension.EventHandler
import com.github.medavox.da.Logger

class DebugHelper {
    companion object {
        const val INFO_DEPRECATION_LOG_EVENT = "INFO_DEPRECATION_LOG"

        /**
         * Check if deprecation messages shall be handled
         *
         * This is either because its logging is not disabled or a deprecation handler was registered
         *
         * @return Boolean
         */
        fun isEnabled(): Boolean {
            val EVENT_HANDLER: EventHandler? = null
            if (!Logger.getInstance(Logger.LOG_DEPRECATED).isLogging() &&
                (EVENT_HANDLER == null || !EVENT_HANDLER.hasHandlerForEvent(INFO_DEPRECATION_LOG_EVENT))
            ) {
                // avoid any work if no one cares
                return false
            }
            return true
        }

        /**
         * Log accesses to deprecated fucntions to the debug log
         *
         * @param alternative (optional) The function or method that should be used instead
         * @param callerOffset (optional) How far the deprecated method is removed from this one
         * @param thing (optional) The deprecated thing, defaults to the calling method
         * @triggers dokuwiki.Debug.INFO_DEPRECATION_LOG_EVENT
         */
        fun dbgDeprecatedFunction(alternative: String = "", callerOffset: Int = 1, thing: String = "") {
            if (!isEnabled()) return
            val backtrace = Thread.currentThread().stackTrace
            var i = 0
            while (i < callerOffset) {
                if (backtrace.size > 1) backtrace.drop(1)
                i += 1
            }
            val (self, call) = backtrace
            var deprecatedThing = thing
            if (deprecatedThing.isEmpty()) {
                deprecatedThing = trim(
                    (!self.className.isNullOrEmpty()).let { if (it) self.className + "::" else "" } +
                            self.methodName + "()", ":"
                )
            }
            triggerDeprecationEvent(
                backtrace,
                alternative,
                deprecatedThing,
                trim(
                    (!call.className.isNullOrEmpty()).let { if (it) call.className + "::" else "" } +
                            call.methodName + "()", ":"
                ),
                self.fileName ?: call.fileName ?: "",
                self.lineNumber ?: call.lineNumber ?: 0
            )
        }

        /**
         * This marks logs a deprecation warning for a property that should no longer be used
         *
         * This is usually called withing a magic getter or setter.
         * For logging deprecated functions or methods see dbgDeprecatedFunction()
         *
         * @param class The class with the deprecated property
         * @param propertyName The name of the deprecated property
         *
         * @triggers dokuwiki.Debug.INFO_DEPRECATION_LOG_EVENT
         */
        fun dbgDeprecatedProperty(class: String, propertyName: String) {
            if (!isEnabled()) return
            val backtrace = Thread.currentThread().stackTrace.drop(1)
            val call = backtrace[1]
            val caller = trim(call.className + "::" + call.methodName + "()", ":")
            val qualifiedName = class + "::$" + propertyName
            triggerDeprecationEvent(
                backtrace,
                "",
                qualifiedName,
                caller,
                backtrace[0].fileName,
                backtrace[0].lineNumber
            )
        }

        /**
         * Trigger a custom deprecation event
         *
         * Usually dbgDeprecatedFunction() or dbgDeprecatedProperty() should be used instead.
         * This method is intended only for those situation where they are not applicable.
         *
         * @param alternative
         * @param deprecatedThing
         * @param caller
         * @param file
         * @param line
         * @param callerOffset How many lines should be removed from the beginning of the backtrace
         */
        fun dbgCustomDeprecationEvent(
            alternative: String,
            deprecatedThing: String,
            caller: String,
            file: String,
            line: Int,
            callerOffset: Int = 1
        ) {
            if (!isEnabled()) return
            val backtrace = Thread.currentThread().stackTrace.drop(callerOffset)
            triggerDeprecationEvent(
                backtrace,
                alternative,
                deprecatedThing,
                caller,
                file,
                line
            )
        }

        private fun triggerDeprecationEvent(
            backtrace: Array<StackTraceElement>,
            alternative: String,
            deprecatedThing: String,
            caller: String,
            file: String,
            line: Int
        ) {
            val data = mapOf(
                "trace" to backtrace,
                "alternative" to alternative,
                "called" to deprecatedThing,
                "caller" to caller,
                "file" to file,
                "line" to line
            )
            val event = Doku_Event(INFO_DEPRECATION_LOG_EVENT, data)
            if (event.advise_before()) {
                var msg = event.data["called"] + " is deprecated. It was called from "
                msg += event.data["caller"] + " in " + event.data["file"] + ":" + event.data["line"]
                if (event.data["alternative"] != null) {
                    msg += " " + event.data["alternative"] + " should be used instead!"
                }
                Logger.getInstance(Logger.LOG_DEPRECATED).log(msg)
            }
            event.advise_after()
        }
    }
}
package com.github.medavox.da.parsing.parsermode

import com.github.medavox.da.parsing.parsermode.AbstractMode

class Media : AbstractMode() {
    override fun connectTo(mode: String) {
        // Word boundaries?
        this.Lexer.addSpecialPattern("\\{\\{(?:[^\\}]|(?:\\}[^\\}]))+\\}\\}", mode, "media")
    }

    override fun getSort(): Int {
        return 320
    }
}

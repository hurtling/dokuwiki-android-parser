package com.github.medavox.da.parsing.parsermode

/**
 * Defines a mode (syntax component) in the Parser
 */
interface ModeInterface {
    /**
     * returns a number used to determine in which order modes are added
     *
     * @return Int
     */
    fun getSort(): Int

    /**
     * Called before any calls to connectTo
     *
     * @return Unit
     */
    fun preConnect()

    /**
     * Connects the mode
     *
     * @param mode: String
     * @return Unit
     */
    fun connectTo(mode: String)

    /**
     * Called after all calls to connectTo
     *
     * @return Unit
     */
    fun postConnect()

    /**
     * Check if given mode is accepted inside this mode
     *
     * @param mode: String
     * @return Boolean
     */
    fun accepts(mode: String): Boolean
}

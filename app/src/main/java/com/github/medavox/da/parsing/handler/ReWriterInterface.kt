package com.github.medavox.da.parsing.handler

import com.github.medavox.da.parsing.handler.CallWriterInterface

abstract class ReWriterInterface
/**
 * ReWriterInterface constructor.
 *
 * This rewriter will be registered as the new call writer in the Handler.
 * The original is passed as parameter
 *
 * @param CallWriterInterface $callWriter the original callwriter
 */(callWriter: CallWriterInterface) : CallWriterInterface {
    abstract val calls: MutableList<Call>

    /**
     * Process any calls that have been added and add them to the
     * original call writer
     *
     * @return CallWriterInterface the original call writer
     */
    abstract fun process(): CallWriterInterface?

    /**
     * Accessor for this rewriter's original CallWriter
     *
     * @return CallWriterInterface
     */
    abstract fun getCallWriter(): CallWriterInterface?
}

package com.github.medavox.da.parsing.parsermode

import com.github.medavox.da.parsing.parsermode.AbstractMode

class Acronym : AbstractMode {
    // A list
    private var acronyms: Array<String> = emptyArray()
    private var pattern: String = ""

    /**
     * Acronym constructor.
     *
     * @param acronyms List of acronyms
     */
    constructor(acronyms: Array<String>) {
        acronyms.sortByDescending { it.length }
        this.acronyms = acronyms
    }

    /** @inheritdoc */
    override fun preConnect() {
        if (acronyms.isEmpty()) return
        val bound = "[\\x00-\\x2f\\x3a-\\x40\\x5b-\\x60\\x7b-\\x7f]"
        val escapedAcronyms = acronyms.map { com.github.medavox.da.parsing.lexer.Lexer.escape(it) }
        pattern = "(?<=^|$bound)(?:" + escapedAcronyms.joinToString("|") + ")(?=$bound)"
    }

    /** @inheritdoc */
    override fun connectTo(mode: String) {
        if (acronyms.isEmpty()) return
        if (pattern.isNotEmpty()) {
            this.Lexer.addSpecialPattern(pattern, mode, "acronym")
        }
    }

    /** @inheritdoc */
    override fun getSort(): Int {
        return 240
    }

    /**
     * sort callback to order by string length descending
     *
     * @param a First string
     * @param b Second string
     *
     * @return Comparison result
     */
    private fun compare(a: String, b: String): Int {
        val aLen = a.length
        val bLen = b.length
        return when {
            aLen > bLen -> -1
            aLen < bLen -> 1
            else -> 0
        }
    }
}

package com.github.medavox.da.extension

import java.io.File

class PluginTrait : PluginInterface {
    private var localised = false
    private var lang = mutableMapOf<String, String>()
    private var configloaded = false
    private var conf = mutableMapOf<String, Any>()

    override fun getInfo(): Map<String, String> {
        val parts = javaClass.name.split("_")
        val info = "DOKU_PLUGIN/${parts[2]}/plugin.info.txt"
        if (File(info).exists()) return confToHash(info)
        msg(
            "getInfo() not implemented in ${javaClass.name} and $info not found.<br />" +
            "Verify you're running the latest version of the plugin. If the problem persists, send a " +
            "bug report to the author of the ${parts[2]} plugin.", -1
        )
        return mapOf(
            "date" to "0000-00-00",
            "name" to "${parts[2]} plugin"
        )
    }

    override fun isSingleton(): Boolean {
        return true
    }

    override fun loadHelper(name: String, msg: Boolean): Any? {
        val obj = plugin_load("helper", name)
        if (obj == null && msg) msg("Helper plugin $name is not available or invalid.", -1)
        return obj
    }

    override fun getPluginType(): String {
        val (t) = javaClass.name.split("_")
        return t
    }

    override fun getPluginName(): String {
        val (_, _, n) = javaClass.name.split("_")
        return n
    }

    override fun getPluginComponent(): String {
        val (_, _, _, c) = javaClass.name.split("_")
        return c
    }

    override fun getLang(id: String): String {
        if (!localised) setupLocale()
        return lang[id] ?: ""
    }

    override fun locale_xhtml(id: String): String {
        return p_cached_output(localFN(id))
    }

    override fun localFN(id: String, ext: String): String {
        val plugin = getPluginName()
        var file = "DOKU_CONF/plugin_lang/$plugin/${conf["lang"]}/$id.$ext"
        if (!File(file).exists()) {
            file = "DOKU_PLUGIN/$plugin/lang/${conf["lang"]}/$id.$ext"
            if (!File(file).exists()) {
                file = "DOKU_PLUGIN/$plugin/lang/en/$id.$ext"
            }
        }
        return file
    }

    override fun setupLocale() {
        if (localised) return
        val path = "DOKU_PLUGIN/${getPluginName()}/lang/"
        val lang = mutableMapOf<String, String>()
        include("$path/en/lang.php")
        for (config_file in config_cascade["lang"]["plugin"]) {
            if (File("$config_file${getPluginName()}/en/lang.php").exists()) {
                include("$config_file${getPluginName()}/en/lang.php")
            }
        }
        if (conf["lang"] != "en") {
            include("$path${conf["lang"]}/lang.php")
            for (config_file in config_cascade["lang"]["plugin"]) {
                if (File("$config_file${getPluginName()}/${conf["lang"]}/lang.php").exists()) {
                    include("$config_file${getPluginName()}/${conf["lang"]}/lang.php")
                }
            }
        }
        this.lang = lang
        localised = true
    }

    override fun getConf(setting: String, notset: Boolean): Any? {
        if (!configloaded) {
            loadConfig()
        }
        return if (conf.containsKey(setting)) {
            conf[setting]
        } else {
            notset
        }
    }

    override fun loadConfig() {
        val defaults = readDefaultSettings()
        val plugin = getPluginName()
        for ((key, value) in defaults) {
            if (conf["plugin"][$plugin][$key] != null) continue
            conf["plugin"][$plugin][$key] = value
        }
        configloaded = true
        this.conf = conf["plugin"][$plugin]
    }

    private fun readDefaultSettings(): Map<String, Any> {
        val path = "DOKU_PLUGIN/${getPluginName()}/conf/"
        val conf = mutableMapOf<String, Any>()
        if (File("$path/default.php").exists()) {
            include("$path/default.php")
        }
        return conf
    }

    override fun email(email: String, name: String, class: String, more: String): String {
        if (email.isEmpty()) return name
        val obfuscatedEmail = obfuscate(email)
        val obfuscatedName = if (name.isEmpty()) obfuscatedEmail else name
        val cssClass = if (class.isEmpty()) "mail" else class
        return "<a href='mailto:$obfuscatedEmail' class='$cssClass' title='$obfuscatedEmail' $more>$obfuscatedName</a>"
    }

    override fun external_link(link: String, title: String, class: String, target: String, more: String): String {
        val obfuscatedLink = htmlentities(link)
        val obfuscatedTitle = if (title.isEmpty()) obfuscatedLink else title
        val obfuscatedTarget = if (target.isEmpty()) conf["target"]["extern"] else target
        val obfuscatedMore = if (conf["relnofollow"]) " rel='nofollow'" else ""
        val cssClass = if (class.isNotEmpty()) " class='$class'" else ""
        val obfuscatedMoreTrimmed = more.trim()
        val obfuscatedMoreFinal = if (obfuscatedMoreTrimmed.isNotEmpty()) " $obfuscatedMoreTrimmed" else ""
        return "<a href='$obfuscatedLink'$cssClass target='$obfuscatedTarget'$obfuscatedMore>$obfuscatedTitle</a>"
    }

    override fun render_text(text: String, format: String): String {
        return p_render(format, p_get_instructions(text), info)
    }
}

package com.github.medavox.da.parsing.parsermode

import com.github.medavox.da.parsing.parsermode.AbstractMode

class Eol : AbstractMode() {
    override fun connectTo(mode: String) {
        val badModes = listOf("listblock", "table")
        if (mode in badModes) {
            return
        }
        // see FS#1652, pattern extended to swallow preceding whitespace to avoid
        // issues with lines that only contain whitespace
        this.Lexer.addSpecialPattern("(?:^[ \t]*)?\n", mode, "eol")
    }

    override fun getSort(): Int {
        return 370
    }
}

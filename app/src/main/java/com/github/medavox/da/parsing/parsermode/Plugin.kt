package com.github.medavox.da.parsing.parsermode

import com.github.medavox.da.parsing.parsermode.AbstractMode

/**
 * @fixme do we need this anymore or could the syntax plugin inherit directly from abstract mode?
 */
abstract class Plugin : AbstractMode() {}

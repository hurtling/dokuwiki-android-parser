package com.github.medavox.da.parsing.parsermode

import com.github.medavox.da.parsing.parsermode.AbstractMode

class Windowssharelink : AbstractMode() {
    private var pattern: String = ""

    override fun preConnect() {
        pattern = "\\\\\\\\w+?(?:\\\\[\\w\\-$]+)+"
    }

    override fun connectTo(mode: String) {
        this.Lexer.addSpecialPattern(
            pattern,
            mode,
            "windowssharelink"
        )
    }

    override fun getSort(): Int {
        return 350
    }
}

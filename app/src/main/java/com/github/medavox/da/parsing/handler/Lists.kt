package com.github.medavox.da.parsing.handler

class Lists(callWriter: CallWriterInterface) : AbstractRewriter(callWriter) {
    private val listCalls: MutableList<Call> = mutableListOf()
    private val listStack: MutableList<Call> = mutableListOf()
    private var initialDepth = 0

    companion object {
        const val NODE = 1
    }

    override fun finalise() {
        writeCall(Call("list_close", mutableListOf(), calls.last().pos))
        process()
        callWriter.finalise()
//        callWriter = null
    }

    override fun process(): CallWriterInterface? {
        for (call in calls) {
            when (call[0]) {
                "list_item" -> listOpen(call)
                "list_open" -> listStart(call)
                "list_close" -> listEnd(call)
                else -> listContent(call)
            }
        }
        callWriter.writeCalls(listCalls)
        return callWriter
    }

    private fun listStart(call: Call) {
        val depth = interpretSyntax(call[1][0], listType)
        initialDepth = depth
        listStack.add(Call(listType, depth, 1))
        listCalls.add(Call("list${listType}_open", mutableListOf(), call.pos))
        listCalls.add(Call("listitem_open", arrayOf(1), call.pos))
        listCalls.add(Call("listcontent_open", mutableListOf(), call.pos))
    }

    private fun listEnd(call: Call) {
        var closeContent = true
        while (listStack.isNotEmpty()) {
            val list = listStack.removeAt(listStack.size - 1)
            if (closeContent) {
                listCalls.add(Call("listcontent_close", mutableListOf(), call.pos))
                closeContent = false
            }
            listCalls.add(Call("listitem_close", mutableListOf(), call.pos))
            listCalls.add(Call("list${list[0]}_close", mutableListOf(), call.pos))
        }
    }

    private fun listOpen(call: Call) {
        val depth = interpretSyntax(call[1][0], listType)
        val end = listStack.last()
        val key = listStack.lastIndex
        if (depth < initialDepth) {
            depth = initialDepth
        }
        if (depth == end[1]) {
            if (listType == end[0]) {
                listCalls.add(Call("listcontent_close", mutableListOf(), call.pos))
                listCalls.add(Call("listitem_close", mutableListOf(), call.pos))
                listCalls.add(Call("listitem_open", arrayOf(depth - 1), call.pos))
                listCalls.add(Call("listcontent_open", mutableListOf(), call.pos))
                listStack[key][2] = listCalls.size - 2
            } else {
                listCalls.add(Call("listcontent_close", mutableListOf(), call.pos))
                listCalls.add(Call("listitem_close", mutableListOf(), call.pos))
                listCalls.add(Call("list${end[0]}_close", mutableListOf(), call.pos))
                listCalls.add(Call("list${listType}_open", mutableListOf(), call.pos))
                listCalls.add(Call("listitem_open", arrayOf(depth - 1), call.pos))
                listCalls.add(v("listcontent_open", mutableListOf(), call.pos))
                listStack.removeAt(listStack.size - 1)
                listStack.add(Call(listType, depth, listCalls.size - 2))
            }
        } else if (depth > end[1]) {
            listCalls.add(Call("listcontent_close", mutableListOf(), call.pos))
            listCalls.add(Call("list${listType}_open", mutableListOf(), call.pos))
            listCalls.add(Call("listitem_open", arrayOf(depth - 1), call.pos))
            listCalls.add(Call("listcontent_open", mutableListOf(), call.pos))
            listCalls[listStack[key][2]][1][1] = NODE
            listStack.add(Call(listType, depth, listCalls.size - 2))
        } else {
            listCalls.add(Call("listcontent_close", mutableListOf(), call.pos))
            listCalls.add(Call("listitem_close", mutableListOf(), call.pos))
            listCalls.add(Call("list${end[0]}_close", mutableListOf(), call.pos))
            listStack.removeAt(listStack.size - 1)
            while (true) {
                val end = listStack.last()
                val key = listStack.lastIndex
                if (end[1] <= depth) {
                    depth = end[1]
                    listCalls.add(Call("listitem_close", mutableListOf(), call.pos))
                    if (end[0] == listType) {
                        listCalls.add(Call("listitem_open", arrayOf(depth - 1), call.pos))
                        listCalls.add(Call("listcontent_open", mutableListOf(), call.pos))
                        listStack[key][2] = listCalls.size - 2
                    } else {
                        listCalls.add(Call("list${end[0]}_close", mutableListOf(), call.pos))
                        listCalls.add(Call("list${listType}_open", mutableListOf(), call.pos))
                        listCalls.add(Call("listitem_open", arrayOf(depth - 1), call.pos))
                        listCalls.add(Call("listcontent_open", mutableListOf(), call.pos))
                        listStack.removeAt(listStack.size - 1)
                        listStack.add(Call(listType, depth, listCalls.size - 2))
                    }
                    break
                } else {
                    listCalls.add(Call("listitem_close", mutableListOf(), call.pos))
                    listCalls.add(Call("list${end[0]}_close", mutableListOf(), call.pos))
                    listStack.removeAt(listStack.size - 1)
                }
            }
        }
    }

    private fun listContent(call: Call) {
        listCalls.add(call)
    }

    private fun interpretSyntax(match: Any, type: Any): Int {
        val typeOut = if (match.toString().endsWith("*")) "u" else "o" //fixme: what does this do???
        return match.toString().replace("\t", "  ").count { it == ' ' } + 1
    }
}

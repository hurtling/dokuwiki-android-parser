package com.github.medavox.da.parsing.handler

import com.github.medavox.da.parsing.handler.CallWriterInterface

/**
 * Generic call writer class to handle nesting of rendering instructions
 * within a render instruction. Also see nest() method of renderer base class
 *
 * @author    Chris Smith <chris@jalakai.co.uk>
 */
class Nest
    /**
     * @param  CallWriterInterface $CallWriter     the parser's current call writer, i.e. the one above us in the chain
     * @param  string     $close          closing instruction name, this is required to properly terminate the
     *                                    syntax mode if the document ends without a closing pattern
     */
    (private val CallWriter: CallWriterInterface, private val close: String = "nest_close") : AbstractRewriter(CallWriter) {
    private var closingInstruction: String = close
    override var calls: MutableList<Call> = mutableListOf()

    override fun writeCall(call: Call) {
        calls.add(call)
    }

    override fun writeCalls(calls: List<Call>) {
        this.calls.addAll(calls)
    }

    override fun finalise() {
        writeCall(Call(closingInstruction, mutableListOf(), calls.last().pos))
        process()
        this.CallWriter.finalise()
        //this.CallWriter = null
    }

    override fun process(): CallWriterInterface? {
        // merge consecutive cdata
        val unmerged_calls = calls
        calls = mutableListOf()

        for (call in unmerged_calls) {
            addCall(call)
        }
        CallWriter.writeCall(Call("nest", mutableListOf(calls), calls.first().pos))
        return CallWriter
    }

    /**
     * @param array $call
     */
    private fun addCall(call: Call) {
        val key = calls.size
        if (key > 0 && call[0] == "cdata" && calls[key-1][0] == "cdata") {
            calls[key-1].args[0] += call.args[0]
        } else if (call[0] == "eol") {
            // do nothing (eol shouldn't be allowed, to counter preformatted fix in #1652 & #1699)
        } else {
            calls.add(call)
        }
    }
}

package com.github.medavox.da.parser
import com.github.medavox.da.extension.Plugin
import com.github.medavox.da.extension.SyntaxPlugin

const val PREG_PATTERN_VALID_LANGUAGE = "#[^a-zA-Z0-9\\-_]#"

abstract class Doku_Renderer : Plugin() {
    val info: MutableMap<String, Boolean> = mutableMapOf(
        "cache" to true,
        "toc" to true
    )
    val smileys = emptyList<String>()
    val entities = emptyList<String>()
    val acronyms = emptyList<String>()
    val interwiki = emptyList<String>()
    var date_at = ""
    protected var headers = emptyList<String>()
    var doc = ""

    fun reset() {
        headers = emptyList()
        doc = ""
        info["cache"] = true
        info["toc"] = true
    }

    override fun isSingleton(): Boolean {
        return false
    }

    abstract fun getFormat(): String

    fun nocache() {
        info["cache"] = false
    }

    fun notoc() {
        info["toc"] = false
    }

    fun plugin(name: String, data: Any, state: String = "", match: String = "") {
        val plugin = plugin_load("syntax", name)
        if (plugin != null) {
            plugin.render(getFormat(), this, data)
        }
    }

    fun nest(instructions: List<List<Any>>) {
        for (instruction in instructions) {
            if (instruction[0] in this::class.members) {
                val method = this::class.members.find { it.name == instruction[0] }
                method?.call(this, instruction[1] ?: emptyList())
            }
        }
    }

    open fun nest_close() {
    }

    // Syntax modes - sub classes will need to implement them to fill doc
    open fun document_start() {
    }

    open fun document_end() {
    }

    open fun render_TOC(): String {
        return ""
    }

    open fun toc_additem(id: String, text: String, level: Int) {
    }

    fun header(text: String, level: Int, pos: Any) {
    }

    open fun section_open(level: Int) {
    }

    open fun section_close() {
    }

    open fun cdata(text: String) {
    }

    open fun p_open() {
    }

    open fun p_close() {
    }

    open fun linebreak() {
    }

    open fun hr() {
    }

    open fun strong_open() {
    }

    open fun strong_close() {
    }

    open fun emphasis_open() {
    }

    open fun emphasis_close() {
    }

    open fun underline_open() {
    }

    open fun underline_close() {
    }

    open fun monospace_open() {
    }

    open fun monospace_close() {
    }

    open fun subscript_open() {
    }

    open fun subscript_close() {
    }

    open fun superscript_open() {
    }

    open fun superscript_close() {
    }

    open fun deleted_open() {
    }

    open fun deleted_close() {
    }

    open fun footnote_open() {
    }

    open fun footnote_close() {
    }

    open fun listu_open() {
    }

    open fun listu_close() {
    }

    open fun listo_open() {
    }

    open fun listo_close() {
    }

    open fun listitem_open(level: Int, node: Boolean = false) {
    }

    open fun listitem_close() {
    }

    open fun listcontent_open() {
    }

    open fun listcontent_close() {
    }

    fun unformatted(text: String) {
        cdata(text)
    }

    open fun preformatted(text: String) {
    }

    open fun quote_open() {
    }

    open fun quote_close() {
    }

    open fun file(text: String, lang: Any? = null, file: Any? = null) {
    }

    open fun code(text: String, lang: Any? = null, file: Any? = null) {
    }

    open fun acronym(acronym: String) {
    }

    open fun smiley(smiley: String) {
    }

    open fun entity(entity: String) {
    }

    open fun multiplyentity(x: Any, y: Any) {
    }

    open fun singlequoteopening() {
    }

    open fun singlequoteclosing() {
    }

    open fun apostrophe() {
    }

    open fun doublequoteopening() {
    }

    open fun doublequoteclosing() {
    }

    open fun camelcaselink(link: String) {
    }

    open fun locallink(hash: String, name: Any? = null) {
    }

    open fun internallink(link: String, title: Any? = null) {
    }

    open fun externallink(link: String, title: Any? = null) {
    }

    open fun rss(url: String, params: Any) {
    }

    open fun interwikilink(link: String, title: Any, wikiName: Any, wikiUri: Any) {
    }

    open fun filelink(link: String, title: Any? = null) {
    }

    open fun windowssharelink(link: String, title: Any? = null) {
    }

    open fun emaillink(address: String, name: Any? = null) {
    }

    fun internalmedia(
        src: String,
        title: Any? = null,
        align: Any? = null,
        width: Any? = null,
        height: Any? = null,
        cache: Any? = null,
        linking: Any? = null
    ) {
    }

    fun externalmedia(
        src: String,
        title: Any? = null,
        align: Any? = null,
        width: Any? = null,
        height: Any? = null,
        cache: Any? = null,
        linking: Any? = null
    ) {
    }

    fun internalmedialink(
        src: String,
        title: Any? = null,
        align: Any? = null,
        width: Any? = null,
        height: Any? = null,
        cache: Any? = null
    ) {
    }

    fun externalmedialink(
        src: String,
        title: Any? = null,
        align: Any? = null,
        width: Any? = null,
        height: Any? = null,
        cache: Any? = null
    ) {
    }

    open fun table_open(maxcols: Any? = null, numrows: Any? = null, pos: Any? = null) {
    }

    open fun table_close(pos: Any? = null) {
    }

    open fun tablethead_open() {
    }

    open fun tablethead_close() {
    }

    open fun tabletbody_open() {
    }

    open fun tabletbody_close() {
    }

    open fun tabletfoot_open() {
    }

    open fun tabletfoot_close() {
    }

    open fun tablerow_open() {
    }

    open fun tablerow_close() {
    }

    open fun tableheader_open(colspan: Int = 1, align: Any? = null, rowspan: Int = 1) {
    }

    open fun tableheader_close() {
    }

    open fun tablecell_open(colspan: Int = 1, align: Any? = null, rowspan: Int = 1) {
    }

    open fun tablecell_close() {
    }

    // util functions, you probably won't need to reimplement them
    fun _headerToLink(title: String, create: Boolean = false): String {
        if (create) {
            return sectionID(title, headers)
        } else {
            val check = emptyList<String>()
            return sectionID(title, check)
        }
    }

    open fun _simpleTitle(name: String): String {
        val conf = emptyMap<String, Any>()
        val (name, hash) = sexplode('#', name, 2)
        if (hash.isNotEmpty()) return hash
        name = if (conf["useslash"]) {
            strtr(name, listOf(';/' to ';:', ';' to ':'))
        } else {
            strtr(name, ';', ':')
        }
        return noNSorNS(name)
    }

    fun _resolveInterWiki(shortcut: String, reference: String, exists: Any? = null): String {
        val url = if (shortcut in interwiki) {
            interwiki[shortcut]
        } else if ("default" in interwiki) {
            shortcut = "default"
            interwiki[shortcut]
        } else {
            shortcut = ""
            ""
        }
        var hash = reference.substringAfterLast('#')
        if (hash.isNotEmpty()) {
            reference = reference.substringBeforeLast('#')
            hash = hash.substring(1)
        }
        if (url.contains(Regex("\\{(URL|NAME|SCHEME|HOST|PORT|PATH|QUERY)\\}"))) {
            url = url.replace("{URL}", rawurlencode(reference))
            url = url.replace("{NAME}", if (url[0] == ':') {
                reference
            } else {
                preg_replace_callback(
                    "/[[\\\\\\]^`{|}#%]/",
                    { match -> rawurlencode(match.groupValues[0]) },
                    reference
                )
            })
            val parsed = parse_url(reference)
            val scheme = parsed["scheme"] ?: ""
            val host = parsed["host"] ?: ""
            val port = parsed["port"] ?: 80
            val path = parsed["path"] ?: ""
            val query = parsed["query"] ?: ""
            url = strtr(url, mapOf(
                "{SCHEME}" to scheme,
                "{HOST}" to host,
                "{PORT}" to port,
                "{PATH}" to path,
                "{QUERY}" to query
            ))
        } else if (url.isNotEmpty()) {
            url += rawurlencode(reference)
        }
        if (url.isNotEmpty() && url[0] == ':') {
            var urlparam = ""
            var id = url
            if (url.contains('?')) {
                val (id, urlparam) = sexplode('?', url, 2, "")
            }
            url = wl(cleanID(id), urlparam)
            exists = page_exists(id)
        }
        if (hash.isNotEmpty()) {
            url += '#' + rawurlencode(hash)
        }
        return url
    }
}

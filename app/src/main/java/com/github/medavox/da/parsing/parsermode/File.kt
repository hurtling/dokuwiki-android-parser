package com.github.medavox.da.parsing.parsermode

import com.github.medavox.da.parsing.parsermode.AbstractMode

class File : AbstractMode() {
    override fun connectTo(mode: String) {
        this.Lexer.addEntryPattern("<file\\b(?=.*</file>)", mode, "file")
    }

    override fun postConnect() {
        this.Lexer.addExitPattern("</file>", "file")
    }

    override fun getSort(): Int {
        return 210
    }
}

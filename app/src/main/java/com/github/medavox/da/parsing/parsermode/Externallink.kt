package com.github.medavox.da.parsing.parsermode

import com.github.medavox.da.getSchemes
import com.github.medavox.da.parsing.parsermode.AbstractMode

class Externallink : AbstractMode() {
    private var schemes: List<String> = listOf()
    private var patterns: Array<String> = emptyArray()

    override fun preConnect() {
        if (patterns.isNotEmpty()) return
        val ltrs = "\\w"
        val gunk = "/\\#~:.?+=&%@!\\-\\[\\]"
        val punc = ".:?\\-;,"
        val host = ltrs + punc
        val any = ltrs + gunk + punc
        schemes = getSchemes()
        for (scheme in schemes) {
            patterns += "\\b(?i)$scheme(?-i)://[$any]+?(?=[$punc]*[^$any])"
        }
        patterns += "(?<![/\\\\])\\b(?i)www?(?-i)\\.[$host]+?\\." +
                "[$host]+?[$any]+?(?=[$punc]*[^$any])"
        patterns += "(?<![/\\\\])\\b(?i)ftp?(?-i)\\.[$host]+?\\." +
                "[$host]+?[$any]+?(?=[$punc]*[^$any])"
    }

    override fun connectTo(mode: String) {
        for (pattern in patterns) {
            Lexer.addSpecialPattern(pattern, mode, "externallink")
        }
    }

    override fun getSort(): Int {
        return 330
    }

    fun getPatterns(): Array<String> {
        return patterns
    }
}

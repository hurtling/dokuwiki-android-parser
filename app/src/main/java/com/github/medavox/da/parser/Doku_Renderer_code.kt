package com.github.medavox.da.parser

import org.apache.commons.io.FilenameUtils
import org.apache.commons.lang3.StringUtils
import java.net.http.HttpHeaders

class Doku_Renderer_code : Doku_Renderer() {
    private var _codeblock = 0

    fun code(text: String, language: String? = null, filename: String = "") {
        val language = language ?: "txt"
        val cleanedLanguage = language.replace(PREG_PATTERN_VALID_LANGUAGE.toRegex(), "")
        val filename = if (filename.isEmpty()) "snippet.$cleanedLanguage" else filename
        val cleanedFilename = FilenameUtils.getBaseName(filename)
        val strippedFilename = StringUtils.stripAccents(cleanedFilename, '_')
        val userAgent = HttpHeaders.USER_AGENT
        val crlf = if (userAgent.contains("Windows")) "\r\n" else "\n"

        if (_codeblock == INPUT.str("codeblock")) {
            println("Content-Type: text/plain; charset=utf-8")
            println("Content-Disposition: attachment; filename=$filename")
            println("X-Robots-Tag: noindex")
            println(text.trim(crlf))
            return
        }

        _codeblock++
    }

    fun file(text: String, language: String? = null, filename: String = "") {
        code(text, language, filename)
    }

    fun document_end() {
        http_status(404)
        println("404 - Not found")
        return
    }

    fun getFormat(): String {
        return "code"
    }
}

package com.github.medavox.da

import com.github.medavox.da.extension.Event

/**
 * Log messages to a daily log file
 */
class Logger(private val facility: String) {
    companion object {
        const val LOG_ERROR = "error"
        const val LOG_DEPRECATED = "deprecated"
        const val LOG_DEBUG = "debug"
        private val instances: MutableMap<String, Logger> = mutableMapOf()

        /**
         * Return a Logger instance for the given facility
         *
         * @param facility The type of log
         * @return Logger
         */
        @JvmStatic
        fun getInstance(facility: String = LOG_ERROR): Logger {
            if (!instances.containsKey(facility)) {
                instances[facility] = Logger(facility)
            }
            return instances[facility]!!
        }
    }

    private var isLogging = true

    /**
     * Logger constructor.
     *
     * @param facility The type of log
     */
    init {
        // Should logging be disabled for this facility?
        val dontlog = conf["dontlog"].split(",").map { it.trim() }
        if (facility in dontlog) {
            isLogging = false
        }
    }

    /**
     * Convenience method to directly log to the error log
     *
     * @param message The log message
     * @param details Any details that should be added to the log entry
     * @param file A source filename if this is related to a source position
     * @param line A line number for the above file
     * @return has a log been written?
     */
    fun error(message: String, details: Any? = null, file: String = "", line: Int = 0): Boolean {
        return getInstance(LOG_ERROR).log(message, details, file, line)
    }

    /**
     * Convenience method to directly log to the debug log
     *
     * @param message The log message
     * @param details Any details that should be added to the log entry
     * @param file A source filename if this is related to a source position
     * @param line A line number for the above file
     * @return has a log been written?
     */
    fun debug(message: String, details: Any? = null, file: String = "", line: Int = 0): Boolean {
        return getInstance(LOG_DEBUG).log(message, details, file, line)
    }

    /**
     * Convenience method to directly log to the deprecation log
     *
     * @param message The log message
     * @param details Any details that should be added to the log entry
     * @param file A source filename if this is related to a source position
     * @param line A line number for the above file
     * @return has a log been written?
     */
    fun deprecated(message: String, details: Any? = null, file: String = "", line: Int = 0): Boolean {
        return getInstance(LOG_DEPRECATED).log(message, details, file, line)
    }

    /**
     * Log a message to the facility log
     *
     * @param message The log message
     * @param details Any details that should be added to the log entry
     * @param file A source filename if this is related to a source position
     * @param line A line number for the above file
     * @triggers LOGGER_DATA_FORMAT can be used to change the logged data or intercept it
     * @return has a log been written?
     */
    fun log(message: String, details: Any? = null, file: String = "", line: Int = 0): Boolean {
        val EVENT_HANDLER = TODO()
        if (!isLogging) {
            return false
        }
        val datetime = System.currentTimeMillis() / 1000
        val data = mapOf(
            "facility" to facility,
            "datetime" to datetime,
            "message" to message,
            "details" to details,
            "file" to file,
            "line" to line,
            "loglines" to emptyList<String>(),
            "logfile" to getLogfile(datetime)
        )
        if (EVENT_HANDLER != null) {
            val event = Event("LOGGER_DATA_FORMAT", data)
            if (event.advise_before()) {
                data["loglines"] = formatLogLines(data)
            }
            event.advise_after()
        } else {
            // The event system is not yet available, to ensure the log isn't lost even on
            // fatal errors, the default action is executed
            data["loglines"] = formatLogLines(data)
        }
        // only log when any data available
        return if (data["loglines"].isNotEmpty()) {
            writeLogLines(data["loglines"] as List<String>, data["logfile"] as String)
        } else {
            false
        }
    }

    /**
     * Is this logging instace actually logging?
     *
     * @return bool
     */
    fun isLogging(): Boolean {
        return isLogging
    }

    /**
     * Formats the given data as loglines
     *
     * @param data Event data from LOGGER_DATA_FORMAT
     * @return the lines to log
     */
    private fun formatLogLines(data: Map<String, Any>): List<String> {
        val (facility, datetime, message, details, file, line) = data
        // details are logged indented
        val loglines = if (details != null) {
            if (details !is String) {
                val jsonDetails = TODO()
                jsonDetails.split("\n").map { "  $it" }
            } else {
                listOf(details)
            }
        } else {
            emptyList()
        }
        // datetime, fileline, message
        val logline = "${java.time.LocalDateTime.ofEpochSecond(datetime, 0, java.time.ZoneOffset.UTC)}\t" +
                if (file.isNotEmpty()) {
                    "$file${if (line != 0) "($line)" else ""}"
                } else {
                    ""
                } +
                "\t$message"
        return listOf(logline) + loglines
    }

    /**
     * Construct the log file for the given day
     *
     * @param date Date to access, false for today
     * @return string
     */
    private fun getLogfile(date: Any?): String {
        val conf = TODO()
        val logdir = conf["logdir"]
        val facilityDir = "$logdir/$facility"
        val formattedDate = if (date != null && date !is Number) {
            java.time.LocalDate.parse(date.toString()).format(java.time.format.DateTimeFormatter.ISO_DATE)
        } else {
            java.time.LocalDate.now().format(java.time.format.DateTimeFormatter.ISO_DATE)
        }
        return "$facilityDir/$formattedDate.log"
    }

    /**
     * Write the given lines to today's facility log
     *
     * @param lines the raw lines to append to the log
     * @param logfile where to write to
     * @return true if the log was written
     */
    private fun writeLogLines(lines: List<String>, logfile: String): Boolean {
        if (DOKU_UNITTEST != null) {
            System.err.println("\n[$facility] ${lines.joinToString("\n")}\n")
        }
        return TODO()
    }
}
package com.github.medavox.da.parsing.handler

class Quote(callWriter: CallWriterInterface) : AbstractRewriter(callWriter) {
    private val quoteCalls: MutableList<Call> = mutableListOf()

    override fun finalise() {
        writeCall(Call("quote_end", listOf(), calls.last().pos))
        process()
        callWriter.finalise()
//        callWriter = null
    }

    override fun process(): CallWriterInterface {
        var quoteDepth = 1
        for (call in calls) {
            when (call[0]) {
                "quote_start" -> {
                    quoteCalls.add(Call("quote_open", listOf(), call.pos))
                }
                "quote_newline" -> {
                    val quoteLength = getDepth(call.args[0])//fixme: might cause an IndexOutOfBoundsException if the list is empty
                    if (quoteLength > quoteDepth) {
                        val quoteDiff = quoteLength - quoteDepth
                        for (i in 1..quoteDiff) {
                            quoteCalls.add(Call("quote_open", listOf(), call.pos))
                        }
                    } else if (quoteLength < quoteDepth) {
                        val quoteDiff = quoteDepth - quoteLength
                        for (i in 1..quoteDiff) {
                            quoteCalls.add(Call("quote_close", listOf(), call.pos))
                        }
                    } else {
                        if (call[0] != "quote_start") quoteCalls.add(Call("linebreak", listOf(), call.pos))
                    }
                    quoteDepth = quoteLength
                }
                "quote_end" -> {
                    if (quoteDepth > 1) {
                        val quoteDiff = quoteDepth - 1
                        for (i in 1..quoteDiff) {
                            quoteCalls.add(Call("quote_close", listOf(), call.pos))
                        }
                    }
                    quoteCalls.add(Call("quote_close", listOf(), call.pos))
                    callWriter.writeCalls(quoteCalls)
                }
                else -> {
                    quoteCalls.add(call)
                }
            }
        }
        return callWriter
    }

    private fun getDepth(marker: String): Int {
        val matches = Regex(">{1,}").findAll(marker).toList()
        val quoteLength = matches[0].value.length
        return quoteLength
    }
}

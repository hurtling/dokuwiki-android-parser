package com.github.medavox.da.parsing.lexer

import java.util.regex.Pattern

class ParallelRegex(private val caseSensitive: Boolean) {
    private val patterns = mutableListOf<String>()
    private val labels = mutableListOf<String>()
    private var regex: Pattern? = null

    fun addPattern(pattern: String, label: String = "") {
        patterns.add(pattern)
        labels.add(label)
        regex = null
    }

    fun apply(subject: String): String {
        if (patterns.isEmpty()) {
            return ""
        }
        val regex = getCompoundedRegex()
        val matcher = regex.matcher(subject)
        if (!matcher.find()) {
            return ""
        }
        val match = matcher.group()
        val size = matcher.groupCount()
        for (i in 1..size) {
            if (matcher.group(i) != null && labels[i - 1].isNotEmpty()) {
                return labels[i - 1]
            }
        }
        return ""
    }

    /**
     * Attempts to split the string against all patterns at once
     *
     * @param string $subject      String to match against.
     * @param array $split         The split result: array containing, pre-match, match & post-match strings
     * @return boolean             True on success.
     *
     * @author Christopher Smith <chris@jalakai.co.uk>
     */
    fun split(subject: String): List<String> {
        if (patterns.isEmpty()) {
            return listOf(subject, "", "")
        }
        val regex = getCompoundedRegex()
        val matcher = regex.matcher(subject)
        if (!matcher.find()) {
            return listOf(subject, "", "")
        }
        val idx = matcher.groupCount() - 1
        val pre = subject.substring(0, matcher.start())
        val post = subject.substring(matcher.end())
        return listOf(pre, matcher.group(), post)
    }

    private fun getCompoundedRegex(): Pattern {
        if (regex == null) {
            val pattern = patterns.joinToString("|")
            regex = Pattern.compile(pattern, getPerlMatchingFlags())
        }
        return regex!!
    }

    private fun getPerlMatchingFlags(): Int {
        return if (caseSensitive) Pattern.MULTILINE else Pattern.MULTILINE or Pattern.CASE_INSENSITIVE
    }
}

package com.github.medavox.da.parsing.parsermode

class Entity(entities: Array<String>) : AbstractMode() {
    private var entities: Array<String> = entities
    private var pattern: String = ""

    init {
        if (entities.isNotEmpty() && pattern.isEmpty()) {
            var sep = ""
            for (entity in entities) {
                pattern += sep + com.github.medavox.da.parsing.lexer.Lexer.escape(entity)
                sep = "|"
            }
        }
    }

    override fun preConnect() {
        if (entities.isEmpty() || pattern != "") return
        var sep = ""
        for (entity in entities) {
            pattern += sep + com.github.medavox.da.parsing.lexer.Lexer.escape(entity)
            sep = "|"
        }
    }

    override fun connectTo(mode: String) {
        if (entities.isEmpty()) return
        if (pattern.length > 0) {
            this.Lexer.addSpecialPattern(pattern, mode, "entity")
        }
    }

    override fun getSort(): Int {
        return 260
    }
}

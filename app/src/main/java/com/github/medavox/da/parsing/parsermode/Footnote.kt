package com.github.medavox.da.parsing.parsermode

import com.github.medavox.da.parser.PARSER_MODES
import com.github.medavox.da.parsing.parsermode.AbstractMode

class Footnote : AbstractMode() {
    init {
        this.allowedModes = (PARSER_MODES.container +
                PARSER_MODES.formatting +
                PARSER_MODES.substition +
                PARSER_MODES.protected +
                PARSER_MODES.disabled).toMutableList()
        this.allowedModes.remove("footnote")
    }

    override fun connectTo(mode: String) {
        this.Lexer.addEntryPattern(
            "\\x28\\x28(?=.*\\x29\\x29)",
            mode,
            "footnote"
        )
    }

    override fun postConnect() {
        this.Lexer.addExitPattern(
            "\\x29\\x29",
            "footnote"
        )
    }

    override fun getSort(): Int {
        return 150
    }
}

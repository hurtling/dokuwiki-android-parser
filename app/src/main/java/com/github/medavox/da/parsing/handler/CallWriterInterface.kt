package com.github.medavox.da.parsing.handler



interface CallWriterInterface {
    /**
     * Add a call to our call list
     *
     * @param call the call to be added
     */
    fun writeCall(call: Call)

    /**
     * Append a list of calls to our call list
     *
     * @param calls list of calls to be appended
     */
    fun writeCalls(calls: List<Call>)

    /**
     * Explicit request to finish up and clean up NOW!
     * (probably because document end has been reached)
     *
     * If part of a CallWriter chain, call finalise on
     * the original call writer
     */
    fun finalise()
}

package com.github.medavox.da.parsing.parsermode

import com.github.medavox.da.parsing.lexer.Lexer

class Smiley : AbstractMode {
    private var smileys: Array<String> = emptyArray()
    private var pattern: String = ""

    constructor(smileys: Array<String>) {
        this.smileys = smileys
    }

    override fun preConnect() {
        if (smileys.isEmpty() || pattern != "") return
        var sep = ""
        for (smiley in smileys) {
            pattern += "$sep(?<=\\W|^)" + com.github.medavox.da.parsing.lexer.Lexer.escape(smiley) + "(?=\\W|\$)"
            sep = "|"
        }
    }

    override fun connectTo(mode: String) {
        if (smileys.isEmpty()) return
        if (pattern.isNotEmpty()) {
            this.Lexer.addSpecialPattern(pattern, mode, "smiley")
        }
    }

    override fun getSort(): Int {
        return 230
    }
}

package com.github.medavox.da.parsing.parsermode

import com.github.medavox.da.parser.PARSER_MODES
import com.github.medavox.da.parsing.parsermode.AbstractMode

class Table : AbstractMode() {
    init {
        allowedModes = PARSER_MODES.formatting +
                PARSER_MODES.substition +
                PARSER_MODES.disabled +
                PARSER_MODES.protected
    }

    override fun connectTo(mode: String) {
        Lexer.addEntryPattern("[\\t ]*\\n\\^", mode, "table")
        Lexer.addEntryPattern("[\\t ]*\\n\\|", mode, "table")
    }

    override fun postConnect() {
        Lexer.addPattern("\\n\\^", "table")
        Lexer.addPattern("\\n\\|", "table")
        Lexer.addPattern("[\\t ]*:::[\\t ]*(?=[\\|\\^])", "table")
        Lexer.addPattern("[\\t ]+", "table")
        Lexer.addPattern("\\^", "table")
        Lexer.addPattern("\\|", "table")
        Lexer.addExitPattern("\\n", "table")
    }

    override fun getSort(): Int {
        return 60
    }
}

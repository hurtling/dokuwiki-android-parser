package com.github.medavox.da.extension

import com.github.medavox.da.Logger

/**
 * The Action plugin event
 */
class Event(
    /** READONLY  event name, objects must register against this name to see the event */
    var name: String = "",
    /** READWRITE data relevant to the event, no standardised format, refer to event docs */
    var data: Any? = null,
    /**
     * READWRITE the results of the event action, only relevant in "_AFTER" advise
     * event handlers may modify this if they are preventing the default action
     * to provide the after event handlers with event results
     */
    var result: Any? = null,
    /** READONLY  if true, event handlers can prevent the events default action */
    var canPreventDefault: Boolean = true,
    /** whether or not to carry out the default action associated with the event */
    protected var runDefault: Boolean = true,
    /** whether or not to continue propagating the event to other handlers */
    protected var mayContinue: Boolean = true
) {
    /**
     * event constructor
     *
     * @param name
     * @param data
     */
    constructor(name: String, data: Any) : this(name, data, null)

    /**
     * @return string
     */
    override fun toString(): String {
        return name
    }

    /**
     * advise all registered BEFORE handlers of this event
     *
     * if these methods are used by functions outside of this object, they must
     * properly handle correct processing of any default action and issue an
     * advise_after() signal. e.g.
     *    val evt = Event(name, data)
     *    if (evt.advise_before(canPreventDefault)) {
     *      // default action code block
     *    }
     *    evt.advise_after()
     *
     * @param enablePreventDefault
     * @return results of processing the event, usually runDefault
     */
    fun advise_before(enablePreventDefault: Boolean = true): Boolean {
        val EVENT_HANDLER
        this.canPreventDefault = enablePreventDefault
        if (EVENT_HANDLER != null) {
            EVENT_HANDLER.process_event(this, "BEFORE")
        } else {
            Logger.getInstance(Logger.LOG_DEBUG)
                .log("$name:BEFORE event triggered before event system was initialized")
        }
        return (!enablePreventDefault || runDefault)
    }

    /**
     * advise all registered AFTER handlers of this event
     *
     * @param enablePreventDefault
     * @see advise_before() for details
     */
    fun advise_after() {
        val EVENT_HANDLER
        mayContinue = true
        if (EVENT_HANDLER != null) {
            EVENT_HANDLER.process_event(this, "AFTER")
        } else {
            Logger.getInstance(Logger.LOG_DEBUG)
                .log("$name:AFTER event triggered before event system was initialized")
        }
    }

    /**
     * trigger
     *
     * - advise all registered (<event>_BEFORE) handlers that this event is about to take place
     * - carry out the default action using data based on enablePrevent and
     *   runDefault, all of which may have been modified by the event handlers.
     * - advise all registered (<event>_AFTER) handlers that the event has taken place
     *
     * @param action
     * @param enablePrevent
     * @return  $event->results
     *          the value set by any <event>_before or <event> handlers if the default action is prevented
     *          or the results of the default action (as modified by <event>_after handlers)
     *          or NULL no action took place and no handler modified the value
     */
    fun trigger(action: ((Any?) -> Any)? = null, enablePreventArg: Boolean = true): Any? {
        var enablePrevent = enablePreventArg
        if (action == null) {
            enablePrevent = false
            if (action != null) {
                error(
                    "The default action of $this " +
                            "is not null but also not callable. Maybe the method is not public?"
                )
            }
        }
        if (advise_before(enablePrevent) && action != null) {
            result = action(data)
        }
        advise_after()
        return result
    }

    /**
     * stopPropagation
     *
     * stop any further processing of the event by event handlers
     * this function does not prevent the default action taking place
     */
    fun stopPropagation() {
        mayContinue = false
    }

    /**
     * may the event propagate to the next handler?
     *
     * @return bool
     */
    fun mayPropagate(): Boolean {
        return mayContinue
    }

    /**
     * preventDefault
     *
     * prevent the default action taking place
     */
    fun preventDefault() {
        runDefault = false
    }

    /**
     * should the default action be executed?
     *
     * @return bool
     */
    fun mayRunDefault(): Boolean {
        return runDefault
    }

    /**
     * Convenience method to trigger an event
     *
     * Creates, triggers and destroys an event in one go
     *
     * @param name name for the event
     * @param data event data
     * @param action (optional, default=NULL) default action, a php callback function
     * @param canPreventDefault (optional, default=true) can hooks prevent the default action
     *
     * @return mixed the event results value after all event processing is complete
     * by default this is the return value of the default action however
     * it can be set or modified by event handler hooks
     */
    companion object {
        fun createAndTrigger(
            name: String,
            data: Any,
            action: ((Any?) -> Any)? = null,
            canPreventDefault: Boolean = true
        ): Any? {
            val evt = Event(name, data)
            return evt.trigger(action, canPreventDefault)
        }
    }
}
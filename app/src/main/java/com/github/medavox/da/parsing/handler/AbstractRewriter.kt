package com.github.medavox.da.parsing.handler

import com.github.medavox.da.parsing.handler.ReWriterInterface

/**
 * Basic implementation of the rewriter interface to be specialized by children
 */
abstract class AbstractRewriter(
    /** original CallWriter */
    protected var callWriter: CallWriterInterface
) : ReWriterInterface(callWriter) {
    override var calls: MutableList<Call> = mutableListOf()

    override fun writeCall(call: Call) {
        calls.add(call)
    }

    override fun writeCalls(calls: List<Call>) {
        this.calls.addAll(calls)
    }

    override fun getCallWriter(): CallWriterInterface {
        return callWriter
    }
}

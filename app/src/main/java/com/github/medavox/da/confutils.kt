package com.github.medavox.da

import com.github.medavox.da.extension.AuthPlugin
import com.github.medavox.da.extension.Event

const val DOKU_CONF_NEGATION = "!"

fun mimetype(file: String, knownonly: Boolean = true): Array<Any> {
    val mtypes = getMimeTypes()
    val ext = file.lastIndexOf('.')
    if (ext == -1) {
        return arrayOf(false, false, false)
    }
    val ext = file.substring(ext + 1).toLowerCase()
    if (!mtypes.containsKey(ext)) {
        if (knownonly) {
            return arrayOf(false, false, false)
        } else {
            return arrayOf(ext, "application/octet-stream", true)
        }
    }
    if (mtypes[ext]?.get(0) == '!') {
        return arrayOf(ext, mtypes[ext]?.substring(1), true)
    } else {
        return arrayOf(ext, mtypes[ext], false)
    }
}

fun getMimeTypes(): Map<String, String> {
    val mime: Map<String, String>? = null
    if (mime == null) {
        val mime = retrieveConfig("mime", ::confToHash)
        val mime = mime.filterValues { it.isNotEmpty() }
    }
    return mime
}

fun getAcronyms(): Map<String, String> {
    val acronyms: Map<String, String>? = null
    if (acronyms == null) {
        val acronyms = retrieveConfig("acronyms", ::confToHash)
        val acronyms = acronyms.filterValues { it.isNotEmpty() }
    }
    return acronyms
}

fun getSmileys(): Map<String, String> {
    val smileys: Map<String, String>? = null
    if (smileys == null) {
        val smileys = retrieveConfig("smileys", ::confToHash)
        val smileys = smileys.filterValues { it.isNotEmpty() }
    }
    return smileys
}

fun getEntities(): Map<String, String> {
    val entities: Map<String, String>? = null
    if (entities == null) {
        val entities = retrieveConfig("entities", ::confToHash)
        val entities = entities.filterValues { it.isNotEmpty() }
    }
    return entities
}

fun getInterwiki(): Map<String, String> {
    val wikis: Map<String, String>? = null
    if (wikis == null) {
        val wikis = retrieveConfig("interwiki", ::confToHash, arrayOf(true))
        val wikis = wikis.filterValues { it.isNotEmpty() }
        wikis["this"] = DOKU_URL + "{NAME}"
    }
    return wikis
}

fun getCdnUrls(): List<String> {
    val conf = mutableMapOf<String, Any>()
    val versions = mutableMapOf<String, String>()
    val lines = file(DOKU_INC + "lib/scripts/jquery/versions")
    for (line in lines) {
        val line = line.trim().replace(Regex("#.*$"), "")
        if (line == "") continue
        val (key, value) = line.split("=", limit = 2)
        versions[key.trim()] = value.trim()
    }
    val src = mutableListOf<String>()
    val data = mapOf("versions" to versions, "src" to src)
    val event = Event("CONFUTIL_CDN_SELECT", data)
    if (event.advise_before()) {
        if (!conf["jquerycdn"]) {
            val jqmod = versions.values.joinToString("-").md5()
            src.add(DOKU_BASE + "lib/exe/jquery.php" + "?tseed=" + jqmod)
        } else if (conf["jquerycdn"] == "jquery") {
            src.add("https://code.jquery.com/jquery-${versions["JQ_VERSION"]}.min.js")
            src.add("https://code.jquery.com/ui/${versions["JQUI_VERSION"]}/jquery-ui.min.js")
        } else if (conf["jquerycdn"] == "cdnjs") {
            src.add("https://cdnjs.cloudflare.com/ajax/libs/jquery/${versions["JQ_VERSION"]}/jquery.min.js")
            src.add("https://cdnjs.cloudflare.com/ajax/libs/jqueryui/${versions["JQUI_VERSION"]}/jquery-ui.min.js")
        }
    }
    event.advise_after()
    return src
}

fun getWordblocks(): List<String> {
    val wordblocks: List<String>? = null
    if (wordblocks == null) {
        val wordblocks = retrieveConfig("wordblock", ::confToHash, null, ::array_merge_with_removal)
    }
    return wordblocks
}

fun getSchemes(): List<String> {
    val schemes: List<String>? = null
    if (schemes == null) {
        val schemes = retrieveConfig("scheme", ::confToHash, null, ::array_merge_with_removal)
        val schemes = schemes.map { it.trim() }
        val schemes = schemes.map { it.replace(Regex("^#.*"), "") }
        val schemes = schemes.filter { it.isNotEmpty() }
    }
    return schemes
}

fun linesToHash(lines: List<String>, lower: Boolean = false): Map<String, String> {
    val conf = mutableMapOf<String, String>()
    if (lines.isNotEmpty() && lines[0].startsWith("\uFEFF")) {
        lines[0] = lines[0].substring(1)
    }
    for (line in lines) {
        val line = line.replace(Regex("(?<![&\\\\])#.*$"), "")
        val line = line.replace("\\#", "#")
        val line = line.trim()
        if (line == "") continue
        val (key, value) = line.split(Regex("\\s+"), limit = 2)
        val line = arrayOf(key, value)
        if (lower) {
            conf[line[0].toLowerCase()] = line[1]
        } else {
            conf[line[0]] = line[1]
        }
    }
    return conf
}

fun confToHash(file: String, lower: Boolean = false): Map<String, String> {
    val conf = mutableMapOf<String, String>()
    val lines = file(file)
    if (lines.isEmpty()) return conf
    return linesToHash(lines, lower)
}

fun jsonToArray(file: String): Map<String, Any> {
    val json = file_get_contents(file)
    val conf = json_decode(json, true)
    if (conf == null) {
        return emptyMap()
    }
    return conf
}

fun retrieveConfig(type: String, fn: (String, Boolean) -> Map<String, String>, params: Array<Any>? = null, combine: (Map<String, String>, Map<String, String>) -> Map<String, String> = ::array_merge): Map<String, String> {
    val config_cascade = mutableMapOf<String, Map<String, List<String>>>()
    if (params == null) params = emptyArray()
    val combined = mutableMapOf<String, String>()
    if (!config_cascade.containsKey(type)) error("Missing config cascade for \"$type\"")
    for (config_group in listOf("default", "local", "protected")) {
        if (config_cascade[type]?.containsKey(config_group) == false) continue
        for (file in config_cascade[type]?.get(config_group) ?: emptyList()) {
            if (file_exists(file)) {
                val config = fn(file, *params)
                combined = combine(combined, config)
            }
        }
    }
    return combined
}

fun getConfigFiles(type: String): List<String> {
    val config_cascade = mutableMapOf<String, Map<String, List<String>>>()
    val files = mutableListOf<String>()
    if (!config_cascade.containsKey(type)) error("Missing config cascade for \"$type\"")
    for (config_group in listOf("default", "local", "protected")) {
        if (config_cascade[type]?.containsKey(config_group) == false) continue
        files.addAll(config_cascade[type]?.get(config_group) ?: emptyList())
    }
    return files
}

fun actionOK(action: String): Boolean {
    val disabled: List<String>? = null
    if (disabled == null || defined("SIMPLE_TEST")) {
        val conf = mutableMapOf<String, Any>()
        val auth: AuthPlugin? = null
        val disabled = conf["disableactions"]?.split(",")?.map { it.trim() }?.toMutableList() ?: mutableListOf()
        if ((conf["openregister"] != null && !conf["openregister"]) || auth == null || !auth.canDo("addUser")) {
            disabled.add("register")
        }
        if ((conf["resendpasswd"] != null && !conf["resendpasswd"]) || auth == null || !auth.canDo("modPass")) {
            disabled.add("resendpwd")
        }
        if ((conf["subscribers"] != null && !conf["subscribers"]) || auth == null) {
            disabled.add("subscribe")
        }
        if (auth == null || !auth.canDo("Profile")) {
            disabled.add("profile")
        }
        if (auth == null || !auth.canDo("delUser")) {
            disabled.add("profile_delete")
        }
        if (auth == null) {
            disabled.add("login")
        }
        if (auth == null || !auth.canDo("logout")) {
            disabled.add("logout")
        }
        disabled = disabled.distinct()
    }
    return !disabled.contains(action)
}

fun useHeading(linktype: String): Boolean {
    val useHeading: Map<String, Boolean>? = null
    if (defined("DOKU_UNITTEST")) useHeading = null
    if (useHeading == null) {
        val conf = mutableMapOf<String, Any>()
        if (!conf["useheading"].isNullOrEmpty()) {
            when (conf["useheading"]) {
                "content" -> useHeading["content"] = true
                "navigation" -> useHeading["navigation"] = true
                else -> {
                    useHeading["content"] = true
                    useHeading["navigation"] = true
                }
            }
        } else {
            useHeading = emptyMap()
        }
    }
    return useHeading[linktype] ?: false
}

fun conf_encodeString(str: String, code: String): String {
    return when (code) {
        "base64" -> "<b>" + base64_encode(str)
        "uuencode" -> "<u>" + convert_uuencode(str)
        "plain" -> str
        else -> str
    }
}

fun conf_decodeString(str: String): String {
    return when (str.substring(0, 3)) {
        "<b>" -> base64_decode(str.substring(3))
        "<u>" -> convert_uudecode(str.substring(3))
        else -> str
    }
}

fun array_merge_with_removal(current: List<String>, new: List<String>): List<String> {
    val merged = current.toMutableList()
    for (value in new) {
        if (value.startsWith(DOKU_CONF_NEGATION)) {
            val idx = merged.indexOf(value.trim().substring(1))
            if (idx != -1) {
                merged.removeAt(idx)
            }
        } else {
            merged.add(value.trim())
        }
    }
    return merged
}
package com.github.medavox.da.parsing.parsermode

import com.github.medavox.da.parsing.parsermode.AbstractMode

class Internallink : AbstractMode() {
    override fun connectTo(mode: String) {
        // Word boundaries?
        this.Lexer.addSpecialPattern("\\[\\[.*?\\]\\](?!\\])", mode, "internallink")
    }

    override fun getSort(): Int {
        return 300
    }
}
